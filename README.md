# fonia4

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what
PSRs you support to avoid any confusion with users and contributors.

## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
bin/        
build/
docs/
config/
src/
tests/
vendor/
```


## Install

Via Composer

``` bash
$ composer require siaupheng/fonia4
```

## Usage

``` php
$skeleton = new siaupheng\fonia4();
echo $skeleton->echoPhrase('Hello, League!');
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

## Security

If you discover any security related issues, please email siaupheng@3fonia.com instead of using the issue tracker.

## Credits

- [Siau Pheng][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/siaupheng/fonia4.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/siaupheng/fonia4/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/siaupheng/fonia4.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/siaupheng/fonia4.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/siaupheng/fonia4.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/siaupheng/fonia4
[link-travis]: https://travis-ci.org/siaupheng/fonia4
[link-scrutinizer]: https://scrutinizer-ci.com/g/siaupheng/fonia4/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/siaupheng/fonia4
[link-downloads]: https://packagist.org/packages/siaupheng/fonia4
[link-author]: https://github.com/siaupheng
[link-contributors]: ../../contributors

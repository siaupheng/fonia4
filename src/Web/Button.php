<?php

/*******************************************************************

    Module        : /Web/Button.php
    Desc.         : v4 - Button Generator (jQuery & CoreUI)
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 27th, 2008.
    Last Modified : April 4th, 2022.

    (c) 2008 - 2022, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\Web;

class Button {
    private $__abutton = array();
    private $__bsepa = false;

    public function __construct() {}
    private function add($type="info", $id="", $label="", $bmiss=false) {
        if ($id=="") $id = "random".rand(1000, 10000);
        $this->__abutton[] = array('type'=>$type, 'id'=>$id, 'label'=>$label, 'bmiss'=>$bmiss);
    }

    public function separator() {
        $this->__abutton[] = array('type'=>"separator");
        $this->__bsepa = true;
    }
    public function primary($id="", $label="", $bmiss=false) {
        $this->add("primary", $id, $label, $bmiss);
    }
    public function info($id="", $label="", $bmiss=false) {
        $this->add("info", $id, $label, $bmiss);
    }
    public function danger($id="", $label="", $bmiss=false) {
        $this->add("danger", $id, $label, $bmiss);
    }
    public function warning($id="", $label="", $bmiss=false) {
        $this->add("warning", $id, $label, $bmiss);
    }
    public function success($id="", $label="", $bmiss=false) {
        $this->add("success", $id, $label, $bmiss);
    }
    public function secondary($id="", $label="", $bmiss=false) {
        $this->add("secondary", $id, $label, $bmiss);
    }

    public function Button($size=2) {
        if (0==count($this->__abutton)) return "";
        $items = count($this->__abutton) - ($this->__bsepa==true ? 1 : 0);
        $sepa_size = (12 == $items*$size) ? 0 : 12 - ($items*$size);
        $_out = '<div class="row mb-2">';
        foreach ($this->__abutton as $item) {
            if ("separator" == $item['type']) {
                if (0 < $sepa_size) $_out .= '<div class="d-none d-lg-block col-lg-'.$sepa_size.'">&nbsp;</div>';
            } else {
                $_out .= '<div class="col-md-'.floor($size*2).' col-lg-'.$size.' mb-sm-0"><button type="button" class="btn btn-block btn-'.$item['type'].' shadow-none" id="'.$item['id'].'"'.(true==$item['bmiss']?' data-coreui-dismiss="modal"':'').'>'.$item['label'].'</button></div>';
            }
        }
        $_out .= '</div>';
        return $_out;
    }
}

?>

<?php

/*******************************************************************

    Module        : /Web/Form.php
    Desc.         : v4 - Form Generator (jQuery & CoreUI)
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : February 27th, 2008.
    Last Modified : April 17th, 2022.

    (c) 2008 - 2022, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\Web;

final class Form {
    private $__aform = array();
    private $__ahide = array();
    private $__button = null;
    private $__nbtnsize = 2;
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia4\DB\MySQL(); }
        $this->__button = new Button();
    }

    public function option($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $aarr=array(), $thelp="", $tchange=null, $bmulti=false) {
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_change = ($tchange==null) ? '' : ' onchange="'.$tchange.'"';
        $_multi = ($bmulti==true) ? ' multiple' : '';
        $tout = '<select class="form-select option-tags" id="'.$tfield.'" name="'.$tfield.'"'.$_enab.$_change.$_req.$_multi.' >';
        if (count($aarr)>0) {
            foreach ($aarr as $key => $val) {
                $_val2 = $val; $olabel = "";
                if (is_array($val)) {
                    $_val2 = $val[0];
                    $olabel = (isset($val[1])) ? ' olabel="'.$val[1].'"' : "";
                }
                $tout .= '<option value="'.$key.'"'.$olabel;
                $aval = array($tvalue);
                if ($bmulti==true) $aval = explode(",", $tvalue);
                $tout .= (in_array($key, $aval)) ? ' selected="selected"' : "";
                $tout .= '>'.$_val2.'</option>';
            }
        }
        $tout .= '</select>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function option_db($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $ttable=null, $afield=null, $twhere=null, $addarray=null, $thelp=null, $tchange=null, $bmulti=false) {
        $afield = ($afield==null) ? array("ID", "Nama", "Label") : $afield;
        $_twhere = ($twhere==null) ? "" : " WHERE ".$twhere;
        $_twhere .= ($twhere===null || stripos($twhere,"ORDER BY")===false) ? " ORDER BY ".$afield[1] : "";
        $ahasil = array();
        if (is_array($addarray)) $ahasil[$addarray[0]] = (!isset($afield[2])) ? $addarray[1] : array($addarray[1], $addarray[2]);
        if ($ttable<>null) {
            $this->__db->Query("SELECT ".implode(",", $afield)." FROM ".$ttable.$_twhere);
            while ($this->__db->Next()) {
                $ahasil[$this->__db->Row($afield[0])] = (!isset($afield[2])) ? $this->__db->Row($afield[1]) : array($this->__db->Row($afield[1]), $this->__db->Row($afield[2]));
            }
        }
        $tout = $this->option($tlabel, $tfield, $tvalue, $benable, $breq, $ahasil, $thelp, $tchange, $bmulti);
        return $tout;
    }

    public function option_que($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $tquery=null, $afield=null, $addarray=null, $thelp="", $tchange=null) {
        $afield = ($afield==null) ? array("ID", "Nama", "Label") : $afield;
        $ahasil = array();
        if (is_array($addarray)) $ahasil[$addarray[0]] = (!isset($afield[2])) ? $addarray[1] : array($addarray[1], $addarray[2]);
        if ($tquery<>null) {
            $this->__db->Query($tquery);
            while ($this->__db->Next()) {
                $ahasil[$this->__db->Row($afield[0])] = (!isset($afield[2])) ? $this->__db->Row($afield[1]) : array($this->__db->Row($afield[1]), $this->__db->Row($afield[2]));
            }
        }
        $tout = $this->option($tlabel, $tfield, $tvalue, $benable, $breq, $ahasil, $thelp, $tchange);
        return $tout;
    }

    public function option_jkl($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $thelp="", $tchange=null) {
        $akel = array("L" => "Laki-Laki", "P" => "Perempuan");
        $tout = $this->option($tlabel, $tfield, $tvalue, $benable, $breq, $akel, $thelp, $tchange);
        return $tout;
    }

    public function option_ulevel($tlabel="", $tfield, $tvalue=9, $benable=true, $breq=false, $thelp="", $tchange=null) {
        $tout = $this->option($tlabel, $tfield, $tvalue, $benable, $breq, $_SESSION['__WEB_APP']['USER_LEVEL'], $thelp, $tchange);
        return $tout;
    }

    public function radio($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $aarr=array(), $thelp="", $tchange=null) {
        if ($tvalue == null) {
            $tmp_key = array_keys($aarr);
            $tvalue = $tmp_key[0];
        }
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_change = ($tchange==null) ? "" : ' onchange="'.$tchange.'"';
        $tout = '<input type="hidden" id="'.$tfield.'" value="'.$tvalue.'" />';
        if (count($aarr)>0) {
            foreach ($aarr as $key => $val) {
                $tout .= '<div class="form-check form-check-inline mr-2"><input class="form-check-input" id="'.$tfield.$key.'" type="radio" value="'.$key.'" name="'.$tfield.'"';
                $tout .= ($key == $tvalue) ? ' checked="checked"' : '';
                $tout .= ' onclick="if(this.checked)$(\'#'.$tfield.'\').val(this.value);"'.$_enab.$_change.$_req;
                $tout .= '><label class="form-check-label" for="'.$tfield.$key.'">'.$val.'</label></div>';
            }
        }
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function radio_jkl($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $thelp="", $tchange=null) {
        $tout = $this->radio($tlabel, $tfield, $tvalue, $benable, $breq, array("L" => "Laki-Laki", "P" => "Perempuan"), $thelp, $tchange);
        return $tout;
    }

    public function radio_gd($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $thelp="", $tchange=null) {
        $tvalue = ($tvalue=="") ? "X" : $tvalue;
        $tout = $this->radio($tlabel, $tfield, $tvalue, $benable, $breq, array("A"=>"A", "B"=>"B", "AB"=>"AB", "O"=>"O", "X"=>"NONE"), $thelp, $tchange);
        return $tout;
    }

    public function checkbox($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $avalue=array(), $thelp="", $tchange=null) {
        $bhasil = ((is_string($tvalue) && in_array($tvalue,array("N","T","0"))) or (is_numeric($tvalue) && $tvalue==0) or (is_bool($tvalue) && $tvalue===false) or empty($tvalue)) ? true : false;
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_change = ($tchange==null) ? ' onchange="this.value=(this.checked)?\''.$avalue[0].'\':\''.$avalue[1].'\';"' : ' onchange="this.value=(this.checked)?\''.$avalue[0].'\':\''.$avalue[1].'\';'.$tchange.'"';
        $tout = '<div class="form-check form-check-inline mr-1"><input type="checkbox" class="form-check-input" id="'.$tfield.'" name="'.$tfield.'" value="'.($bhasil?$avalue[1]:$avalue[0]).'"'.$_enab.$_change.$_req;
        $tout .= ($bhasil==false) ? ' checked="checked"' : '';
        $tout .= '></div>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    //todo: perlu test lagi !!
    public function switch_sq($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $aarr=array(), $thelp="", $tchange=null) {
        $_key = array_keys($aarr); $_val = array_values($aarr);
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_change = ($tchange==null) ? ' onchange="this.value=(this.checked)?\''.$_key[0].'\':\''.$_key[1].'\';"' : ' onchange="this.value=(this.checked)?\''.$_key[0].'\':\''.$_key[1].'\';'.$tchange.'"';
        $tout = '<div class="form-check form-switch form-switch-lg"><input class="form-check-input me-0" id="'.$tfield.'" name="'.$tfield.'" type="checkbox" value="'.$tvalue.'"'.$_enab.$_change.$_req;
        $tout .= ($_key[0] == $tvalue) ? ' checked="checked"' : '';
        $tout .= '><label class="form-check-label fw-semibold small" for="'.$tfield.'">&nbsp;</label></div>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    //todo: belum diupgrade !!
    public function form_group_cbox($tfield, $tkey=null, $tclass=null, $ttable=null, $afield=null, $ncol=2) {
        if ($ttable==null) return;
        $avalue = explode(",", $tkey);
        $afield = ($afield==null) ? array("ID", "Nama") : $afield;
        $_twhere = ($twhere=="") ? " ORDER BY ".$afield[0] : " WHERE ".$twhere;
        $tout = "<input id=\"".$tfield."\" value=\"".$tkey."\" type='hidden' class='cgrpbox' /><table width='100%' style='margin:1px 0;padding:2px 2px;'><tr>";

        $nkol = 1;
        $this->__db->Query("SELECT ".implode(",", $afield)." FROM ".$ttable.$_twhere);
        while ($this->__db->Next()) {
            $tout .= "<td width='".round(100/$ncol)."%'><label><input type='checkbox' id='".$tfield."_cgrpbox_".$this->__db->Row($afield[0])."' value='".$this->__db->Row($afield[0])."'";
            for ($i=0; $i<count($avalue); $i++) {
                if ($avalue[$i]==$this->__db->Row($afield[0])) {
                    $tout .= " checked=\"checked\"";
                    break;
                }
            }
            $tout .= " class='text ".$tclass."' />&nbsp;".$this->__db->Row($afield[1])."</label></td>";
            if ($nkol==$ncol) {
                $tout .= "</tr><tr>";
                $nkol = 0;
            }
            $nkol++;
        }
        $tout .= "</tr></table>";
        $this->__aform[$tfield] = $tout;
        return $tout;
    }

    public function textbox($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $ttype="text", $holdtxt=null, $nmaxsize=null, $thelp="", $tkeyup=null, $tblur=null) {
        $_enab = ($benable==true) ? "" : ' readonly';
        $_req = ($breq==true) ? " required=true" : "";
        $_max = ($nmaxsize==null) ? "" : ' maxlength="'.$nmaxsize.'"';
        $_keyup = ($tkeyup==null) ? "" : ' onkeyup="'.$tkeyup.'"';
        $_blur = ($tblur==null) ? "" : ' onblur="'.$tblur.'"';
        $_holdtxt = ($holdtxt==null) ? "" : ' placeholder="'.$holdtxt.'"';
        $_tclass = "";
        if (in_array($ttype,array("pickdate","picktime"))) {
            $_tclass = ' '.$ttype;
            $ttype = "text";
        }
        if (in_array($ttype,array("double"))) {
            $_tclass = ' '.$ttype;
            $tvalue = uang2($tvalue);
        }
        $tout = '<input type="'.$ttype.'" class="form-control'.$_tclass.'" id="'.$tfield.'" name="'.$tfield.'" value="'.$tvalue.'"'.$_enab.$_max.$_keyup.$_blur.$_holdtxt.$_req.' />';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function textauto($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $holdtxt=null, $nmaxsize=null, $thelp="") {
        $tvalue = ($tvalue==null) ? array('value'=>0, 'text'=>"") : $tvalue;
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_max = ($nmaxsize==null) ? "" : ' maxlength="'.$nmaxsize.'"';
        $_holdtxt = ($holdtxt==null) ? "" : ' placeholder="'.$holdtxt.'"';
        $tout = '<select class="form-control" id="'.$tfield.'" name="'.$tfield.'" default-value="'.$tvalue['value'].'" default-text="'.$tvalue['text'].'"'.$_enab.$_max.$_holdtxt.$_req.' /></select>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function datebox($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $thelp="", $tkeyup=null, $tblur=null) {
        if ($tvalue<>null) $tvalue = tgl_sql2str($tvalue);
        $tout = '<div class="input-group">';
        $tout .= $this->textbox($tlabel, $tfield, $tvalue, $benable, $breq, "pickdate", "dd-mm-yyyy", 12, $thelp, $tkeyup, $tblur);
        $tout .= '<span class="input-group-text"><i class="cui-calendar"></i></span></div>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function timebox($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $thelp="", $tkeyup=null, $tblur=null) {
        $tout = $this->textbox($tlabel, $tfield, $tvalue, $benable, $breq, "picktime", "hh:mm:ss", 10, $thelp, $tkeyup, $tblur);
        return $tout;
    }

    public function textarea($tlabel="", $tfield, $tvalue=null, $benable=true, $breq=false, $nrows=1, $holdtxt=null, $thelp="", $tkeyup=null, $tblur=null) {
        $_enab = ($benable==true) ? "" : ' disabled=true';
        $_req = ($breq==true) ? " required=true" : "";
        $_rows = ($nrows==null) ? "" : ' rows="'.$nrows.'"';
        $_keyup = ($tkeyup==null) ? "" : ' onkeyup="'.$tkeyup.'"';
        $_blur = ($tblur==null) ? "" : ' onblur="'.$tblur.'"';
        $_holdtxt = ($holdtxt==null) ? "" : ' placeholder="'.$holdtxt.'"';
        $tout = '<textarea class="form-control" id="'.$tfield.'" name="'.$tfield.'"'.$_enab.$_rows.$_keyup.$_blur.$_holdtxt.$_req.' >'.$tvalue.'</textarea>';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>$thelp, 'form'=>$tout);
        return $tout;
    }

    public function plaintext($tlabel="", $tfield, $tvalue=null) {
        $tout = '<input type="text" class="form-control-plaintext" id="'.$tfield.'" name="'.$tfield.'" value="'.$tvalue.'" readonly />';
        $this->__aform[$tfield] = array('label'=>$tlabel, 'help'=>"", 'form'=>$tout);
        return $tout;
    }

    public function password($tlabel="", $tfield, $tvalue="", $benable=true, $breq=false, $holdtxt=null, $nmaxsize=null, $thelp="", $tkeyup=null) {
        $tout = $this->textbox($tlabel, $tfield, $tvalue, $benable, $breq, "password", $holdtxt, $nmaxsize, $thelp, $tkeyup, $tblur);
        return $tout;
    }

    public function hidden($tfield, $tvalue="", $tclass="text") {
        $tout = '<input type="hidden" id="'.$tfield.'" value="'.$tvalue.'" class="'.$tclass.'" />';
        $this->__ahide[$tfield] = $tout;
        return $tout;
    }

    public function clear() {
        $this->__aform = array();
    }

    public function show($tstr) {
        foreach ($this->__aform as $_key => $_val) {
            $tval = '<div class="row mb-1"><label class="col-sm-5 col-lg-4 col-form-label" for="'.$_key.'">'.$_val['label'].'</label><div class="col-sm-7 col-lg-8">'.$_val['form'].($_val['help']==""?'':'<span class="help-block"><small>'.$_val['help'].'</small></span>').'</div></div>';
            $tstr = str_replace("_".$_key."_", $tval, $tstr);
        }
        $txtbtn = $this->__button->Button($this->__nbtnsize);
        if (""<>$txtbtn) {
            $tstr .= '<div class="row mb-1"><div class="col"><hr class="my-2"></div></div>';
            $tstr .= $txtbtn;
        }
        foreach ($this->__ahide as $_key => $_val) {
            $tstr = str_replace("_".$_key."_", $_val, $tstr);
        }
        $tstr = str_replace("\n", "", $tstr);
        $tstr = str_replace("\t", "", $tstr);
        echo $tstr;
    }

    public function btn_separator() {
        $this->__button->separator();
    }
    public function btn_primary($id="", $label="", $bmiss=false) {
        $this->__button->primary($id, $label, $bmiss);
    }
    public function btn_info($id="", $label="", $bmiss=false) {
        $this->__button->info($id, $label, $bmiss);
    }
    public function btn_danger($id="", $label="", $bmiss=false) {
        $this->__button->danger($id, $label, $bmiss);
    }
    public function btn_warning($id="", $label="", $bmiss=false) {
        $this->__button->warning($id, $label, $bmiss);
    }
    public function btn_success($id="", $label="", $bmiss=false) {
        $this->__button->success($id, $label, $bmiss);
    }
    public function btn_secondary($id="", $label="", $bmiss=false) {
        $this->__button->secondary($id, $label, $bmiss);
    }
    public function Button($size=2) {
        $this->__nbtnsize = $size;
    }
}

?>

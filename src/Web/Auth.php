<?php

/*******************************************************************

    Module        : /Web/Auth.php
    Desc.         : v4 - Fungsi login dan user checking
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : March 16th, 2008.
    Last Modified : April 3rd, 2022.

    (c) 2008 - 2022, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\Web;

final class Auth {
    private $__id_reg = 0;
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia4\DB\MySQL(); }
        $this->__id_reg = $_SESSION['__WEB_APP']['REG_ID'];
    }

    public function set_id_reg($id = 0) {
        if ($id > 0) $this->__id_reg = $id;
    }

    public function user_check($user, $pass) {
        $i_hasil = 0;
        $this->__db->Query("SELECT COUNT(*) Ada FROM app_operator WHERE Username='".$this->__db->safeSQL($user)."' AND Password=MD5('".$this->__db->safeSQL($pass)."') AND Aktif=1");
        if ($this->__db->Next()) $i_hasil = $this->__db->Row("Ada", 0);
        return ($i_hasil > 0 ? true : false);
    }

    public function user_data($user) {
        $adata = array();
        $this->__db->Query("SELECT *,DECODE(Menu,'".$this->__id_reg."') Menu2 FROM app_operator WHERE Username='".$this->__db->SafeSQL($user)."'");
        if ($this->__db->Next()) {
            $adata = $this->__db->AllRow();
            $adata['Menu'] = $adata['Menu2'];
            unset($adata['Menu2']);
        }
        if (!isset($_SESSION['APP_TOKEN'])) {
            $_token = $this->genToken(60);
            $_SESSION['APP_TOKEN'] = $_token;
            $this->__db->Query("INSERT INTO app_token SET Token='".$_token."',UserID='".$adata['UserID']."',IP='".$_SERVER['REMOTE_ADDR']."'");
        }
        $adata['UNIX'] = time();
        $adata['LOCALE'] = $_SESSION['__WEB_APP']['DEF_LOCALE'];
        return $adata;
    }

    public function gen_module_menu() {
        $amodul = array();
        $aallow = array();
        if ($_SESSION['__WEB_APP']['APP_MODULE']) {
            $this->__db->Query("SELECT * FROM app_menu_grup WHERE Grup_MenuID='".$_SESSION['__WEB_USER']['Grup_MenuID']."' LIMIT 1");
            if ($this->__db->Next()) $aallow = ($this->__db->Row("Modul_Akses")=="") ? array() : explode(",", $this->__db->Row("Modul_Akses"));
            if (count($aallow) > 0) {
                $this->__db->Query("SELECT * FROM (SELECT APP_MODULE,APP_PATH FROM app_config) A LEFT JOIN (SELECT Modul,Urut,Judul,Icon FROM app_menu WHERE Level=0) B ON (A.APP_MODULE=B.Modul) ORDER BY B.Urut");
                while ($this->__db->Next()) {
                    if (!in_array($this->__db->Row("Modul"), $aallow) || $this->__db->Row("Modul")==$_SESSION['__WEB_APP']['APP_MODULE']) continue;
                    $amodul[] = $this->__db->AllRow();
                }
            }
        }
        $_SESSION['__WEB_USER']['MENU_MODUL'] = $amodul;
    }

    public function user_allmenu($id_grup = 0) {
        $allmenu = "";
        $this->__db->Query("SELECT DECODE(Menu,'".$this->__id_reg."') MenuX FROM app_menu_grup WHERE Grup_MenuID='".$id_grup."'");
        if ($this->__db->Next()) $allmenu = $this->__db->Row("MenuX");
        return $allmenu;
    }

    public function user_check_level($user, $level = 9) {
        $i_hasil = 0;
        $this->__db->Query("SELECT COUNT(*) Ada FROM app_operator WHERE Username='".$this->__db->safeSQL($user)."' AND Level<='".$this->__db->safeSQL($level)."' AND Aktif=1");
        if ($this->__db->Next()) $i_hasil = $this->__db->Row("Ada", 0);
        return ($i_hasil > 0 ? true : false);
    }

    public function user_log() {
        if ($_SESSION['__WEB_APP']['USER_LOG'] == false) return;
        $a_hasil = array();
        $this->__db->Query("SELECT * FROM app_logs WHERE UserID='".user_id()."' ORDER BY Last_Date DESC LIMIT 1");
        if ($this->__db->Next()) $a_hasil = $this->__db->AllRow();
        if (count($a_hasil) > 0) {
            $this->__db->Query("UPDATE app_operator SET Last_IP='".$a_hasil['Last_IP']."',Last_Date='".$a_hasil['Last_Date']."' WHERE UserID='".user_id()."'");
        }
        //$tgl_lalu = tgl_sql2str(tgl_operasi(tgl_dmy(), 0, -3));
        //$this->__db->Query("DELETE FROM app_logs WHERE UserID='".user_id()."' AND Last_Date<'".$tgl_lalu."'");
        $this->__db->Query("INSERT IGNORE INTO app_logs SELECT UserID,Token,IP,DateStamp FROM app_token WHERE Token='".$_SESSION['APP_TOKEN']."'");
    }

    public function logout() {
        $this->__db->Query("DELETE FROM app_token WHERE Token='".$_SESSION['APP_TOKEN']."'");
    }

    public function token_check() {
        $hasil = false;
        $this->__db->Query("SELECT * FROM app_token WHERE Token='".$_SESSION['APP_TOKEN']."'");
        if ($this->__db->Next()) $hasil = true;
        if ($hasil == true) $this->__db->Query("UPDATE app_token SET DateStamp=NOW() WHERE Token='".$_SESSION['APP_TOKEN']."'");
        return $hasil;
    }

    public function token_update() {
        $this->__db->Query("UPDATE app_token SET DateStamp=NOW() WHERE Token='".$_SESSION['APP_TOKEN']."'");
    }

    public function redirect($appUrl) {
        $source = parse_url($appUrl);
        $desti = "";
        $this->__db->Query("SELECT * FROM app_host WHERE Nama_Host='".$source['host']."'");
        if ($this->__db->Next()) {
            $desti = $source['scheme'].$source['host'].$source['path'].'?'.'auth='.$_SESSION['APP_TOKEN'];
        }
        return $desti;
    }

    private function genToken($length = 16) {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    public function getUserByToken($token) {
        $hasil = "";
        $this->__db->Query("SELECT * FROM app_operator WHERE UserID IN (SELECT UserID FROM app_token WHERE Token='".$token."') LIMIT 1");
        if ($this->__db->Next()) $hasil = $this->__db->Row("Username");
        return $hasil;
    }
}

?>

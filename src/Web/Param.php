<?php

/*******************************************************************

    Module        : /Web/Param.php
    Desc.         : v4 - Class data parameter dari client side
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : November 14th, 2013.
    Last Modified : May 8th, 2021.

    (c) 2013 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\Web;

final class Param {
    private static $__ahasil = array();
    
    public function __construct() {
        if (count(self::$__ahasil)==0) self::__data();
    }

    private static function __data() {
        $_asumber = array_merge($_REQUEST, $_POST, $_GET);
        foreach ($_asumber as $__key => $__val) {
            self::$__ahasil[$__key] = self::safeSQL($__val);
        }
    }

    public static function safeSQL($tstr = "") {
        $_se = array("\\","\0","\n","\r","\x1a","'",'"');
        $_re = array("\\\\","\\0","\\n","\\r","\Z","\'",'\"');
        return str_replace($_se, $_re, $tstr);
    }

    public static function Get($tkey = "", $tdef = null) {
        if (count(self::$__ahasil)==0) self::__data();
        if (isset(self::$__ahasil[$tkey]) && self::$__ahasil[$tkey]) return self::$__ahasil[$tkey];
        return $tdef;
    }

    public static function Get_Text($tkey = "", $tdef = null) {
        if (count(self::$__ahasil)==0) self::__data();
        $_str = (isset(self::$__ahasil[$tkey])) ? self::$__ahasil[$tkey] : $tdef;
        $_str = str_replace("\\r", "", $_str);
        $_str = str_replace("\\n", " ", $_str);
        $_str = str_replace("\\t", " ", $_str);
        return (trim($_str));
    }

    public static function Get_Array() {
        if (count(self::$__ahasil)==0) self::__data();
        return self::$__ahasil;
    }

    public static function Get_ArrayCBOX() {
        if (count(self::$__ahasil)==0) self::__data();
        $_ahasil = array();
        foreach (self::$__ahasil as $__key => $__val) {
            if (strpos($__key, "_cbox_")>0) {
                $_tmpkey = substr($__key, strpos($__key, "_cbox_")+6);
                $_ahasil[$_tmpkey] = $__val;
            }
        }
        return $_ahasil;
    }
}

?>

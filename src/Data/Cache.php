<?php

/*******************************************************************

    Module        : /Data/Cache.php
    Desc.         : v4 - Class cache aplikasi
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : March 16th, 2008.
    Last Modified : May 8th, 2021.

    (c) 2008 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\Data;

use MatthiasMullie\Minify;

final class Cache {
    private $__cachedays = 0;
    private $__cachedir  = "";
    private $__cssdir    = "";
    private $__localedir = "";
    private $__jsdir     = "";
    private $__jsapp     = "";
    private $__realpath  = "";
    private $__ini = array();
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else $this->__db = new \siaupheng\fonia4\DB\MySQL();

        $rootdir = $_SESSION['__WEB_APP']['REAL_PATH'];
        $this->__cachedir  = $rootdir . 'cache/';
        $this->__cssdir    = __DIR__ . '/../../assets/css/';
        $this->__localedir = $rootdir . 'assets/locale/';
        $this->__jsdir     = __DIR__ . '/../../assets/js/';
        $this->__jsapp     = $rootdir . 'apps/js/';
        $this->__realpath  = $rootdir;
        if (!file_exists($this->__cachedir . "cache.ini")) {
            $this->__ini = array('css'=>array('login_stamp'=>"0",'login_file'=>"",'index_stamp'=>"0",'index_file'=>""), 'js'=>array('login_stamp'=>"0",'login_file'=>"",'index_stamp'=>"0",'index_file'=>""));
            $this->save_ini();
        }
        $this->__ini = parse_ini_file($this->__cachedir . "cache.ini", true);
    }

    private function gen_file($type="css", $mode="index") {
        $afile = array();
        if ($type == "css") {
            $_afile = array_unique($GLOBALS['A_CSS']);
            foreach ($_afile as $__key => $val) {
                $afile[] = $this->__cssdir .$val;
            }
            if (file_exists($this->__realpath . 'assets/css/')) {
                if ($handle = opendir($this->__realpath . 'assets/css/')) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == '.' || $file == '..' || is_dir($this->__realpath . 'assets/css/'.$file)) continue;
                        $afile[] = $this->__realpath . 'assets/css/' . $file;
                    }
                    closedir($handle);
                }
            }
        } else if ($type == "js") {
            $_afile = array_unique($GLOBALS['A_JS']);
            foreach ($_afile as $__key => $val) {
                $afile[] = $this->__jsdir .$val;
            }
            if (file_exists($this->__realpath . 'assets/js/')) {
                if ($handle = opendir($this->__realpath . 'assets/js/')) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file == '.' || $file == '..' || is_dir($this->__realpath . 'assets/js/'.$file)) continue;
                        $afile[] = $this->__realpath . 'assets/js/' . $file;
                    }
                    closedir($handle);
                }
            }
            // add locale file
            $ljs = (isset($_SESSION['__WEB_USER']['LOCALE'])?$_SESSION['__WEB_USER']['LOCALE']:$_SESSION['__WEB_APP']['DEF_LOCALE']).".js";
            if ($handle = opendir($this->__localedir)) {
                while (false !== ($file = readdir($handle))) {
                    $file = $this->__localedir . $file;
                    if (substr($file, -5) == $ljs) $afile[] = $this->__localedir .basename(str_replace("\\", "/", $file));
                }
                closedir($handle);
            }
            $afile[] = $this->__jsapp . $mode . ".js";
        } else if ($type == "app") {
            $afile[] = $this->__jsapp ."sys/profile.js";
            $afile[] = $this->__jsapp ."sys/chpasswd.js";
            $amenu = explode(",", $_SESSION['__WEB_USER']['Menu_Grup'] .",". $_SESSION['__WEB_USER']['Menu']);
            $tmenu = implode("','", array_unique($amenu));
            $this->__db->Query("SELECT * FROM app_menu WHERE Link<>'' AND Kode IN ('".$tmenu."') ORDER BY Level, Urut");
            while ($this->__db->Next()) {
                $afile[] = $this->__jsapp . str_replace("php", "js", $this->__db->Row("Link"));
            }
        }
        return $afile;
    }

    private function check_file($afile) {
        $lastmod = 0;
        foreach ($afile as $nfile) {
            if (file_exists($nfile)) {
                $lastmod += filemtime($nfile);
            }
        }
        if ($this->__cachedays > 0) {
            if ($handle = opendir($this->__cachedir)) {
                while (false !== ($file = readdir($handle))) {
                    if ($file == '.' || $file == '..' || is_dir($this->__cachedir.$file)) continue;
                    if ((time()-filemtime($this->__cachedir.$file)) > (86400*$this->__cachedays)) {
                        unlink($this->__cachedir . $file);
                    }
                }
                closedir($handle);
            }
        }
        return $lastmod;
    }

    private function create_cache($type="css", $mode="index", $afile=array(), $lastmod=0) {
        $contents = '';
        $bpublic = ($_SERVER['SERVER_ADDR'] == "127.0.0.1") ? false : true;
        if ($bpublic == true) {
            if ($type=="css") {
                $smini = new Minify\CSS;
            } else {
                $smini = new Minify\JS;
            }
        }
        foreach ($afile as $nfile) {
            if (file_exists($nfile)) {
                if ($bpublic == true) {
                    $smini->add($nfile);
                } else {
                    $contents .= "\n". file_get_contents($nfile);
                }
            }
        }
        if ($bpublic) $contents = $smini->minify();
        $cachefile = md5($mode . $lastmod) . ($type=="app"?".app":"") . ($type=="css" ? ".css" : ".js");
        file_put_contents($this->__cachedir . $cachefile, $contents);
        if ($type == "app") return;
        $this->__ini[$type][$mode.'_stamp'] = $lastmod;
        $this->__ini[$type][$mode.'_file'] = $cachefile;
        $this->save_ini();
    }

    private function save_ini(){
        $content = ""; 
        foreach ($this->__ini as $key => $elem) {
            $content .= ($content==""?"":"\n"). "[".$key."]\n";
            foreach ($elem as $key2 => $elem2) {
                if ($elem2=="") $content .= $key2." = \n";
                else $content .= $key2." = \"".$elem2."\"\n";
            }
        }
        file_put_contents($this->__cachedir . "cache.ini", $content);
    }

    public function Main($type="css") {
        $mode = (isset($_SESSION['__WEB_USER']) && is_array($_SESSION['__WEB_USER']) && $_SESSION['__WEB_USER']['UserID']>0) ? "index" : "login";
        $afile = $this->gen_file($type, $mode);
        $lastmod = $this->check_file($afile);
        if ($lastmod <> intval($this->__ini[$type][$mode.'_stamp']) || !file_exists($this->__cachedir . $this->__ini[$type][$mode.'_file'])) {
            $this->create_cache($type, $mode, $afile, $lastmod);
        }
        return $_SESSION['__WEB_APP']['WEB_PATH'] ."cache/". $this->__ini[$type][$mode.'_file'];
    }

    public function Apps() {
        $id_user = (isset($_SESSION['__WEB_USER']) && is_array($_SESSION['__WEB_USER'])) ? $_SESSION['__WEB_USER']['UserID'] : 0;
        $afile = $this->gen_file("app", $id_user);
        $lastmod = $this->check_file($afile);
        $appfile = md5($id_user . $lastmod) . ".app.js";
        if (!file_exists($this->__cachedir . $appfile)) {
            $this->create_cache("app", $id_user, $afile, $lastmod);
        }
        return $_SESSION['__WEB_APP']['WEB_PATH'] ."cache/". $appfile;
    }
}

?>

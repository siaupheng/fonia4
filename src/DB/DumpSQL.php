<?php

/*******************************************************************

    Module        : /DB/DumpSQL.php
    Desc.         : v4 - Class Dump Data SQL
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : May 11th, 2011.
    Last Modified : May 8th, 2021.

    (c) 2011 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\DB;

final class DumpSQL {
    private $__adata = array();
    private $__aignore = array("vi_%","rep_%");
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia4\DB\MySQL(); }

        $this->__adata = $this->__db->getConfig();
    }

    public function set_db($nama_db = "") {
        if ($nama_db<>"") $this->__adata['db'] = $nama_db;
    }

    public function ignore_table() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0];
        $this->__aignore = array_merge($this->__aignore, $tmp_arr);
    }

    private function ignore_prepare() {
        $ahasil = array();
        foreach($this->__aignore as $_val) {
            if (strpos($_val, "%")===false) {
                $ahasil[] = $_val;
            } else {
                $this->__db->Query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$this->__db->getDBName()."' AND TABLE_NAME LIKE '".$_val."'");
                while ($this->__db->Next()) {
                    $ahasil[] = $this->__db->Row("TABLE_NAME");
                }
            }
        }
        return $ahasil;
    }

    public function create_backup($folder="backups") {
        $namafile = date("Ymd").".gz";
        $folder = $_SESSION['__WEB_APP']['REAL_PATH'].$folder."/".date("Y");
        if (!is_dir($folder)) mkdir($folder);
        if (file_exists($folder."/".$namafile)) unlink($folder."/".$namafile);
        $_cmd = "mysqldump -C --hex-blob -h ".$this->__adata['host']." -P ".$this->__adata['port']." -u ".$this->__adata['user']." -p".$this->__adata['pwd']." ".$this->__adata['db'];
        $itable = $this->ignore_prepare();
        foreach($itable as $_val) $_cmd .= " --ignore-table=".$this->__adata['db'].".".$_val;
        $_cmd .= " | gzip -9 > ".$folder."/".$namafile;
        system($_cmd);
    }

    private function get_file_list($directory) {
        $arrayItems = array();
        $skipByExclude = false;
        $handle = opendir($directory);
        if ($handle) {
            while (false !== ($file = readdir($handle))) {
                preg_match("/(^(([\.]){1,2})$|(\.(svn|git|md))|(Thumbs\.db|\.DS_STORE))$/iu", $file, $skip);
                $skipByExclude = in_array($file, array(",",".."));
                if (!$skip && !$skipByExclude) {
                    if (is_dir($directory. DIRECTORY_SEPARATOR . $file)) {
                        $arrayItems = array_merge($arrayItems, $this->get_file_list($directory. DIRECTORY_SEPARATOR . $file));
                    } else {
                        $file = $directory . DIRECTORY_SEPARATOR . $file;
                        $arrayItems[] = str_replace("\\", "/", $file);
                    }
                }
            }
            closedir($handle);
        }
        return $arrayItems;
    }

    private function format_bytes($bytes) {
       if ($bytes < 1024) return $bytes.' B';
       elseif ($bytes < 1048576) return uang2($bytes / 1024).' KB';
       elseif ($bytes < 1073741824) return uang2($bytes / 1048576).' MB';
       elseif ($bytes < 1099511627776) return uang2($bytes / 1073741824).' GB';
       else return uang2($bytes / 1099511627776).' TB';
    }

    public function disk_space() {
        $_free = disk_free_space($_SESSION['__WEB_APP']['REAL_PATH']);
        $_tot = disk_total_space($_SESSION['__WEB_APP']['REAL_PATH']);
        $_used = $_tot - $_free;
        $_aret = array();
        $_aret['TOTAL'] = $this->format_bytes($_tot);
        $_aret['USED'] = $this->format_bytes($_used);
        $_aret['FREE'] = $this->format_bytes($_free);
        $_aret['USED_P'] = ($_used/$_tot) * 100;
        $_aret['FREE_P'] = ($_free/$_tot) * 100;
        return $_aret; 
    }

    public function update_list($folder="backups") {
        $folder = $_SESSION['__WEB_APP']['REAL_PATH'].$folder;
        $this->__db->Query("TRUNCATE TABLE app_backup");
        $afile = $this->get_file_list($folder);
        foreach($afile as $_key => $_val) {
            if (file_exists($_val)) {
                $_file = str_replace($folder."/", "", $_val);
                $_waktu = date("Y-m-d H:i:s", filemtime($_val));
                $_ukuran = $this->format_bytes(filesize($_val));
                $this->__db->Query("INSERT INTO app_backup SET Nama_File='".$_file."',Tanggal='".$_waktu."',Ukuran='".$_ukuran."'");
            }
        }
    }
}

?>

<?php

/*******************************************************************

    Module        : /DB/CheckSQL.php
    Desc.         : v4 - Class Check SQL / Repair Database
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : May 11th, 2011.
    Last Modified : May 8th, 2021.

    (c) 2011 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

namespace siaupheng\fonia4\DB;

final class CheckSQL {
    private $__atabel = array();
    private $__aignore = array();
    protected $__db = null;

    public function __construct($db = null){
        if (is_resource($db)) { $this->__db = $db; }
            else { $this->__db = new \siaupheng\fonia4\DB\MySQL(); }

        $this->__db->Query("SELECT TABLE_NAME,ENGINE,TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$this->__db->getDBName()."' AND TABLE_TYPE='BASE TABLE'");
        while ($this->__db->Next()) {
            $this->__atabel[] = array($this->__db->Row("TABLE_NAME"), ' <i>('.$this->__db->Row("ENGINE").')</i>',' <i>('.uang($this->__db->Row("TABLE_ROWS")).' Records)</i>');
        }
    }

    public function ignore_table() {
        $tmp_arr = func_get_args();
        if (is_array($tmp_arr[0])) $tmp_arr = $tmp_arr[0];
        $this->__aignore = $tmp_arr;
    }

    private function _check_db($atabel=array()) {
        $hasil = "";
        if (!in_array($atabel[0], $this->__aignore)) {
            $pesan = "<span style=\"color:blue;\">OK</span>";
            $this->__db->Query("CHECK TABLE ".$atabel[0]);
            if ($this->__db->Next()) {
                if (strtoupper($this->__db->Row("Msg_text"))<>"OK") {
                    $pesan = "<span style=\"color:red;\">ERROR</span>";
                    $this->__db->Query("REPAIR TABLE ".$atabel[0]);
                    if ($this->__db->Next()) {
                        if (strtoupper($this->__db->Row("Msg_text"))=="OK") {
                            $pesan .= " -> <span style=\"color:blue;\">FIXED</span>";
                        }
                    }
                }
            }
            $hasil = $atabel[0] . $atabel[2] ."  ... ". $pesan ."<br>";
        }
        return $hasil;
    }

    public function count_db() {
        return count($this->__atabel);
    }

    public function check_db() {
        $hasil = "=== CEK DATABASE ===<br>";
        for ($n=0;$n<count($this->__atabel);$n++) {
            $hasil .= $this->_check_db($this->__atabel[$n]);
        }
        return $hasil;
    }

    public function check_db_step($n) {
        $hasil .= $this->_check_db($this->__atabel[$n]);
    }

    public function repair_db() {
        $hasil = "=== REPAIR DATABASE ===<br>";
        for ($n=0;$n<count($this->__atabel);$n++) {
            if (!in_array($this->__atabel[$n][0], $this->__aignore)) {
                $pesan = "<span style=\"color:blue;\">OK</span>";
                $this->__db->Query("REPAIR TABLE ".$this->__atabel[$n][0]);
                while ($this->__db->Next()) {
                    $pesan = (strtoupper($this->__db->Row("Msg_text"))=="OK") ? "<span style=\"color:blue;\">OK</span>" : "<span style=\"color:red;\">ERROR</span>";
                }
                $hasil .= $this->__atabel[$n][0] . $this->__atabel[$n][1] ."  ... ". $pesan ."<br>";
            }
        }
        return $hasil;
    }

    public function optimize_db() {
        $hasil = "=== OPTIMALISASI DATABASE ===<br>";
        for ($n=0;$n<count($this->__atabel);$n++) {
            if (!in_array($this->__atabel[$n][0], $this->__aignore)) {
                $pesan = "<span style=\"color:blue;\">OK</span>";
                $this->__db->Query("OPTIMIZE TABLE ".$this->__atabel[$n][0]);
                while ($this->__db->Next()) {
                    $pesan = (strtoupper($this->__db->Row("Msg_text"))=="OK" || strtoupper($this->__db->Row("Msg_text"))=="TABLE IS ALREADY UP TO DATE") ? "<span style=\"color:blue;\">OK</span>" : "<span style=\"color:red;\">ERROR</span>";
                }
                $hasil .= $this->__atabel[$n][0] . $this->__atabel[$n][1] ."  ... ". $pesan ."<br>";
            }
        }
        return $hasil;
    }
}

?>

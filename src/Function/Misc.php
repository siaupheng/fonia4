<?php

/*******************************************************************

    Module        : /Function/Misc.php
    Desc.         : v4 - Fungsi Custom Lain Lain
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : January 27th, 2008.
    Last Modified : May 6th, 2021.

    (c) 2008 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

/*************************** FORMAT ANGKA / CURRENCY *****************************/

if (!function_exists('rupiah')) {
    function rupiah($angka = 0, $desi = 0) {
        return number_format(doubleval($angka), intval($desi), ",", ".");
    }
}

if (!function_exists('rupiah2')) {
    function rupiah2($angka = 0) {
        return rupiah($angka, 2);
    }
}

if (!function_exists('rupiah30')) {
    function rupiah30($angka = 0) {
        $nilai = explode(".", $angka);
        $hasil = (intval($nilai[1])==0) ? rupiah($angka, 0) : rupiah($angka, 3);
        return $hasil;
    }
}

if (!function_exists('kurung_rup')) {
    function kurung_rup($angka = 0, $desi = 0) {
        $thasil = rupiah(abs($angka), $desi);
        if ($angka<0) return "(".$thasil.")";
        return $thasil." ";
    }
}

if (!function_exists('kurung')) {
    function kurung($angka = 0, $desi = 0) {
	    $thasil = uang(abs($angka), $desi);
	    if ($angka<0) return "(".$thasil.")";
	    return $thasil." ";
    }
}

if (!function_exists('kurung2')) {
    function kurung2($angka = 0) {
        return kurung($angka, 2);
    }
}

if (!function_exists('uang')) {
    function uang($angka = 0, $desi = 0) {
        $angka = str_replace(",","",$angka);
	    return number_format(doubleval($angka), intval($desi));
    }
}

if (!function_exists('uang2')) {
    function uang2($angka = 0) {
        return uang($angka, 2);
    }
}

if (!function_exists('koma30')) {
    function koma30($angka = 0) {
        $nilai = explode(".", $angka);
        $hasil = (intval($nilai[1])==0) ? uang($angka, 0) : uang($angka, 3);
        return $hasil;
    }
}

if (!function_exists('items')) {
    function items($item = 0) {
        return rupiah($item)." Items";
    }
}

if (!function_exists('records')) {
    function records($item = 0) {
        return rupiah($item)." Records";
    }
}

if (!function_exists('baris')) {
    function baris($item = 0) {
        return rupiah($item)." Baris";
    }
}

if (!function_exists('npwp')) {
    function npwp($nomor = "000000000000000") {
        return substr($nomor,0,2).".".substr($nomor,2,3).".".substr($nomor,5,3).".".substr($nomor,8,1)."-".substr($nomor,9,3).".".substr($nomor,12,3);
    }
}

if (!function_exists('romawi')) {
    function romawi($n) {
        $hasil = "";
        $iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
        if (array_key_exists($n,$iromawi)) {
            $hasil = $iromawi[$n];
        } else if ($n >= 11 && $n <= 99) {
            $i = $n % 10;
            $hasil = $iromawi[$n-$i] . Romawi($n % 10);
        } else if ($n >= 101 && $n <= 999) {
            $i = $n % 100;
            $hasil = $iromawi[$n-$i] . Romawi($n % 100);
        } else {
            $i = $n % 1000;
            $hasil = $iromawi[$n-$i] . Romawi($n % 1000);
        }
        return $hasil;
    }
}

if (!function_exists('abjad')) {
    function abjad($n) {
        $_str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return substr($_str, ($n-1), 1);
    }
}

/*************************** FORM CONTROL *****************************/

if (!function_exists('checkbox')) {
    function checkbox($nilai = 0) {
	    $bhasil = ((is_string($nilai) && ($nilai=="N" || $nilai=="0")) or (is_numeric($nilai) && $nilai==0) or (is_bool($nilai) && $nilai==false)) ? true : false;
	    return '<input class="form-check-input" type="checkbox" '.(($bhasil)?"":"checked").' readonly />';
    }
}

/*************************** FORMAT TANGGAL / KALENDER *****************************/

if (!function_exists('sqltgl')) {
    function sqltgl($waktu = "0") {
        return tgl_sql2str($waktu);
    }
}

if (!function_exists('sqltgls')) {
    function sqltgls($waktu = "0") {
        return tgl_sql2str($waktu, false, true);
    }
}

if (!function_exists('sqljam')) {
    function sqljam($waktu = "0") {
        return tgl_sql2str($waktu, true);
    }
}

if (!function_exists('sqljams')) {
    function sqljams($waktu = "0") {
        return tgl_sql2str($waktu, true, true);
    }
}

if (!function_exists('tgl_sql2str')) {
    function tgl_sql2str($waktu = "0", $jam = false, $short = false) {
	    if ($waktu=="0" || $waktu=="") return;
	    $atgl = explode(" ", $waktu);
	    $sjam = ($jam) ? ((empty($atgl[1]))?date("H:i:s"):$atgl[1]) : "";
	    $atgl = explode("-", $atgl[0]);
	    $stgl = $atgl[2]."-".$atgl[1]."-".$atgl[0];
        if ($short) $stgl = $atgl[2].".".$atgl[1].".".substr($atgl[0],2);
	    return $stgl.(($jam)?" ".$sjam:"");
    }
}

if (!function_exists('tgl_ymd')) {
    function tgl_ymd($jam = false) {
	    return (($jam) ? date("Y-m-d H:i:s") : date("Y-m-d"));
    }
}

if (!function_exists('tgl_dmy')) {
    function tgl_dmy($jam = false) {
	    return (($jam) ? date("d-m-Y H:i:s") : date("d-m-Y"));
    }
}

if (!function_exists('tgl_operasi')) {
    function tgl_operasi($dtgl = "", $nhari = 0, $nbln = 0, $nthn = 0) {
	    $atgl = explode("-", $dtgl);
	    return date("d-m-Y", mktime(0,0,0, $atgl[1]+$nbln, $atgl[0]+$nhari, $atgl[2]+$nthn));
    }
}

if (!function_exists('tgl_range_hari')) {
    function tgl_range_hari($dftgl="", $dttgl="", $bhitung_awal=false) {
        $_start = strtotime(tgl_sql2str($dftgl));
        $_end = strtotime(tgl_sql2str($dttgl));
        return ceil(abs($_end - $_start) / 86400) + (($bhitung_awal==true)?1:0);
    }
}

if (!function_exists('bulan_array')) {
    function bulan_array() {
	    return array("1"=>"Januari","2"=>"Februari","3"=>"Maret","4"=>"April","5"=>"Mei","6"=>"Juni","7"=>"Juli","8"=>"Agustus","9"=>"September","10"=>"Oktober","11"=>"November","12"=>"Desember");
    }
}

if (!function_exists('tgl_akhbln')) {
    function tgl_akhbln($tgl) {
	    $atgl = explode("-", $tgl);
	    $ablna = array(31,28,31,30,31,30,31,31,30,31,30,31);
	    // jika tahun masehi dan bulan 2
	    if ( (round($atgl[2]/4)==($atgl[2]/4)) && intval($atgl[1])==2 ) return "29-02-".$atgl[2];
	    return $ablna[(intval($atgl[1])-1)]."-".isi_nol($atgl[1],2)."-".$atgl[2];
    }
}

if (!function_exists('tgl_panjang')) {
    function tgl_panjang($dtgl = "") {
        if (trim($dtgl)=="") $dtgl = tgl_dmy();
        $atgl = explode("-", $dtgl);
        $abulan = bulan_array();
        return $atgl[0]." ".$abulan[intval($atgl[1])]." ".$atgl[2];
    }
}

if (!function_exists('tgl_hari')) {
    function tgl_hari($dtgl = "") {
        $nhari = date("w");
        if (trim($dtgl)!="") {
            //$atgl = getdate($dtgl);
            //$nhari = $atgl[wday];
            $nhari = date("w", strtotime(tgl_sql2str($dtgl)));
        }
        $ahari = array("0"=>"Minggu","1"=>"Senin","2"=>"Selasa","3"=>"Rabu","4"=>"Kamis","5"=>"Jum'at","6"=>"Sabtu");
        return $ahari[$nhari];
    }
}
if (!function_exists('tgl_triwulan')) {
    function tgl_triwulan($dtgl = "") {
        if ($dtgl == "") {
            $dtgl = date("m");
        } else {
            $atgl = explode("-", $tgl);
            $dtgl = $atgl[1];
        }
        $hasil = 0;
        if ($dtgl >= 1 && $dtgl <= 3) $hasil = 1;
        if ($dtgl >= 4 && $dtgl <= 6) $hasil = 2;
        if ($dtgl >= 7 && $dtgl <= 9) $hasil = 3;
        if ($dtgl >= 10 && $dtgl <= 12) $hasil = 4;
        return $hasil;
    }
}

/*************************** FORMAT TAMPILAN & ERROR *****************************/

if (!function_exists('subjudul')) {
    function subjudul($tjudul) {
	    return '<table border="0" align="center" valign="middle" width="100%" height="26px"><tr><td width="50%"><hr /></td><td>&nbsp;&nbsp;<span style="text-align:center;font-weight:bold;white-space:nowrap;">'.$tjudul.'</span>&nbsp;&nbsp;</td><td width="50%"><hr /></td></tr></table>';
    }
}

if (!function_exists('showerror')) {
    function showerror($terror) {
        echo '<input type="hidden" id="_Error" class="error" value="'.$terror.'!" />';
    }
}

if (!function_exists('br2nl')) {
    function br2nl($string) {
        return preg_replace('/\<br(\s*)?\/?\>/i', "\n\r", $string);
    }
}

if (!function_exists('isi_nol')) {
    function isi_nol($nilai = "0", $panjang = 1) {
        $nilai = strval($nilai);
	    return str_repeat("0", $panjang-strlen($nilai)).$nilai;
    }
}

if (!function_exists('isLinux')) {
    function isLinux() {
        $agen = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (stripos($agen, 'linux') !== false) return true;
        return false;
    }
}

if (!function_exists('isIE')) {
    function isIE() {
        $agen = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (stripos($agen,'msie') !== false) return true;
        return false;
    }
}

if (!function_exists('isChrome')) {
    function isChrome() {
        $agen = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (stripos($agen,'chrome') !== false) return true;
        return false;
    }
}

if (!function_exists('isFirefox')) {
    function isFirefox() {
        $agen = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (stripos($agen,'firefox') !== false) return true;
        return false;
    }
}

if (!function_exists('potong')) {
    function potong($nilai) {//utk rkpd
        return substr($nilai, 0, 60);
    }
}

if (!function_exists('format_bytes')) {
    function format_bytes($bytes) {
        if ($bytes < 1024) return $bytes.' B';
        elseif ($bytes < 1048576) return uang2($bytes / 1024).' KB';
        elseif ($bytes < 1073741824) return uang2($bytes / 1048576).' MB';
        elseif ($bytes < 1099511627776) return uang2($bytes / 1073741824).' GB';
        else return uang2($bytes / 1099511627776).' TB';
    }
}

?>

<?php

/*******************************************************************

    Module        : /Function/JSON.php
    Desc.         : v4 - Fungsi Custom JSON
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : April 14th, 2012.
    Last Modified : April 29th, 2022.

    (c) 2012 - 2022, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

if (!function_exists('is_json')) {
    function is_json($string) {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}

if (!function_exists('send_json')) {
    function send_json($ajson = array(), $bempty = false) {
        if (count($ajson) > 0 || $bempty == true) {
            header('Cache-Control: no-cache, must-revalidate');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Content-type: application/json');
            echo json_encode($ajson);
        }
    }
}

?>

<?php

/*******************************************************************

    Module        : /Function/Auth.php
    Desc.         : v4 - Fungsi Custom Auth
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : January 27th, 2008.
    Last Modified : May 6th, 2021.

    (c) 2008 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

declare(strict_types=1);

if (!function_exists('user_level')) {
	function user_level($level = 9) {
		if ($_SESSION['__WEB_USER']['Level'] <= $level) return true;
		return false;
	}
}

if (!function_exists('user_logged')) {
	function user_logged() {
		if (isset($_SESSION['__WEB_USER'])) return true;
		return false;
	}
}

if (!function_exists('user_id')) {
	function user_id($idigit = 0) {
		if ($idigit > 0) return isi_nol($_SESSION['__WEB_USER']['UserID'], $idigit);
		return $_SESSION['__WEB_USER']['UserID'];
	}
}

if (!function_exists('user_user')) {
	function user_user() {
		return $_SESSION['__WEB_USER']['Username'];
	}
}

if (!function_exists('user_unix')) {
	function user_unix() {
		return $_SESSION['__WEB_USER']['UNIX'];
	}
}

if (!function_exists('user_nama')) {
	function user_nama() {
		return $_SESSION['__WEB_USER']['Nama'];
	}
}

?>

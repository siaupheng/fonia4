/*******************************************************************

    Module        : /assets/js/print.js
    Desc.         : JS Class Printer & Printer POS
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : May 27th, 2007.
    Last Modified : January 26th, 2021.

    (c) 2008 - 2021, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

var cPRINTER = function(url, options){
    var vars = {
        'url': '',
        'zoom': 90,
        'param': []
    };
    this.construct = function(url, options){
        $.extend(vars, options);
        vars.url = url;
    };
    this.set_url = function (url){
        vars.url = url;
    };
    this.set_zoom = function (zoom){
        if (zoom) vars.zoom = zoom;
    },
    this.add_param = function (par_id, par_val){
        var _getvalue = (par_val) ? par_id + "=" + escape(par_val) : par_id;
        vars.param.push(_getvalue);
    },
    this.doPrint = function(){
        if (!vars.url) {
            console.warn(vars.param + ", cek url php!");
            return false;
        }
        vars.data = FoniaJS.RequestParam(vars.param);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: function(txt){
                var $printData = $('#dataPrint');
                if (!$printData.length) {
                    var $printData = $('<div/>', {id:'dataPrint','class':'d-none d-print-block print-body'}).appendTo('body');
                }
                $printData.html(txt);
                window.print();
            },
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
        });
        return this;
    };
    this.doPreview = function(){
        if (!vars.url) console.warn(vars.param + ", cek url php!");
        vars.data = FoniaJS.RequestParam(vars.param);
        $.ajax({
            type: 'POST',
            url: vars.url,
            data: (vars.data).join('&'),
            success: function(txt){
                if ('' == txt) {
                    FoniaJS.showInfo('Tidak ada data untuk pembuatan laporan !');
                    return false;
                }
                var _wtxt = 
                    '<div class="modal fade" id="winAPPPrint" tabindex="-1" role="dialog" data-coreui-keyboard="false" data-coreui-show="false" data-coreui-backdrop="static" aria-labelledby="winAPPPrintLabel" aria-hidden="true">'+
                    '<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-fs" role="document">'+
                    '<div class="modal-content">'+
                    '<div class="modal-header"><h5 class="modal-title" id="winAPPPrintLabel">Preview Laporan</h5><div class="ml-auto"><button class="btn btn-cetak" id="tombCetak" type="button" aria-label="Cetak"><i class="cui-print"></i><span aria-hidden="true">&nbsp;Cetak</span></button><button class="btn btn-cetak" type="button" data-coreui-dismiss="modal" aria-label="Tutup"><i class="cui-x"></i><span aria-hidden="true">&nbsp;Tutup</span></button></div></div>'+
                    '<div class="modal-body print-body" id="divPrintP">'+txt+'</div>'+
                    '</div></div></div>';
                if ($('#winAPPPrint')) $('#winAPPPrint').modal('dispose').remove();
                $(_wtxt).on('shown.coreui.modal', function(e){
                    $('#winAPPPrint').find('#tombCetak').click(function(){
                        var $printData = $('#dataPrint');
                        if (!$printData.length) {
                            var $printData = $('<div/>', {id:'dataPrint','class':'d-none d-print-block print-body'}).appendTo('body');
                        }
                        $printData.html($('#divPrintP').html());
                        FoniaJS.closeWindow($('#winAPPPrint'));
                        window.print();
                    });
                }).on('hidden.coreui.modal', function(e){
                    $(this).remove();
                }).modal('show');
            },
            error: (typeof vars.onFailure === "function") ? vars.onFailure : null,
        });
    };
    this.construct(url, options);
};

/*
    showPreview: function() {
            toolbar: true,
            toolbarPosition: "top",
            toolbarHeight: 35,
            toolbarMenu: "PP,_,PS,PN,PB",
            toolbarOnload: function() {
                $("butPPrint").addEvent("click", function() {
                    if (this.winPreview) {
                        this.winPreview.close();
                    }
                    window.print();
                }.bind(this));
                $("butPNormal").addEvent("click", function() {
                    this.winPreview.options.zoomfont = 100;
                    $("winPreview_content").setStyle("font-size", this.winPreview.options.zoomfont + "%");
                }.bind(this));
                $("butPSmall").addEvent("click", function() {
                    this.winPreview.options.zoomfont -= 10;
                    $("winPreview_content").setStyle("font-size", this.winPreview.options.zoomfont + "%");
                }.bind(this));
                $("butPBig").addEvent("click", function() {
                    this.winPreview.options.zoomfont += 10;
                    $("winPreview_content").setStyle("font-size", this.winPreview.options.zoomfont + "%");
                }.bind(this));
                $("winPreview_content").setStyle("font-size", this.options.zoom + "%");
            }.bind(this)
        });
    },
});*/

var cPRINTERPOS = function(url, options) {
    var b_default = false;
    var _options = {
        'url': '',
        'param': [],
        'printer': null
    };
    this.construct = function(url, options) {
        $.extend(_options, options);
        _options.url = url;
        if (FoniaJS._def_printer) _options.printer = FoniaJS._def_printer;
        /*if ($('#configPrinter')) {
            _options.printer = $('#configPrinter').html();
        }*/
    };
    this.detectPrinter = function() {
        if (!qz.websocket.isActive()) {
            FoniaJS.showInfo('Status Printer belum Active !');
            return false;
        }
        return true;
    };
    this.getPrinters = function() {
        return [];
    };
    this.setPrinter = function(nprint) {
        (nprint != null) && (_options.printer = nprint);
    };
    this.useDefaultPrinter = function() {
        b_default = true;
    };
    this.set_url = function(url) {
        _options.url = url;
    };
    this.add_param = function(par_id, par_val) {
        var _getvalue = (par_val) ? par_id + '=' + escape(par_val) : par_id;
        _options.param.push(_getvalue);
    };
    this.exec = function() {
        if (!this.detectPrinter()) return;
        if (this.b_default) {
            var parent = this;
            qz.printers.getDefault().then(function(data) {
                var nama_p = data;
                if (data.name != undefined) {
                    nama_p = data.name;
                }
                /*if ($('#configPrinter')) {
                    $('#configPrinter').html(nama_p);
                }*/
                parent._options.param.push('Printer='+nama_p);
                return parent.do_print(nama_p, parent._options.url, parent._options.param);
            }, function(data) {
                FoniaJS.showInfo(data);
            });
        } else {
            var _nprinter = _options.printer;
            if (typeof _nprinter === 'object' && _nprinter.name == undefined) {
                if (_nprinter.file != undefined) _nprinter = _nprinter.file;
                if (_nprinter.host != undefined) _nprinter = _nprinter.host + ":" + _nprinter.port;
            } else {
                if (_nprinter.name != undefined) _nprinter = _nprinter.name;
                if (_nprinter== undefined) _nprinter = 'None';
            }
            _options.param.push('Printer='+_nprinter);
            this.do_print(_options.printer, _options.url, _options.param);
        }
    };
    this.do_print = function(nama_p, url, params) {
        var qzcfg = qz.configs.create(nama_p);
        new Request.JSON({
            url: url,
            sendParam: params,
            onSuccess: function(ajson) {
                if (typeof ajson === "object") {
                    var hasil_cetak = (ajson.type == "escp") ? this.gen_escp(ajson) : this.gen_epl(ajson);
                    qz.print(qzcfg, hasil_cetak);
                } else {
                    FoniaJS.showInfo('Error dalam proses Cetak.');
                }
            }.bind(this)
        }).send();
    };
    this.gen_escp = function(adata) {
        var cut_paper = "\x1D\x56\x41";
        var open_draw = "\x1B\x70\x30\x19\x19";
        var nv_image = "\x1D\x28\x4C\x06\x00\x30\x45\x30\x30\x01\x01";
        var chr_norm = "\x1B\x21\x01";
        var chr_bold = "\x1B\x21\x10";
        var chr_sbold = "\x1B\x21\x31";
        var chr_uline = "\x1B\x21\x80";
        var ohead = [];
        var osheet = [], ocetak = [];
        ohead.push("\x1B\x40");
        ohead.push("\x1B\x61\x01");
        if (adata.config.logo != null) {
            ohead.push({type:'raw',format:'image',data:adata.config.logo,options:{language:'ESCPOS',dotDensity:'double'}});
            ohead.push("\x1B\x32");
        }
        if (parseInt(adata.config.fontsize) == 2) {
            chr_norm = "\x1B\x21\x02";
        }
        if (adata.config.nvlogo) {
            ohead.push((this.options.nv_cmd?this.options.nv_cmd:nv_image));
            ohead.push(chr_norm);
        }
        if (adata.header.length > 0) {
            for (i = 0; i < adata.header.length; i++) {
                if (i==0 && !adata.config.nvlogo && adata.config.logo==null) ohead.push(chr_bold);
                ohead.push(adata.header[i] + "\x0A");
                if (i==0 && !adata.config.nvlogo && adata.config.logo==null) ohead.push(chr_norm);
            }
        }
        for (s = 0; s < adata.content.length; s++) {
            if (adata.content[s].length == 0) continue;
            osheet = ohead.slice(0);
            for (i = 0; i < adata.content[s].length; i++) {
                var tmp_cont = adata.content[s][i];
                tmp_cont = tmp_cont.replace('_SBOLD_', chr_sbold)
                    .replace('_BOLD_', chr_bold)
                    .replace('_NORM_', chr_norm)
                    .replace('_ULINE_', chr_uline);
                osheet.push(tmp_cont + "\x0A");
            }
            if (adata.config.cutpaper) {
                osheet.push(cut_paper + "\x0A");
            }
            //osheet.push("\x1B\x65\x02");//feed up 2row
            ocetak = ocetak.concat(osheet);
        }
        var notaout = [];
        if (adata.config.opendraw && adata.config.opendrawbegin) {
            notaout.push(open_draw);
        }
        for (i = 0; i < adata.config.times; i++) {
            notaout = notaout.concat(ocetak);
        }
        if (adata.config.opendraw && adata.config.opendrawbegin == false) {
            notaout.push(open_draw);
        }
        return notaout;
    };
    this.gen_epl = function(adata) {
        var cetak = [];
        for (i = 0; i < adata.content.length; i++) {
            cetak.push(adata.content[i] + "\r\n");
        }
        var notaout = [];
        for (i = 0; i < adata.config.times; i++) {
            notaout = notaout.concat(cetak);
        }
        return notaout;
    };
    this.construct(url, options);
};

/*!
 * CoreUI v4.2.0-beta.1 (https://coreui.io)
 * Copyright 2022 The CoreUI Team (https://github.com/orgs/coreui/people)
 * Licensed under MIT (https://coreui.io)
 */
!(function (e, t) {
    "object" == typeof exports && "undefined" != typeof module ? (module.exports = t()) : "function" == typeof define && define.amd ? define(t) : ((e = "undefined" != typeof globalThis ? globalThis : e || self).coreui = t());
})(this, function () {
    "use strict";
    const e = "transitionend",
        t = (e) => {
            let t = e.getAttribute("data-coreui-target");
            if (!t || "#" === t) {
                let i = e.getAttribute("href");
                if (!i || (!i.includes("#") && !i.startsWith("."))) return null;
                i.includes("#") && !i.startsWith("#") && (i = `#${i.split("#")[1]}`), (t = i && "#" !== i ? i.trim() : null);
            }
            return t;
        },
        i = (e) => {
            const i = t(e);
            return i && document.querySelector(i) ? i : null;
        },
        n = (e) => {
            const i = t(e);
            return i ? document.querySelector(i) : null;
        },
        s = (t) => {
            t.dispatchEvent(new Event(e));
        },
        o = (e) => !(!e || "object" != typeof e) && (void 0 !== e.jquery && (e = e[0]), void 0 !== e.nodeType),
        a = (e) => (o(e) ? (e.jquery ? e[0] : e) : "string" == typeof e && e.length > 0 ? document.querySelector(e) : null),
        r = (e, t, i) => {
            Object.keys(i).forEach((n) => {
                const s = i[n],
                    a = t[n],
                    r =
                        a && o(a)
                            ? "element"
                            : null == (l = a)
                            ? `${l}`
                            : Object.prototype.toString
                                  .call(l)
                                  .match(/\s([a-z]+)/i)[1]
                                  .toLowerCase();
                var l;
                if (!new RegExp(s).test(r)) throw new TypeError(`${e.toUpperCase()}: Option "${n}" provided type "${r}" but expected type "${s}".`);
            });
        },
        l = (e) => {
            if (!o(e) || 0 === e.getClientRects().length) return !1;
            const t = "visible" === getComputedStyle(e).getPropertyValue("visibility"),
                i = e.closest("details:not([open])");
            if (!i) return t;
            if (i !== e) {
                const t = e.closest("summary");
                if (t && t.parentNode !== i) return !1;
                if (null === t) return !1;
            }
            return t;
        },
        c = (e) => !e || e.nodeType !== Node.ELEMENT_NODE || !!e.classList.contains("disabled") || (void 0 !== e.disabled ? e.disabled : e.hasAttribute("disabled") && "false" !== e.getAttribute("disabled")),
        h = (e) => {
            if (!document.documentElement.attachShadow) return null;
            if ("function" == typeof e.getRootNode) {
                const t = e.getRootNode();
                return t instanceof ShadowRoot ? t : null;
            }
            return e instanceof ShadowRoot ? e : e.parentNode ? h(e.parentNode) : null;
        },
        d = () => {},
        u = (e) => {
            e.offsetHeight;
        },
        p = () => {
            const { jQuery: e } = window;
            return e && !document.body.hasAttribute("data-coreui-no-jquery") ? e : null;
        },
        f = [],
        _ = () => "rtl" === document.documentElement.dir,
        g = (e) => {
            var t;
            (t = () => {
                const t = p();
                if (t) {
                    const i = e.NAME,
                        n = t.fn[i];
                    (t.fn[i] = e.jQueryInterface), (t.fn[i].Constructor = e), (t.fn[i].noConflict = () => ((t.fn[i] = n), e.jQueryInterface));
                }
            }),
                "loading" === document.readyState
                    ? (f.length ||
                          document.addEventListener("DOMContentLoaded", () => {
                              f.forEach((e) => e());
                          }),
                      f.push(t))
                    : t();
        },
        m = (e) => {
            "function" == typeof e && e();
        },
        v = (t, i, n = !0) => {
            if (!n) return void m(t);
            const o =
                ((e) => {
                    if (!e) return 0;
                    let { transitionDuration: t, transitionDelay: i } = window.getComputedStyle(e);
                    const n = Number.parseFloat(t),
                        s = Number.parseFloat(i);
                    return n || s ? ((t = t.split(",")[0]), (i = i.split(",")[0]), 1e3 * (Number.parseFloat(t) + Number.parseFloat(i))) : 0;
                })(i) + 5;
            let a = !1;
            const r = ({ target: n }) => {
                n === i && ((a = !0), i.removeEventListener(e, r), m(t));
            };
            i.addEventListener(e, r),
                setTimeout(() => {
                    a || s(i);
                }, o);
        },
        b = (e, t, i, n) => {
            let s = e.indexOf(t);
            if (-1 === s) return e[!i && n ? e.length - 1 : 0];
            const o = e.length;
            return (s += i ? 1 : -1), n && (s = (s + o) % o), e[Math.max(0, Math.min(s, o - 1))];
        },
        y = /[^.]*(?=\..*)\.|.*/,
        w = /\..*/,
        E = /::\d+$/,
        D = {};
    let L = 1;
    const T = { mouseenter: "mouseover", mouseleave: "mouseout" },
        k = /^(mouseenter|mouseleave)/i,
        C = new Set([
            "click",
            "dblclick",
            "mouseup",
            "mousedown",
            "contextmenu",
            "mousewheel",
            "DOMMouseScroll",
            "mouseover",
            "mouseout",
            "mousemove",
            "selectstart",
            "selectend",
            "keydown",
            "keypress",
            "keyup",
            "orientationchange",
            "touchstart",
            "touchmove",
            "touchend",
            "touchcancel",
            "pointerdown",
            "pointermove",
            "pointerup",
            "pointerleave",
            "pointercancel",
            "gesturestart",
            "gesturechange",
            "gestureend",
            "focus",
            "blur",
            "change",
            "reset",
            "select",
            "submit",
            "focusin",
            "focusout",
            "load",
            "unload",
            "beforeunload",
            "resize",
            "move",
            "DOMContentLoaded",
            "readystatechange",
            "error",
            "abort",
            "scroll",
        ]);
    function A(e, t) {
        return (t && `${t}::${L++}`) || e.uidEvent || L++;
    }
    function O(e) {
        const t = A(e);
        return (e.uidEvent = t), (D[t] = D[t] || {}), D[t];
    }
    function S(e, t, i = null) {
        const n = Object.keys(e);
        for (let s = 0, o = n.length; s < o; s++) {
            const o = e[n[s]];
            if (o.originalHandler === t && o.delegationSelector === i) return o;
        }
        return null;
    }
    function x(e, t, i) {
        const n = "string" == typeof t,
            s = n ? i : t;
        let o = M(e);
        return C.has(o) || (o = e), [n, s, o];
    }
    function I(e, t, i, n, s) {
        if ("string" != typeof t || !e) return;
        if ((i || ((i = n), (n = null)), k.test(t))) {
            const e = (e) =>
                function (t) {
                    if (!t.relatedTarget || (t.relatedTarget !== t.delegateTarget && !t.delegateTarget.contains(t.relatedTarget))) return e.call(this, t);
                };
            n ? (n = e(n)) : (i = e(i));
        }
        const [o, a, r] = x(t, i, n),
            l = O(e),
            c = l[r] || (l[r] = {}),
            h = S(c, a, o ? i : null);
        if (h) return void (h.oneOff = h.oneOff && s);
        const d = A(a, t.replace(y, "")),
            u = o
                ? (function (e, t, i) {
                      return function n(s) {
                          const o = e.querySelectorAll(t);
                          for (let { target: a } = s; a && a !== this; a = a.parentNode) for (let r = o.length; r--; ) if (o[r] === a) return (s.delegateTarget = a), n.oneOff && $.off(e, s.type, t, i), i.apply(a, [s]);
                          return null;
                      };
                  })(e, i, n)
                : (function (e, t) {
                      return function i(n) {
                          return (n.delegateTarget = e), i.oneOff && $.off(e, n.type, t), t.apply(e, [n]);
                      };
                  })(e, i);
        (u.delegationSelector = o ? i : null), (u.originalHandler = a), (u.oneOff = s), (u.uidEvent = d), (c[d] = u), e.addEventListener(r, u, o);
    }
    function N(e, t, i, n, s) {
        const o = S(t[i], n, s);
        o && (e.removeEventListener(i, o, Boolean(s)), delete t[i][o.uidEvent]);
    }
    function M(e) {
        return (e = e.replace(w, "")), T[e] || e;
    }
    const $ = {
            on(e, t, i, n) {
                I(e, t, i, n, !1);
            },
            one(e, t, i, n) {
                I(e, t, i, n, !0);
            },
            off(e, t, i, n) {
                if ("string" != typeof t || !e) return;
                const [s, o, a] = x(t, i, n),
                    r = a !== t,
                    l = O(e),
                    c = t.startsWith(".");
                if (void 0 !== o) {
                    if (!l || !l[a]) return;
                    return void N(e, l, a, o, s ? i : null);
                }
                c &&
                    Object.keys(l).forEach((i) => {
                        !(function (e, t, i, n) {
                            const s = t[i] || {};
                            Object.keys(s).forEach((o) => {
                                if (o.includes(n)) {
                                    const n = s[o];
                                    N(e, t, i, n.originalHandler, n.delegationSelector);
                                }
                            });
                        })(e, l, i, t.slice(1));
                    });
                const h = l[a] || {};
                Object.keys(h).forEach((i) => {
                    const n = i.replace(E, "");
                    if (!r || t.includes(n)) {
                        const t = h[i];
                        N(e, l, a, t.originalHandler, t.delegationSelector);
                    }
                });
            },
            trigger(e, t, i) {
                if ("string" != typeof t || !e) return null;
                const n = p(),
                    s = M(t),
                    o = t !== s,
                    a = C.has(s);
                let r,
                    l = !0,
                    c = !0,
                    h = !1,
                    d = null;
                return (
                    o && n && ((r = n.Event(t, i)), n(e).trigger(r), (l = !r.isPropagationStopped()), (c = !r.isImmediatePropagationStopped()), (h = r.isDefaultPrevented())),
                    a ? ((d = document.createEvent("HTMLEvents")), d.initEvent(s, l, !0)) : (d = new CustomEvent(t, { bubbles: l, cancelable: !0 })),
                    void 0 !== i &&
                        Object.keys(i).forEach((e) => {
                            Object.defineProperty(d, e, { get: () => i[e] });
                        }),
                    h && d.preventDefault(),
                    c && e.dispatchEvent(d),
                    d.defaultPrevented && void 0 !== r && r.preventDefault(),
                    d
                );
            },
        },
        P = new Map(),
        H = {
            set(e, t, i) {
                P.has(e) || P.set(e, new Map());
                const n = P.get(e);
                n.has(t) || 0 === n.size ? n.set(t, i) : console.error(`Bootstrap doesn't allow more than one instance per element. Bound instance: ${Array.from(n.keys())[0]}.`);
            },
            get: (e, t) => (P.has(e) && P.get(e).get(t)) || null,
            remove(e, t) {
                if (!P.has(e)) return;
                const i = P.get(e);
                i.delete(t), 0 === i.size && P.delete(e);
            },
        };
    class j {
        constructor(e) {
            (e = a(e)) && ((this._element = e), H.set(this._element, this.constructor.DATA_KEY, this));
        }
        dispose() {
            H.remove(this._element, this.constructor.DATA_KEY),
                $.off(this._element, this.constructor.EVENT_KEY),
                Object.getOwnPropertyNames(this).forEach((e) => {
                    this[e] = null;
                });
        }
        _queueCallback(e, t, i = !0) {
            v(e, t, i);
        }
        static getInstance(e) {
            return H.get(a(e), this.DATA_KEY);
        }
        static getOrCreateInstance(e, t = {}) {
            return this.getInstance(e) || new this(e, "object" == typeof t ? t : null);
        }
        static get VERSION() {
            return "4.2.0-beta.1";
        }
        static get NAME() {
            throw new Error('You have to implement the static method "NAME", for each component!');
        }
        static get DATA_KEY() {
            return `coreui.${this.NAME}`;
        }
        static get EVENT_KEY() {
            return `.${this.DATA_KEY}`;
        }
    }
    const B = (e, t = "hide") => {
        const i = `click.dismiss${e.EVENT_KEY}`,
            s = e.NAME;
        $.on(document, i, `[data-coreui-dismiss="${s}"]`, function (i) {
            if ((["A", "AREA"].includes(this.tagName) && i.preventDefault(), c(this))) return;
            const o = n(this) || this.closest(`.${s}`);
            e.getOrCreateInstance(o)[t]();
        });
    };
    class R extends j {
        static get NAME() {
            return "alert";
        }
        close() {
            if ($.trigger(this._element, "close.coreui.alert").defaultPrevented) return;
            this._element.classList.remove("show");
            const e = this._element.classList.contains("fade");
            this._queueCallback(() => this._destroyElement(), this._element, e);
        }
        _destroyElement() {
            this._element.remove(), $.trigger(this._element, "closed.coreui.alert"), this.dispose();
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = R.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    B(R, "close"), g(R);
    const W = '[data-coreui-toggle="button"]';
    class z extends j {
        static get NAME() {
            return "button";
        }
        toggle() {
            this._element.setAttribute("aria-pressed", this._element.classList.toggle("active"));
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = z.getOrCreateInstance(this);
                "toggle" === e && t[e]();
            });
        }
    }
    function V(e) {
        return "true" === e || ("false" !== e && (e === Number(e).toString() ? Number(e) : "" === e || "null" === e ? null : e));
    }
    function F(e) {
        return e.replace(/[A-Z]/g, (e) => `-${e.toLowerCase()}`);
    }
    $.on(document, "click.coreui.button.data-api", W, (e) => {
        e.preventDefault();
        const t = e.target.closest(W);
        z.getOrCreateInstance(t).toggle();
    }),
        g(z);
    const q = {
            setDataAttribute(e, t, i) {
                e.setAttribute(`data-coreui-${F(t)}`, i);
            },
            removeDataAttribute(e, t) {
                e.removeAttribute(`data-coreui-${F(t)}`);
            },
            getDataAttributes(e) {
                if (!e) return {};
                const t = {};
                return (
                    Object.keys(e.dataset)
                        .filter((e) => e.startsWith("coreui"))
                        .forEach((i) => {
                            let n = i.replace(/^coreui/, "");
                            (n = n.charAt(0).toLowerCase() + n.slice(1, n.length)), (t[n] = V(e.dataset[i]));
                        }),
                    t
                );
            },
            getDataAttribute: (e, t) => V(e.getAttribute(`data-coreui-${F(t)}`)),
            offset(e) {
                const t = e.getBoundingClientRect();
                return { top: t.top + window.pageYOffset, left: t.left + window.pageXOffset };
            },
            position: (e) => ({ top: e.offsetTop, left: e.offsetLeft }),
        },
        U = (e, t) => {
            const i = Math.ceil(e.length / t);
            return Array.from({ length: t })
                .fill("")
                .map((t, n) => e.slice(n * i, (n + 1) * i));
        },
        Y = (e, t, i, n) => {
            let s;
            return (
                n &&
                    n.forEach((t) => {
                        Array.isArray(t) && K(e, t[0], t[1]) && (s = !0), t instanceof Date && Z(e, t) && (s = !0);
                    }),
                t && e < t && (s = !0),
                i && e > i && (s = !0),
                s
            );
        },
        K = (e, t, i) => t && i && t <= e && e <= i,
        Q = (e, t, i) => (t && Z(t, e)) || (i && Z(i, e)),
        X = (e, t, i) => t && i && Z(i, e) && t < i,
        G = (e) => {
            const t = new Date(e.getTime()),
                i = t.getMonth();
            return t.setDate(t.getDate() + 1), t.getMonth() !== i;
        },
        Z = (e, t) => e.getDate() === t.getDate() && e.getMonth() === t.getMonth() && e.getFullYear() === t.getFullYear(),
        J = (e, t, i) => t && i && Z(t, e) && t < i,
        ee = (e) => {
            const t = new Date();
            return e.getDate() === t.getDate() && e.getMonth() === t.getMonth() && e.getFullYear() === t.getFullYear();
        },
        te = "calendar",
        ie = ".coreui.calendar",
        ne = `calendarDateChange${ie}`,
        se = `cellHover${ie}`,
        oe = `endDateChange${ie}`,
        ae = `load${ie}.data-api`,
        re = `mouseenter${ie}`,
        le = `mouseleave${ie}`,
        ce = `startDateChange${ie}`,
        he = ".calendar-cell-inner",
        de = { calendarDate: new Date(), disabledDates: null, endDate: null, firstDayOfWeek: 1, locale: navigator.language, maxDate: null, minDate: null, range: !0, startDate: null, selectEndDate: !1, weekdayLength: 2 },
        ue = {
            calendarDate: "(date|string|null)",
            disabledDates: "(array|null)",
            endDate: "(date|string|null)",
            firstDayOfWeek: "number",
            locale: "string",
            maxDate: "(date|string|null)",
            minDate: "(date|string|null)",
            range: "boolean",
            startDate: "(date|string|null)",
            selectEndDate: "boolean",
            weekdayLength: "number",
        };
    class pe extends j {
        constructor(e, t) {
            super(e),
                (this._config = this._getConfig(t)),
                (this._calendarDate = this._config.calendarDate),
                (this._startDate = this._config.startDate),
                (this._endDate = this._config.endDate),
                (this._selectEndDate = this._config.selectEndDate),
                (this._view = "days"),
                this._createCalendar(),
                this._addEventListeners();
        }
        static get Default() {
            return de;
        }
        static get DefaultType() {
            return ue;
        }
        static get NAME() {
            return te;
        }
        _addEventListeners() {
            $.on(this._element, "click", he, (e) => {
                e.preventDefault(),
                    e.target.classList.contains("day") && this._selectDate(q.getDataAttribute(e.target, "date")),
                    e.target.classList.contains("month") && (this._setCalendarDate(new Date(this._calendarDate.getFullYear(), q.getDataAttribute(e.target, "month"), 1)), (this._view = "days")),
                    e.target.classList.contains("year") && ((this._calendarDate = new Date(q.getDataAttribute(e.target, "year"), this._calendarDate.getMonth(), 1)), (this._view = "months")),
                    this._updateCalendar();
            }),
                $.on(this._element, re, he, (e) => {
                    e.preventDefault(), e.target.parentElement.classList.contains("disabled") || $.trigger(this._element, se, { date: new Date(q.getDataAttribute(e.target, "date")) });
                }),
                $.on(this._element, le, he, (e) => {
                    e.preventDefault(), $.trigger(this._element, se, { date: null });
                }),
                $.on(this._element, "click", ".btn-prev", (e) => {
                    e.preventDefault(), this._modifyCalendarDate(0, -1);
                }),
                $.on(this._element, "click", ".btn-double-prev", (e) => {
                    e.preventDefault(), this._modifyCalendarDate(-1);
                }),
                $.on(this._element, "click", ".btn-next", (e) => {
                    e.preventDefault(), this._modifyCalendarDate(0, 1);
                }),
                $.on(this._element, "click", ".btn-double-next", (e) => {
                    e.preventDefault(), this._modifyCalendarDate(1);
                }),
                $.on(this._element, "click", ".btn-month", (e) => {
                    e.preventDefault(), (this._view = "months"), (this._element.innerHTML = ""), this._createCalendar();
                }),
                $.on(this._element, "click", ".btn-year", (e) => {
                    e.preventDefault(), (this._view = "years"), (this._element.innerHTML = ""), this._createCalendar();
                });
        }
        _setCalendarDate(e) {
            (this._calendarDate = e), $.trigger(this._element, ne, { date: e });
        }
        _modifyCalendarDate(e, t = 0) {
            const i = this._calendarDate.getFullYear(),
                n = this._calendarDate.getMonth(),
                s = new Date(i, n, 1);
            e && s.setFullYear(s.getFullYear() + e), t && s.setMonth(s.getMonth() + t), (this._calendarDate = s), $.trigger(this._element, ne, { date: s }), this._updateCalendar();
        }
        _setEndDate(e, t = !1) {
            (this._endDate = new Date(e)), $.trigger(this._element, oe, { date: this._endDate, selectEndDate: t });
        }
        _setStartDate(e, t = !0) {
            (this._startDate = new Date(e)), $.trigger(this._element, ce, { date: this._startDate, selectEndDate: t });
        }
        _selectDate(e) {
            Y(e, this._config.minDate, this._config.maxDate, this._config.disabledDates) ||
                (this._config.range
                    ? this._selectEndDate
                        ? this._startDate && this._startDate > new Date(e)
                            ? (this._setEndDate(this._startDate), this._setStartDate(e))
                            : this._setEndDate(e)
                        : this._setStartDate(e, !0)
                    : this._setStartDate(e));
        }
        _createCalendar() {
            const { firstDayOfWeek: e, locale: t, weekdayLength: i } = this._config,
                n = this._calendarDate.getFullYear(),
                s = this._calendarDate.getMonth(),
                o = document.createElement("div");
            o.classList.add("calendar-nav"),
                (o.innerHTML = `\n      <div class="calendar-nav-prev">\n        <button class="btn btn-transparent btn-sm btn-double-prev">\n          <span class="calendar-nav-icon calendar-nav-icon-double-prev"></span>\n        </button>\n        <button class="btn btn-transparent btn-sm btn-prev">\n          <span class="calendar-nav-icon calendar-nav-icon-prev"></span>\n        </button>\n      </div>\n      <div class="calendar-nav-date">\n        <button class="btn btn-transparent btn-sm btn-month">\n          ${this._calendarDate.toLocaleDateString(
                    t,
                    { month: "long" }
                )}\n        </button>\n        <button class="btn btn-transparent btn-sm btn-year">\n          ${this._calendarDate.toLocaleDateString(t, {
                    year: "numeric",
                })}\n        </button>\n      </div>\n      <div class="calendar-nav-next">\n        <button class="btn btn-transparent btn-sm btn-next">\n          <span class="calendar-nav-icon calendar-nav-icon-next"></span>\n        </button>\n        <button class="btn btn-transparent btn-sm btn-double-next">\n          <span class="calendar-nav-icon calendar-nav-icon-double-next"></span>\n        </button>\n      </div>\n    `);
            const a = ((e, t, i) => {
                    const n = ((e, t, i) => {
                            const n = [],
                                s = new Date(e, t),
                                o = s.getFullYear(),
                                a = s.getMonth();
                            let r = 6 - (6 - new Date(o, a, 1).getDay()) - i;
                            i && (r = r < 0 ? 7 + r : r);
                            for (let e = -1 * r; e < 0; e++) n.push({ date: new Date(o, a, e + 1), month: "previous" });
                            return n;
                        })(e, t, i),
                        s = ((e, t) => {
                            const i = [],
                                n = new Date(e, t + 1, 0).getDate();
                            for (let s = 1; s <= n; s++) i.push({ date: new Date(e, t, s), month: "current" });
                            return i;
                        })(e, t),
                        o = ((e, t, i, n) => {
                            const s = [],
                                o = 42 - (i.length + n.length);
                            for (let i = 1; i <= o; i++) s.push({ date: new Date(e, t + 1, i), month: "next" });
                            return s;
                        })(e, t, n, s),
                        a = [...n, ...s, ...o],
                        r = [];
                    return (
                        a.forEach((e, t) => {
                            (t % 7 != 0 && 0 !== r.length) || r.push([]), r[r.length - 1].push(e);
                        }),
                        r
                    );
                })(n, s, e),
                r = U(
                    ((e) => {
                        const t = [],
                            i = new Date();
                        i.setDate(1);
                        for (let n = 0; n < 12; n++) i.setMonth(n), t.push(i.toLocaleString(e, { month: "short" }));
                        return t;
                    })(t),
                    4
                ),
                l = U(
                    ((e) => {
                        const t = [];
                        for (let i = e - 6; i < e + 6; i++) t.push(i);
                        return t;
                    })(this._calendarDate.getFullYear()),
                    4
                ),
                c = a[0],
                h = document.createElement("table");
            (h.innerHTML = `\n    ${
                "days" === this._view
                    ? `\n      <thead>\n        <tr>\n          ${c
                          .map(
                              ({ date: e }) =>
                                  `<th class="calendar-cell">\n              <div class="calendar-header-cell-inner">\n                ${e.toLocaleDateString(t, { weekday: "long" }).slice(0, i)}\n              </div>\n            </th>`
                          )
                          .join("")}\n        </tr>\n      </thead>`
                    : ""
            }\n      <tbody>\n        ${
                "days" === this._view
                    ? a
                          .map(
                              (e) =>
                                  `<tr>${e
                                      .map(
                                          ({ date: e, month: i }) =>
                                              `<td class="calendar-cell ${this._dayClassNames(
                                                  e,
                                                  i
                                              )}">\n              <div\n                class="calendar-cell-inner day"\n                data-coreui-date="${e}"\n              >${e.toLocaleDateString(t, {
                                                  day: "numeric",
                                              })}</div>\n            </td>`
                                      )
                                      .join("")}</tr>`
                          )
                          .join("")
                    : ""
            }\n        ${
                "months" === this._view
                    ? r
                          .map(
                              (e, t) =>
                                  `<tr>${e
                                      .map((e, i) => `<td class="calendar-cell">\n              <div class="calendar-cell-inner month" data-coreui-month="${3 * t + i}">\n                ${e}\n              </div>\n            </td>`)
                                      .join("")}</tr>`
                          )
                          .join("")
                    : ""
            }\n        ${
                "years" === this._view
                    ? l
                          .map(
                              (e) =>
                                  `<tr>${e.map((e) => `<td class="calendar-cell">\n              <div class="calendar-cell-inner year" data-coreui-year="${e}">\n                ${e}\n              </div>\n            </td>`).join("")}</tr>`
                          )
                          .join("")
                    : ""
            }\n      </tbody>\n    `),
                this._element.classList.add("calendar"),
                this._element.append(o, h);
        }
        _updateCalendar() {
            (this._element.innerHTML = ""), this._createCalendar();
        }
        _dayClassNames(e, t) {
            return [
                ee(e) && "today",
                Y(e, this._config.minDate, this._config.maxDate, this._config.disabledDates) && "disabled",
                `${t}`,
                G(e) && "last",
                "current" === t && K(e, this._startDate, this._endDate) && "range",
                Q(e, this._startDate, this._endDate) && "selected",
                J(e, this._startDate, this._endDate) && "start",
                X(e, this._startDate, this._endDate) && "end",
            ].join(" ");
        }
        _getConfig(e) {
            return (e = { ...this.constructor.Default, ...q.getDataAttributes(this._element), ...e }), r(te, e, ue), e;
        }
        static calendarInterface(e, t) {
            const i = pe.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = pe.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    $.on(window, ae, () => {
        Array.from(document.querySelectorAll(".calendar")).forEach((e) => {
            pe.calendarInterface(e);
        });
    }),
        g(pe);
    const fe = {
            find: (e, t = document.documentElement) => [].concat(...Element.prototype.querySelectorAll.call(t, e)),
            findOne: (e, t = document.documentElement) => Element.prototype.querySelector.call(t, e),
            children: (e, t) => [].concat(...e.children).filter((e) => e.matches(t)),
            parents(e, t) {
                const i = [];
                let n = e.parentNode;
                for (; n && n.nodeType === Node.ELEMENT_NODE && 3 !== n.nodeType; ) n.matches(t) && i.push(n), (n = n.parentNode);
                return i;
            },
            prev(e, t) {
                let i = e.previousElementSibling;
                for (; i; ) {
                    if (i.matches(t)) return [i];
                    i = i.previousElementSibling;
                }
                return [];
            },
            next(e, t) {
                let i = e.nextElementSibling;
                for (; i; ) {
                    if (i.matches(t)) return [i];
                    i = i.nextElementSibling;
                }
                return [];
            },
            focusableChildren(e) {
                const t = ["a", "button", "input", "textarea", "select", "details", "[tabindex]", '[contenteditable="true"]'].map((e) => `${e}:not([tabindex^="-"])`).join(", ");
                return this.find(t, e).filter((e) => !c(e) && l(e));
            },
        },
        _e = "carousel",
        ge = ".coreui.carousel",
        me = { interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0, touch: !0 },
        ve = { interval: "(number|boolean)", keyboard: "boolean", slide: "(boolean|string)", pause: "(string|boolean)", wrap: "boolean", touch: "boolean" },
        be = "next",
        ye = "prev",
        we = "left",
        Ee = "right",
        De = { ArrowLeft: Ee, ArrowRight: we },
        Le = `slide${ge}`,
        Te = `slid${ge}`,
        ke = `keydown${ge}`,
        Ce = `mouseenter${ge}`,
        Ae = `mouseleave${ge}`,
        Oe = `touchstart${ge}`,
        Se = `touchmove${ge}`,
        xe = `touchend${ge}`,
        Ie = `pointerdown${ge}`,
        Ne = `pointerup${ge}`,
        Me = `dragstart${ge}`,
        $e = `load${ge}.data-api`,
        Pe = `click${ge}.data-api`,
        He = "active",
        je = ".active.carousel-item";
    class Be extends j {
        constructor(e, t) {
            super(e),
                (this._items = null),
                (this._interval = null),
                (this._activeElement = null),
                (this._isPaused = !1),
                (this._isSliding = !1),
                (this.touchTimeout = null),
                (this.touchStartX = 0),
                (this.touchDeltaX = 0),
                (this._config = this._getConfig(t)),
                (this._indicatorsElement = fe.findOne(".carousel-indicators", this._element)),
                (this._touchSupported = "ontouchstart" in document.documentElement || navigator.maxTouchPoints > 0),
                (this._pointerEvent = Boolean(window.PointerEvent)),
                this._addEventListeners();
        }
        static get Default() {
            return me;
        }
        static get NAME() {
            return _e;
        }
        next() {
            this._slide(be);
        }
        nextWhenVisible() {
            !document.hidden && l(this._element) && this.next();
        }
        prev() {
            this._slide(ye);
        }
        pause(e) {
            e || (this._isPaused = !0), fe.findOne(".carousel-item-next, .carousel-item-prev", this._element) && (s(this._element), this.cycle(!0)), clearInterval(this._interval), (this._interval = null);
        }
        cycle(e) {
            e || (this._isPaused = !1),
                this._interval && (clearInterval(this._interval), (this._interval = null)),
                this._config && this._config.interval && !this._isPaused && (this._updateInterval(), (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval)));
        }
        to(e) {
            this._activeElement = fe.findOne(je, this._element);
            const t = this._getItemIndex(this._activeElement);
            if (e > this._items.length - 1 || e < 0) return;
            if (this._isSliding) return void $.one(this._element, Te, () => this.to(e));
            if (t === e) return this.pause(), void this.cycle();
            const i = e > t ? be : ye;
            this._slide(i, this._items[e]);
        }
        _getConfig(e) {
            return (e = { ...me, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(_e, e, ve), e;
        }
        _handleSwipe() {
            const e = Math.abs(this.touchDeltaX);
            if (e <= 40) return;
            const t = e / this.touchDeltaX;
            (this.touchDeltaX = 0), t && this._slide(t > 0 ? Ee : we);
        }
        _addEventListeners() {
            this._config.keyboard && $.on(this._element, ke, (e) => this._keydown(e)),
                "hover" === this._config.pause && ($.on(this._element, Ce, (e) => this.pause(e)), $.on(this._element, Ae, (e) => this.cycle(e))),
                this._config.touch && this._touchSupported && this._addTouchEventListeners();
        }
        _addTouchEventListeners() {
            const e = (e) => this._pointerEvent && ("pen" === e.pointerType || "touch" === e.pointerType),
                t = (t) => {
                    e(t) ? (this.touchStartX = t.clientX) : this._pointerEvent || (this.touchStartX = t.touches[0].clientX);
                },
                i = (e) => {
                    this.touchDeltaX = e.touches && e.touches.length > 1 ? 0 : e.touches[0].clientX - this.touchStartX;
                },
                n = (t) => {
                    e(t) && (this.touchDeltaX = t.clientX - this.touchStartX),
                        this._handleSwipe(),
                        "hover" === this._config.pause && (this.pause(), this.touchTimeout && clearTimeout(this.touchTimeout), (this.touchTimeout = setTimeout((e) => this.cycle(e), 500 + this._config.interval)));
                };
            fe.find(".carousel-item img", this._element).forEach((e) => {
                $.on(e, Me, (e) => e.preventDefault());
            }),
                this._pointerEvent
                    ? ($.on(this._element, Ie, (e) => t(e)), $.on(this._element, Ne, (e) => n(e)), this._element.classList.add("pointer-event"))
                    : ($.on(this._element, Oe, (e) => t(e)), $.on(this._element, Se, (e) => i(e)), $.on(this._element, xe, (e) => n(e)));
        }
        _keydown(e) {
            if (/input|textarea/i.test(e.target.tagName)) return;
            const t = De[e.key];
            t && (e.preventDefault(), this._slide(t));
        }
        _getItemIndex(e) {
            return (this._items = e && e.parentNode ? fe.find(".carousel-item", e.parentNode) : []), this._items.indexOf(e);
        }
        _getItemByOrder(e, t) {
            const i = e === be;
            return b(this._items, t, i, this._config.wrap);
        }
        _triggerSlideEvent(e, t) {
            const i = this._getItemIndex(e),
                n = this._getItemIndex(fe.findOne(je, this._element));
            return $.trigger(this._element, Le, { relatedTarget: e, direction: t, from: n, to: i });
        }
        _setActiveIndicatorElement(e) {
            if (this._indicatorsElement) {
                const t = fe.findOne(".active", this._indicatorsElement);
                t.classList.remove(He), t.removeAttribute("aria-current");
                const i = fe.find("[data-coreui-target]", this._indicatorsElement);
                for (let t = 0; t < i.length; t++)
                    if (Number.parseInt(i[t].getAttribute("data-coreui-slide-to"), 10) === this._getItemIndex(e)) {
                        i[t].classList.add(He), i[t].setAttribute("aria-current", "true");
                        break;
                    }
            }
        }
        _updateInterval() {
            const e = this._activeElement || fe.findOne(je, this._element);
            if (!e) return;
            const t = Number.parseInt(e.getAttribute("data-coreui-interval"), 10);
            t ? ((this._config.defaultInterval = this._config.defaultInterval || this._config.interval), (this._config.interval = t)) : (this._config.interval = this._config.defaultInterval || this._config.interval);
        }
        _slide(e, t) {
            const i = this._directionToOrder(e),
                n = fe.findOne(je, this._element),
                s = this._getItemIndex(n),
                o = t || this._getItemByOrder(i, n),
                a = this._getItemIndex(o),
                r = Boolean(this._interval),
                l = i === be,
                c = l ? "carousel-item-start" : "carousel-item-end",
                h = l ? "carousel-item-next" : "carousel-item-prev",
                d = this._orderToDirection(i);
            if (o && o.classList.contains(He)) return void (this._isSliding = !1);
            if (this._isSliding) return;
            if (this._triggerSlideEvent(o, d).defaultPrevented) return;
            if (!n || !o) return;
            (this._isSliding = !0), r && this.pause(), this._setActiveIndicatorElement(o), (this._activeElement = o);
            const p = () => {
                $.trigger(this._element, Te, { relatedTarget: o, direction: d, from: s, to: a });
            };
            if (this._element.classList.contains("slide")) {
                o.classList.add(h), u(o), n.classList.add(c), o.classList.add(c);
                const e = () => {
                    o.classList.remove(c, h), o.classList.add(He), n.classList.remove(He, h, c), (this._isSliding = !1), setTimeout(p, 0);
                };
                this._queueCallback(e, n, !0);
            } else n.classList.remove(He), o.classList.add(He), (this._isSliding = !1), p();
            r && this.cycle();
        }
        _directionToOrder(e) {
            return [Ee, we].includes(e) ? (_() ? (e === we ? ye : be) : e === we ? be : ye) : e;
        }
        _orderToDirection(e) {
            return [be, ye].includes(e) ? (_() ? (e === ye ? we : Ee) : e === ye ? Ee : we) : e;
        }
        static carouselInterface(e, t) {
            const i = Be.getOrCreateInstance(e, t);
            let { _config: n } = i;
            "object" == typeof t && (n = { ...n, ...t });
            const s = "string" == typeof t ? t : n.slide;
            if ("number" == typeof t) i.to(t);
            else if ("string" == typeof s) {
                if (void 0 === i[s]) throw new TypeError(`No method named "${s}"`);
                i[s]();
            } else n.interval && n.ride && (i.pause(), i.cycle());
        }
        static jQueryInterface(e) {
            return this.each(function () {
                Be.carouselInterface(this, e);
            });
        }
        static dataApiClickHandler(e) {
            const t = n(this);
            if (!t || !t.classList.contains("carousel")) return;
            const i = { ...q.getDataAttributes(t), ...q.getDataAttributes(this) },
                s = this.getAttribute("data-coreui-slide-to");
            s && (i.interval = !1), Be.carouselInterface(t, i), s && Be.getInstance(t).to(s), e.preventDefault();
        }
    }
    $.on(document, Pe, "[data-coreui-slide], [data-coreui-slide-to]", Be.dataApiClickHandler),
        $.on(window, $e, () => {
            const e = fe.find('[data-coreui-ride="carousel"]');
            for (let t = 0, i = e.length; t < i; t++) Be.carouselInterface(e[t], Be.getInstance(e[t]));
        }),
        g(Be);
    const Re = "collapse",
        We = "coreui.collapse",
        ze = `.${We}`,
        Ve = { toggle: !0, parent: null },
        Fe = { toggle: "boolean", parent: "(null|element)" },
        qe = `show${ze}`,
        Ue = `shown${ze}`,
        Ye = `hide${ze}`,
        Ke = `hidden${ze}`,
        Qe = `click${ze}.data-api`,
        Xe = "show",
        Ge = "collapse",
        Ze = "collapsing",
        Je = "collapsed",
        et = ":scope .collapse .collapse",
        tt = '[data-coreui-toggle="collapse"]';
    class it extends j {
        constructor(e, t) {
            super(e), (this._isTransitioning = !1), (this._config = this._getConfig(t)), (this._triggerArray = []);
            const n = fe.find(tt);
            for (let e = 0, t = n.length; e < t; e++) {
                const t = n[e],
                    s = i(t),
                    o = fe.find(s).filter((e) => e === this._element);
                null !== s && o.length && ((this._selector = s), this._triggerArray.push(t));
            }
            this._initializeChildren(), this._config.parent || this._addAriaAndCollapsedClass(this._triggerArray, this._isShown()), this._config.toggle && this.toggle();
        }
        static get Default() {
            return Ve;
        }
        static get NAME() {
            return Re;
        }
        toggle() {
            this._isShown() ? this.hide() : this.show();
        }
        show() {
            if (this._isTransitioning || this._isShown()) return;
            let e,
                t = [];
            if (this._config.parent) {
                const e = fe.find(et, this._config.parent);
                t = fe.find(".collapse.show, .collapse.collapsing", this._config.parent).filter((t) => !e.includes(t));
            }
            const i = fe.findOne(this._selector);
            if (t.length) {
                const n = t.find((e) => i !== e);
                if (((e = n ? it.getInstance(n) : null), e && e._isTransitioning)) return;
            }
            if ($.trigger(this._element, qe).defaultPrevented) return;
            t.forEach((t) => {
                i !== t && it.getOrCreateInstance(t, { toggle: !1 }).hide(), e || H.set(t, We, null);
            });
            const n = this._getDimension();
            this._element.classList.remove(Ge), this._element.classList.add(Ze), (this._element.style[n] = 0), this._addAriaAndCollapsedClass(this._triggerArray, !0), (this._isTransitioning = !0);
            const s = `scroll${n[0].toUpperCase() + n.slice(1)}`;
            this._queueCallback(
                () => {
                    (this._isTransitioning = !1), this._element.classList.remove(Ze), this._element.classList.add(Ge, Xe), (this._element.style[n] = ""), $.trigger(this._element, Ue);
                },
                this._element,
                !0
            ),
                (this._element.style[n] = `${this._element[s]}px`);
        }
        hide() {
            if (this._isTransitioning || !this._isShown()) return;
            if ($.trigger(this._element, Ye).defaultPrevented) return;
            const e = this._getDimension();
            (this._element.style[e] = `${this._element.getBoundingClientRect()[e]}px`), u(this._element), this._element.classList.add(Ze), this._element.classList.remove(Ge, Xe);
            const t = this._triggerArray.length;
            for (let e = 0; e < t; e++) {
                const t = this._triggerArray[e],
                    i = n(t);
                i && !this._isShown(i) && this._addAriaAndCollapsedClass([t], !1);
            }
            (this._isTransitioning = !0),
                (this._element.style[e] = ""),
                this._queueCallback(
                    () => {
                        (this._isTransitioning = !1), this._element.classList.remove(Ze), this._element.classList.add(Ge), $.trigger(this._element, Ke);
                    },
                    this._element,
                    !0
                );
        }
        _isShown(e = this._element) {
            return e.classList.contains(Xe);
        }
        _getConfig(e) {
            return ((e = { ...Ve, ...q.getDataAttributes(this._element), ...e }).toggle = Boolean(e.toggle)), (e.parent = a(e.parent)), r(Re, e, Fe), e;
        }
        _getDimension() {
            return this._element.classList.contains("collapse-horizontal") ? "width" : "height";
        }
        _initializeChildren() {
            if (!this._config.parent) return;
            const e = fe.find(et, this._config.parent);
            fe.find(tt, this._config.parent)
                .filter((t) => !e.includes(t))
                .forEach((e) => {
                    const t = n(e);
                    t && this._addAriaAndCollapsedClass([e], this._isShown(t));
                });
        }
        _addAriaAndCollapsedClass(e, t) {
            e.length &&
                e.forEach((e) => {
                    t ? e.classList.remove(Je) : e.classList.add(Je), e.setAttribute("aria-expanded", t);
                });
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = {};
                "string" == typeof e && /show|hide/.test(e) && (t.toggle = !1);
                const i = it.getOrCreateInstance(this, t);
                if ("string" == typeof e) {
                    if (void 0 === i[e]) throw new TypeError(`No method named "${e}"`);
                    i[e]();
                }
            });
        }
    }
    $.on(document, Qe, tt, function (e) {
        ("A" === e.target.tagName || (e.delegateTarget && "A" === e.delegateTarget.tagName)) && e.preventDefault();
        const t = i(this);
        fe.find(t).forEach((e) => {
            it.getOrCreateInstance(e, { toggle: !1 }).toggle();
        });
    }),
        g(it);
    var nt = "top",
        st = "bottom",
        ot = "right",
        at = "left",
        rt = "auto",
        lt = [nt, st, ot, at],
        ct = "start",
        ht = "end",
        dt = "clippingParents",
        ut = "viewport",
        pt = "popper",
        ft = "reference",
        _t = lt.reduce(function (e, t) {
            return e.concat([t + "-" + ct, t + "-" + ht]);
        }, []),
        gt = [].concat(lt, [rt]).reduce(function (e, t) {
            return e.concat([t, t + "-" + ct, t + "-" + ht]);
        }, []),
        mt = "beforeRead",
        vt = "read",
        bt = "afterRead",
        yt = "beforeMain",
        wt = "main",
        Et = "afterMain",
        Dt = "beforeWrite",
        Lt = "write",
        Tt = "afterWrite",
        kt = [mt, vt, bt, yt, wt, Et, Dt, Lt, Tt];
    function Ct(e) {
        return e ? (e.nodeName || "").toLowerCase() : null;
    }
    function At(e) {
        if (null == e) return window;
        if ("[object Window]" !== e.toString()) {
            var t = e.ownerDocument;
            return (t && t.defaultView) || window;
        }
        return e;
    }
    function Ot(e) {
        return e instanceof At(e).Element || e instanceof Element;
    }
    function St(e) {
        return e instanceof At(e).HTMLElement || e instanceof HTMLElement;
    }
    function xt(e) {
        return "undefined" != typeof ShadowRoot && (e instanceof At(e).ShadowRoot || e instanceof ShadowRoot);
    }
    const It = {
        name: "applyStyles",
        enabled: !0,
        phase: "write",
        fn: function (e) {
            var t = e.state;
            Object.keys(t.elements).forEach(function (e) {
                var i = t.styles[e] || {},
                    n = t.attributes[e] || {},
                    s = t.elements[e];
                St(s) &&
                    Ct(s) &&
                    (Object.assign(s.style, i),
                    Object.keys(n).forEach(function (e) {
                        var t = n[e];
                        !1 === t ? s.removeAttribute(e) : s.setAttribute(e, !0 === t ? "" : t);
                    }));
            });
        },
        effect: function (e) {
            var t = e.state,
                i = { popper: { position: t.options.strategy, left: "0", top: "0", margin: "0" }, arrow: { position: "absolute" }, reference: {} };
            return (
                Object.assign(t.elements.popper.style, i.popper),
                (t.styles = i),
                t.elements.arrow && Object.assign(t.elements.arrow.style, i.arrow),
                function () {
                    Object.keys(t.elements).forEach(function (e) {
                        var n = t.elements[e],
                            s = t.attributes[e] || {},
                            o = Object.keys(t.styles.hasOwnProperty(e) ? t.styles[e] : i[e]).reduce(function (e, t) {
                                return (e[t] = ""), e;
                            }, {});
                        St(n) &&
                            Ct(n) &&
                            (Object.assign(n.style, o),
                            Object.keys(s).forEach(function (e) {
                                n.removeAttribute(e);
                            }));
                    });
                }
            );
        },
        requires: ["computeStyles"],
    };
    function Nt(e) {
        return e.split("-")[0];
    }
    var Mt = Math.max,
        $t = Math.min,
        Pt = Math.round;
    function Ht(e, t) {
        void 0 === t && (t = !1);
        var i = e.getBoundingClientRect(),
            n = 1,
            s = 1;
        if (St(e) && t) {
            var o = e.offsetHeight,
                a = e.offsetWidth;
            a > 0 && (n = Pt(i.width) / a || 1), o > 0 && (s = Pt(i.height) / o || 1);
        }
        return { width: i.width / n, height: i.height / s, top: i.top / s, right: i.right / n, bottom: i.bottom / s, left: i.left / n, x: i.left / n, y: i.top / s };
    }
    function jt(e) {
        var t = Ht(e),
            i = e.offsetWidth,
            n = e.offsetHeight;
        return Math.abs(t.width - i) <= 1 && (i = t.width), Math.abs(t.height - n) <= 1 && (n = t.height), { x: e.offsetLeft, y: e.offsetTop, width: i, height: n };
    }
    function Bt(e, t) {
        var i = t.getRootNode && t.getRootNode();
        if (e.contains(t)) return !0;
        if (i && xt(i)) {
            var n = t;
            do {
                if (n && e.isSameNode(n)) return !0;
                n = n.parentNode || n.host;
            } while (n);
        }
        return !1;
    }
    function Rt(e) {
        return At(e).getComputedStyle(e);
    }
    function Wt(e) {
        return ["table", "td", "th"].indexOf(Ct(e)) >= 0;
    }
    function zt(e) {
        return ((Ot(e) ? e.ownerDocument : e.document) || window.document).documentElement;
    }
    function Vt(e) {
        return "html" === Ct(e) ? e : e.assignedSlot || e.parentNode || (xt(e) ? e.host : null) || zt(e);
    }
    function Ft(e) {
        return St(e) && "fixed" !== Rt(e).position ? e.offsetParent : null;
    }
    function qt(e) {
        for (var t = At(e), i = Ft(e); i && Wt(i) && "static" === Rt(i).position; ) i = Ft(i);
        return i && ("html" === Ct(i) || ("body" === Ct(i) && "static" === Rt(i).position))
            ? t
            : i ||
                  (function (e) {
                      var t = -1 !== navigator.userAgent.toLowerCase().indexOf("firefox");
                      if (-1 !== navigator.userAgent.indexOf("Trident") && St(e) && "fixed" === Rt(e).position) return null;
                      for (var i = Vt(e); St(i) && ["html", "body"].indexOf(Ct(i)) < 0; ) {
                          var n = Rt(i);
                          if (
                              "none" !== n.transform ||
                              "none" !== n.perspective ||
                              "paint" === n.contain ||
                              -1 !== ["transform", "perspective"].indexOf(n.willChange) ||
                              (t && "filter" === n.willChange) ||
                              (t && n.filter && "none" !== n.filter)
                          )
                              return i;
                          i = i.parentNode;
                      }
                      return null;
                  })(e) ||
                  t;
    }
    function Ut(e) {
        return ["top", "bottom"].indexOf(e) >= 0 ? "x" : "y";
    }
    function Yt(e, t, i) {
        return Mt(e, $t(t, i));
    }
    function Kt(e) {
        return Object.assign({}, { top: 0, right: 0, bottom: 0, left: 0 }, e);
    }
    function Qt(e, t) {
        return t.reduce(function (t, i) {
            return (t[i] = e), t;
        }, {});
    }
    const Xt = {
        name: "arrow",
        enabled: !0,
        phase: "main",
        fn: function (e) {
            var t,
                i = e.state,
                n = e.name,
                s = e.options,
                o = i.elements.arrow,
                a = i.modifiersData.popperOffsets,
                r = Nt(i.placement),
                l = Ut(r),
                c = [at, ot].indexOf(r) >= 0 ? "height" : "width";
            if (o && a) {
                var h = (function (e, t) {
                        return Kt("number" != typeof (e = "function" == typeof e ? e(Object.assign({}, t.rects, { placement: t.placement })) : e) ? e : Qt(e, lt));
                    })(s.padding, i),
                    d = jt(o),
                    u = "y" === l ? nt : at,
                    p = "y" === l ? st : ot,
                    f = i.rects.reference[c] + i.rects.reference[l] - a[l] - i.rects.popper[c],
                    _ = a[l] - i.rects.reference[l],
                    g = qt(o),
                    m = g ? ("y" === l ? g.clientHeight || 0 : g.clientWidth || 0) : 0,
                    v = f / 2 - _ / 2,
                    b = h[u],
                    y = m - d[c] - h[p],
                    w = m / 2 - d[c] / 2 + v,
                    E = Yt(b, w, y),
                    D = l;
                i.modifiersData[n] = (((t = {})[D] = E), (t.centerOffset = E - w), t);
            }
        },
        effect: function (e) {
            var t = e.state,
                i = e.options.element,
                n = void 0 === i ? "[data-popper-arrow]" : i;
            null != n && ("string" != typeof n || (n = t.elements.popper.querySelector(n))) && Bt(t.elements.popper, n) && (t.elements.arrow = n);
        },
        requires: ["popperOffsets"],
        requiresIfExists: ["preventOverflow"],
    };
    function Gt(e) {
        return e.split("-")[1];
    }
    var Zt = { top: "auto", right: "auto", bottom: "auto", left: "auto" };
    function Jt(e) {
        var t,
            i = e.popper,
            n = e.popperRect,
            s = e.placement,
            o = e.variation,
            a = e.offsets,
            r = e.position,
            l = e.gpuAcceleration,
            c = e.adaptive,
            h = e.roundOffsets,
            d = e.isFixed,
            u = a.x,
            p = void 0 === u ? 0 : u,
            f = a.y,
            _ = void 0 === f ? 0 : f,
            g = "function" == typeof h ? h({ x: p, y: _ }) : { x: p, y: _ };
        (p = g.x), (_ = g.y);
        var m = a.hasOwnProperty("x"),
            v = a.hasOwnProperty("y"),
            b = at,
            y = nt,
            w = window;
        if (c) {
            var E = qt(i),
                D = "clientHeight",
                L = "clientWidth";
            E === At(i) && "static" !== Rt((E = zt(i))).position && "absolute" === r && ((D = "scrollHeight"), (L = "scrollWidth")),
                (E = E),
                (s === nt || ((s === at || s === ot) && o === ht)) && ((y = st), (_ -= (d && w.visualViewport ? w.visualViewport.height : E[D]) - n.height), (_ *= l ? 1 : -1)),
                (s !== at && ((s !== nt && s !== st) || o !== ht)) || ((b = ot), (p -= (d && w.visualViewport ? w.visualViewport.width : E[L]) - n.width), (p *= l ? 1 : -1));
        }
        var T,
            k = Object.assign({ position: r }, c && Zt),
            C =
                !0 === h
                    ? (function (e) {
                          var t = e.x,
                              i = e.y,
                              n = window.devicePixelRatio || 1;
                          return { x: Pt(t * n) / n || 0, y: Pt(i * n) / n || 0 };
                      })({ x: p, y: _ })
                    : { x: p, y: _ };
        return (
            (p = C.x),
            (_ = C.y),
            l
                ? Object.assign({}, k, (((T = {})[y] = v ? "0" : ""), (T[b] = m ? "0" : ""), (T.transform = (w.devicePixelRatio || 1) <= 1 ? "translate(" + p + "px, " + _ + "px)" : "translate3d(" + p + "px, " + _ + "px, 0)"), T))
                : Object.assign({}, k, (((t = {})[y] = v ? _ + "px" : ""), (t[b] = m ? p + "px" : ""), (t.transform = ""), t))
        );
    }
    const ei = {
        name: "computeStyles",
        enabled: !0,
        phase: "beforeWrite",
        fn: function (e) {
            var t = e.state,
                i = e.options,
                n = i.gpuAcceleration,
                s = void 0 === n || n,
                o = i.adaptive,
                a = void 0 === o || o,
                r = i.roundOffsets,
                l = void 0 === r || r,
                c = { placement: Nt(t.placement), variation: Gt(t.placement), popper: t.elements.popper, popperRect: t.rects.popper, gpuAcceleration: s, isFixed: "fixed" === t.options.strategy };
            null != t.modifiersData.popperOffsets && (t.styles.popper = Object.assign({}, t.styles.popper, Jt(Object.assign({}, c, { offsets: t.modifiersData.popperOffsets, position: t.options.strategy, adaptive: a, roundOffsets: l })))),
                null != t.modifiersData.arrow && (t.styles.arrow = Object.assign({}, t.styles.arrow, Jt(Object.assign({}, c, { offsets: t.modifiersData.arrow, position: "absolute", adaptive: !1, roundOffsets: l })))),
                (t.attributes.popper = Object.assign({}, t.attributes.popper, { "data-popper-placement": t.placement }));
        },
        data: {},
    };
    var ti = { passive: !0 };
    const ii = {
        name: "eventListeners",
        enabled: !0,
        phase: "write",
        fn: function () {},
        effect: function (e) {
            var t = e.state,
                i = e.instance,
                n = e.options,
                s = n.scroll,
                o = void 0 === s || s,
                a = n.resize,
                r = void 0 === a || a,
                l = At(t.elements.popper),
                c = [].concat(t.scrollParents.reference, t.scrollParents.popper);
            return (
                o &&
                    c.forEach(function (e) {
                        e.addEventListener("scroll", i.update, ti);
                    }),
                r && l.addEventListener("resize", i.update, ti),
                function () {
                    o &&
                        c.forEach(function (e) {
                            e.removeEventListener("scroll", i.update, ti);
                        }),
                        r && l.removeEventListener("resize", i.update, ti);
                }
            );
        },
        data: {},
    };
    var ni = { left: "right", right: "left", bottom: "top", top: "bottom" };
    function si(e) {
        return e.replace(/left|right|bottom|top/g, function (e) {
            return ni[e];
        });
    }
    var oi = { start: "end", end: "start" };
    function ai(e) {
        return e.replace(/start|end/g, function (e) {
            return oi[e];
        });
    }
    function ri(e) {
        var t = At(e);
        return { scrollLeft: t.pageXOffset, scrollTop: t.pageYOffset };
    }
    function li(e) {
        return Ht(zt(e)).left + ri(e).scrollLeft;
    }
    function ci(e) {
        var t = Rt(e),
            i = t.overflow,
            n = t.overflowX,
            s = t.overflowY;
        return /auto|scroll|overlay|hidden/.test(i + s + n);
    }
    function hi(e) {
        return ["html", "body", "#document"].indexOf(Ct(e)) >= 0 ? e.ownerDocument.body : St(e) && ci(e) ? e : hi(Vt(e));
    }
    function di(e, t) {
        var i;
        void 0 === t && (t = []);
        var n = hi(e),
            s = n === (null == (i = e.ownerDocument) ? void 0 : i.body),
            o = At(n),
            a = s ? [o].concat(o.visualViewport || [], ci(n) ? n : []) : n,
            r = t.concat(a);
        return s ? r : r.concat(di(Vt(a)));
    }
    function ui(e) {
        return Object.assign({}, e, { left: e.x, top: e.y, right: e.x + e.width, bottom: e.y + e.height });
    }
    function pi(e, t) {
        return t === ut
            ? ui(
                  (function (e) {
                      var t = At(e),
                          i = zt(e),
                          n = t.visualViewport,
                          s = i.clientWidth,
                          o = i.clientHeight,
                          a = 0,
                          r = 0;
                      return n && ((s = n.width), (o = n.height), /^((?!chrome|android).)*safari/i.test(navigator.userAgent) || ((a = n.offsetLeft), (r = n.offsetTop))), { width: s, height: o, x: a + li(e), y: r };
                  })(e)
              )
            : Ot(t)
            ? (function (e) {
                  var t = Ht(e);
                  return (
                      (t.top = t.top + e.clientTop),
                      (t.left = t.left + e.clientLeft),
                      (t.bottom = t.top + e.clientHeight),
                      (t.right = t.left + e.clientWidth),
                      (t.width = e.clientWidth),
                      (t.height = e.clientHeight),
                      (t.x = t.left),
                      (t.y = t.top),
                      t
                  );
              })(t)
            : ui(
                  (function (e) {
                      var t,
                          i = zt(e),
                          n = ri(e),
                          s = null == (t = e.ownerDocument) ? void 0 : t.body,
                          o = Mt(i.scrollWidth, i.clientWidth, s ? s.scrollWidth : 0, s ? s.clientWidth : 0),
                          a = Mt(i.scrollHeight, i.clientHeight, s ? s.scrollHeight : 0, s ? s.clientHeight : 0),
                          r = -n.scrollLeft + li(e),
                          l = -n.scrollTop;
                      return "rtl" === Rt(s || i).direction && (r += Mt(i.clientWidth, s ? s.clientWidth : 0) - o), { width: o, height: a, x: r, y: l };
                  })(zt(e))
              );
    }
    function fi(e) {
        var t,
            i = e.reference,
            n = e.element,
            s = e.placement,
            o = s ? Nt(s) : null,
            a = s ? Gt(s) : null,
            r = i.x + i.width / 2 - n.width / 2,
            l = i.y + i.height / 2 - n.height / 2;
        switch (o) {
            case nt:
                t = { x: r, y: i.y - n.height };
                break;
            case st:
                t = { x: r, y: i.y + i.height };
                break;
            case ot:
                t = { x: i.x + i.width, y: l };
                break;
            case at:
                t = { x: i.x - n.width, y: l };
                break;
            default:
                t = { x: i.x, y: i.y };
        }
        var c = o ? Ut(o) : null;
        if (null != c) {
            var h = "y" === c ? "height" : "width";
            switch (a) {
                case ct:
                    t[c] = t[c] - (i[h] / 2 - n[h] / 2);
                    break;
                case ht:
                    t[c] = t[c] + (i[h] / 2 - n[h] / 2);
            }
        }
        return t;
    }
    function _i(e, t) {
        void 0 === t && (t = {});
        var i = t,
            n = i.placement,
            s = void 0 === n ? e.placement : n,
            o = i.boundary,
            a = void 0 === o ? dt : o,
            r = i.rootBoundary,
            l = void 0 === r ? ut : r,
            c = i.elementContext,
            h = void 0 === c ? pt : c,
            d = i.altBoundary,
            u = void 0 !== d && d,
            p = i.padding,
            f = void 0 === p ? 0 : p,
            _ = Kt("number" != typeof f ? f : Qt(f, lt)),
            g = h === pt ? ft : pt,
            m = e.rects.popper,
            v = e.elements[u ? g : h],
            b = (function (e, t, i) {
                var n =
                        "clippingParents" === t
                            ? (function (e) {
                                  var t = di(Vt(e)),
                                      i = ["absolute", "fixed"].indexOf(Rt(e).position) >= 0 && St(e) ? qt(e) : e;
                                  return Ot(i)
                                      ? t.filter(function (e) {
                                            return Ot(e) && Bt(e, i) && "body" !== Ct(e);
                                        })
                                      : [];
                              })(e)
                            : [].concat(t),
                    s = [].concat(n, [i]),
                    o = s[0],
                    a = s.reduce(function (t, i) {
                        var n = pi(e, i);
                        return (t.top = Mt(n.top, t.top)), (t.right = $t(n.right, t.right)), (t.bottom = $t(n.bottom, t.bottom)), (t.left = Mt(n.left, t.left)), t;
                    }, pi(e, o));
                return (a.width = a.right - a.left), (a.height = a.bottom - a.top), (a.x = a.left), (a.y = a.top), a;
            })(Ot(v) ? v : v.contextElement || zt(e.elements.popper), a, l),
            y = Ht(e.elements.reference),
            w = fi({ reference: y, element: m, strategy: "absolute", placement: s }),
            E = ui(Object.assign({}, m, w)),
            D = h === pt ? E : y,
            L = { top: b.top - D.top + _.top, bottom: D.bottom - b.bottom + _.bottom, left: b.left - D.left + _.left, right: D.right - b.right + _.right },
            T = e.modifiersData.offset;
        if (h === pt && T) {
            var k = T[s];
            Object.keys(L).forEach(function (e) {
                var t = [ot, st].indexOf(e) >= 0 ? 1 : -1,
                    i = [nt, st].indexOf(e) >= 0 ? "y" : "x";
                L[e] += k[i] * t;
            });
        }
        return L;
    }
    function gi(e, t) {
        void 0 === t && (t = {});
        var i = t,
            n = i.placement,
            s = i.boundary,
            o = i.rootBoundary,
            a = i.padding,
            r = i.flipVariations,
            l = i.allowedAutoPlacements,
            c = void 0 === l ? gt : l,
            h = Gt(n),
            d = h
                ? r
                    ? _t
                    : _t.filter(function (e) {
                          return Gt(e) === h;
                      })
                : lt,
            u = d.filter(function (e) {
                return c.indexOf(e) >= 0;
            });
        0 === u.length && (u = d);
        var p = u.reduce(function (t, i) {
            return (t[i] = _i(e, { placement: i, boundary: s, rootBoundary: o, padding: a })[Nt(i)]), t;
        }, {});
        return Object.keys(p).sort(function (e, t) {
            return p[e] - p[t];
        });
    }
    const mi = {
        name: "flip",
        enabled: !0,
        phase: "main",
        fn: function (e) {
            var t = e.state,
                i = e.options,
                n = e.name;
            if (!t.modifiersData[n]._skip) {
                for (
                    var s = i.mainAxis,
                        o = void 0 === s || s,
                        a = i.altAxis,
                        r = void 0 === a || a,
                        l = i.fallbackPlacements,
                        c = i.padding,
                        h = i.boundary,
                        d = i.rootBoundary,
                        u = i.altBoundary,
                        p = i.flipVariations,
                        f = void 0 === p || p,
                        _ = i.allowedAutoPlacements,
                        g = t.options.placement,
                        m = Nt(g),
                        v =
                            l ||
                            (m !== g && f
                                ? (function (e) {
                                      if (Nt(e) === rt) return [];
                                      var t = si(e);
                                      return [ai(e), t, ai(t)];
                                  })(g)
                                : [si(g)]),
                        b = [g].concat(v).reduce(function (e, i) {
                            return e.concat(Nt(i) === rt ? gi(t, { placement: i, boundary: h, rootBoundary: d, padding: c, flipVariations: f, allowedAutoPlacements: _ }) : i);
                        }, []),
                        y = t.rects.reference,
                        w = t.rects.popper,
                        E = new Map(),
                        D = !0,
                        L = b[0],
                        T = 0;
                    T < b.length;
                    T++
                ) {
                    var k = b[T],
                        C = Nt(k),
                        A = Gt(k) === ct,
                        O = [nt, st].indexOf(C) >= 0,
                        S = O ? "width" : "height",
                        x = _i(t, { placement: k, boundary: h, rootBoundary: d, altBoundary: u, padding: c }),
                        I = O ? (A ? ot : at) : A ? st : nt;
                    y[S] > w[S] && (I = si(I));
                    var N = si(I),
                        M = [];
                    if (
                        (o && M.push(x[C] <= 0),
                        r && M.push(x[I] <= 0, x[N] <= 0),
                        M.every(function (e) {
                            return e;
                        }))
                    ) {
                        (L = k), (D = !1);
                        break;
                    }
                    E.set(k, M);
                }
                if (D)
                    for (
                        var $ = function (e) {
                                var t = b.find(function (t) {
                                    var i = E.get(t);
                                    if (i)
                                        return i.slice(0, e).every(function (e) {
                                            return e;
                                        });
                                });
                                if (t) return (L = t), "break";
                            },
                            P = f ? 3 : 1;
                        P > 0 && "break" !== $(P);
                        P--
                    );
                t.placement !== L && ((t.modifiersData[n]._skip = !0), (t.placement = L), (t.reset = !0));
            }
        },
        requiresIfExists: ["offset"],
        data: { _skip: !1 },
    };
    function vi(e, t, i) {
        return void 0 === i && (i = { x: 0, y: 0 }), { top: e.top - t.height - i.y, right: e.right - t.width + i.x, bottom: e.bottom - t.height + i.y, left: e.left - t.width - i.x };
    }
    function bi(e) {
        return [nt, ot, st, at].some(function (t) {
            return e[t] >= 0;
        });
    }
    const yi = {
            name: "hide",
            enabled: !0,
            phase: "main",
            requiresIfExists: ["preventOverflow"],
            fn: function (e) {
                var t = e.state,
                    i = e.name,
                    n = t.rects.reference,
                    s = t.rects.popper,
                    o = t.modifiersData.preventOverflow,
                    a = _i(t, { elementContext: "reference" }),
                    r = _i(t, { altBoundary: !0 }),
                    l = vi(a, n),
                    c = vi(r, s, o),
                    h = bi(l),
                    d = bi(c);
                (t.modifiersData[i] = { referenceClippingOffsets: l, popperEscapeOffsets: c, isReferenceHidden: h, hasPopperEscaped: d }),
                    (t.attributes.popper = Object.assign({}, t.attributes.popper, { "data-popper-reference-hidden": h, "data-popper-escaped": d }));
            },
        },
        wi = {
            name: "offset",
            enabled: !0,
            phase: "main",
            requires: ["popperOffsets"],
            fn: function (e) {
                var t = e.state,
                    i = e.options,
                    n = e.name,
                    s = i.offset,
                    o = void 0 === s ? [0, 0] : s,
                    a = gt.reduce(function (e, i) {
                        return (
                            (e[i] = (function (e, t, i) {
                                var n = Nt(e),
                                    s = [at, nt].indexOf(n) >= 0 ? -1 : 1,
                                    o = "function" == typeof i ? i(Object.assign({}, t, { placement: e })) : i,
                                    a = o[0],
                                    r = o[1];
                                return (a = a || 0), (r = (r || 0) * s), [at, ot].indexOf(n) >= 0 ? { x: r, y: a } : { x: a, y: r };
                            })(i, t.rects, o)),
                            e
                        );
                    }, {}),
                    r = a[t.placement],
                    l = r.x,
                    c = r.y;
                null != t.modifiersData.popperOffsets && ((t.modifiersData.popperOffsets.x += l), (t.modifiersData.popperOffsets.y += c)), (t.modifiersData[n] = a);
            },
        },
        Ei = {
            name: "popperOffsets",
            enabled: !0,
            phase: "read",
            fn: function (e) {
                var t = e.state,
                    i = e.name;
                t.modifiersData[i] = fi({ reference: t.rects.reference, element: t.rects.popper, strategy: "absolute", placement: t.placement });
            },
            data: {},
        },
        Di = {
            name: "preventOverflow",
            enabled: !0,
            phase: "main",
            fn: function (e) {
                var t = e.state,
                    i = e.options,
                    n = e.name,
                    s = i.mainAxis,
                    o = void 0 === s || s,
                    a = i.altAxis,
                    r = void 0 !== a && a,
                    l = i.boundary,
                    c = i.rootBoundary,
                    h = i.altBoundary,
                    d = i.padding,
                    u = i.tether,
                    p = void 0 === u || u,
                    f = i.tetherOffset,
                    _ = void 0 === f ? 0 : f,
                    g = _i(t, { boundary: l, rootBoundary: c, padding: d, altBoundary: h }),
                    m = Nt(t.placement),
                    v = Gt(t.placement),
                    b = !v,
                    y = Ut(m),
                    w = "x" === y ? "y" : "x",
                    E = t.modifiersData.popperOffsets,
                    D = t.rects.reference,
                    L = t.rects.popper,
                    T = "function" == typeof _ ? _(Object.assign({}, t.rects, { placement: t.placement })) : _,
                    k = "number" == typeof T ? { mainAxis: T, altAxis: T } : Object.assign({ mainAxis: 0, altAxis: 0 }, T),
                    C = t.modifiersData.offset ? t.modifiersData.offset[t.placement] : null,
                    A = { x: 0, y: 0 };
                if (E) {
                    if (o) {
                        var O,
                            S = "y" === y ? nt : at,
                            x = "y" === y ? st : ot,
                            I = "y" === y ? "height" : "width",
                            N = E[y],
                            M = N + g[S],
                            $ = N - g[x],
                            P = p ? -L[I] / 2 : 0,
                            H = v === ct ? D[I] : L[I],
                            j = v === ct ? -L[I] : -D[I],
                            B = t.elements.arrow,
                            R = p && B ? jt(B) : { width: 0, height: 0 },
                            W = t.modifiersData["arrow#persistent"] ? t.modifiersData["arrow#persistent"].padding : { top: 0, right: 0, bottom: 0, left: 0 },
                            z = W[S],
                            V = W[x],
                            F = Yt(0, D[I], R[I]),
                            q = b ? D[I] / 2 - P - F - z - k.mainAxis : H - F - z - k.mainAxis,
                            U = b ? -D[I] / 2 + P + F + V + k.mainAxis : j + F + V + k.mainAxis,
                            Y = t.elements.arrow && qt(t.elements.arrow),
                            K = Y ? ("y" === y ? Y.clientTop || 0 : Y.clientLeft || 0) : 0,
                            Q = null != (O = null == C ? void 0 : C[y]) ? O : 0,
                            X = N + U - Q,
                            G = Yt(p ? $t(M, N + q - Q - K) : M, N, p ? Mt($, X) : $);
                        (E[y] = G), (A[y] = G - N);
                    }
                    if (r) {
                        var Z,
                            J = "x" === y ? nt : at,
                            ee = "x" === y ? st : ot,
                            te = E[w],
                            ie = "y" === w ? "height" : "width",
                            ne = te + g[J],
                            se = te - g[ee],
                            oe = -1 !== [nt, at].indexOf(m),
                            ae = null != (Z = null == C ? void 0 : C[w]) ? Z : 0,
                            re = oe ? ne : te - D[ie] - L[ie] - ae + k.altAxis,
                            le = oe ? te + D[ie] + L[ie] - ae - k.altAxis : se,
                            ce =
                                p && oe
                                    ? (function (e, t, i) {
                                          var n = Yt(e, t, i);
                                          return n > i ? i : n;
                                      })(re, te, le)
                                    : Yt(p ? re : ne, te, p ? le : se);
                        (E[w] = ce), (A[w] = ce - te);
                    }
                    t.modifiersData[n] = A;
                }
            },
            requiresIfExists: ["offset"],
        };
    function Li(e, t, i) {
        void 0 === i && (i = !1);
        var n,
            s,
            o = St(t),
            a =
                St(t) &&
                (function (e) {
                    var t = e.getBoundingClientRect(),
                        i = Pt(t.width) / e.offsetWidth || 1,
                        n = Pt(t.height) / e.offsetHeight || 1;
                    return 1 !== i || 1 !== n;
                })(t),
            r = zt(t),
            l = Ht(e, a),
            c = { scrollLeft: 0, scrollTop: 0 },
            h = { x: 0, y: 0 };
        return (
            (o || (!o && !i)) &&
                (("body" !== Ct(t) || ci(r)) && (c = (n = t) !== At(n) && St(n) ? { scrollLeft: (s = n).scrollLeft, scrollTop: s.scrollTop } : ri(n)),
                St(t) ? (((h = Ht(t, !0)).x += t.clientLeft), (h.y += t.clientTop)) : r && (h.x = li(r))),
            { x: l.left + c.scrollLeft - h.x, y: l.top + c.scrollTop - h.y, width: l.width, height: l.height }
        );
    }
    function Ti(e) {
        var t = new Map(),
            i = new Set(),
            n = [];
        function s(e) {
            i.add(e.name),
                [].concat(e.requires || [], e.requiresIfExists || []).forEach(function (e) {
                    if (!i.has(e)) {
                        var n = t.get(e);
                        n && s(n);
                    }
                }),
                n.push(e);
        }
        return (
            e.forEach(function (e) {
                t.set(e.name, e);
            }),
            e.forEach(function (e) {
                i.has(e.name) || s(e);
            }),
            n
        );
    }
    var ki = { placement: "bottom", modifiers: [], strategy: "absolute" };
    function Ci() {
        for (var e = arguments.length, t = new Array(e), i = 0; i < e; i++) t[i] = arguments[i];
        return !t.some(function (e) {
            return !(e && "function" == typeof e.getBoundingClientRect);
        });
    }
    function Ai(e) {
        void 0 === e && (e = {});
        var t = e,
            i = t.defaultModifiers,
            n = void 0 === i ? [] : i,
            s = t.defaultOptions,
            o = void 0 === s ? ki : s;
        return function (e, t, i) {
            void 0 === i && (i = o);
            var s,
                a,
                r = { placement: "bottom", orderedModifiers: [], options: Object.assign({}, ki, o), modifiersData: {}, elements: { reference: e, popper: t }, attributes: {}, styles: {} },
                l = [],
                c = !1,
                h = {
                    state: r,
                    setOptions: function (i) {
                        var s = "function" == typeof i ? i(r.options) : i;
                        d(), (r.options = Object.assign({}, o, r.options, s)), (r.scrollParents = { reference: Ot(e) ? di(e) : e.contextElement ? di(e.contextElement) : [], popper: di(t) });
                        var a,
                            c,
                            u = (function (e) {
                                var t = Ti(e);
                                return kt.reduce(function (e, i) {
                                    return e.concat(
                                        t.filter(function (e) {
                                            return e.phase === i;
                                        })
                                    );
                                }, []);
                            })(
                                ((a = [].concat(n, r.options.modifiers)),
                                (c = a.reduce(function (e, t) {
                                    var i = e[t.name];
                                    return (e[t.name] = i ? Object.assign({}, i, t, { options: Object.assign({}, i.options, t.options), data: Object.assign({}, i.data, t.data) }) : t), e;
                                }, {})),
                                Object.keys(c).map(function (e) {
                                    return c[e];
                                }))
                            );
                        return (
                            (r.orderedModifiers = u.filter(function (e) {
                                return e.enabled;
                            })),
                            r.orderedModifiers.forEach(function (e) {
                                var t = e.name,
                                    i = e.options,
                                    n = void 0 === i ? {} : i,
                                    s = e.effect;
                                if ("function" == typeof s) {
                                    var o = s({ state: r, name: t, instance: h, options: n });
                                    l.push(o || function () {});
                                }
                            }),
                            h.update()
                        );
                    },
                    forceUpdate: function () {
                        if (!c) {
                            var e = r.elements,
                                t = e.reference,
                                i = e.popper;
                            if (Ci(t, i)) {
                                (r.rects = { reference: Li(t, qt(i), "fixed" === r.options.strategy), popper: jt(i) }),
                                    (r.reset = !1),
                                    (r.placement = r.options.placement),
                                    r.orderedModifiers.forEach(function (e) {
                                        return (r.modifiersData[e.name] = Object.assign({}, e.data));
                                    });
                                for (var n = 0; n < r.orderedModifiers.length; n++)
                                    if (!0 !== r.reset) {
                                        var s = r.orderedModifiers[n],
                                            o = s.fn,
                                            a = s.options,
                                            l = void 0 === a ? {} : a,
                                            d = s.name;
                                        "function" == typeof o && (r = o({ state: r, options: l, name: d, instance: h }) || r);
                                    } else (r.reset = !1), (n = -1);
                            }
                        }
                    },
                    update:
                        ((s = function () {
                            return new Promise(function (e) {
                                h.forceUpdate(), e(r);
                            });
                        }),
                        function () {
                            return (
                                a ||
                                    (a = new Promise(function (e) {
                                        Promise.resolve().then(function () {
                                            (a = void 0), e(s());
                                        });
                                    })),
                                a
                            );
                        }),
                    destroy: function () {
                        d(), (c = !0);
                    },
                };
            if (!Ci(e, t)) return h;
            function d() {
                l.forEach(function (e) {
                    return e();
                }),
                    (l = []);
            }
            return (
                h.setOptions(i).then(function (e) {
                    !c && i.onFirstUpdate && i.onFirstUpdate(e);
                }),
                h
            );
        };
    }
    var Oi = Ai(),
        Si = Ai({ defaultModifiers: [ii, Ei, ei, It] }),
        xi = Ai({ defaultModifiers: [ii, Ei, ei, It, wi, mi, Di, Xt, yi] });
    const Ii = Object.freeze(
            Object.defineProperty(
                {
                    __proto__: null,
                    popperGenerator: Ai,
                    detectOverflow: _i,
                    createPopperBase: Oi,
                    createPopper: xi,
                    createPopperLite: Si,
                    top: nt,
                    bottom: st,
                    right: ot,
                    left: at,
                    auto: rt,
                    basePlacements: lt,
                    start: ct,
                    end: ht,
                    clippingParents: dt,
                    viewport: ut,
                    popper: pt,
                    reference: ft,
                    variationPlacements: _t,
                    placements: gt,
                    beforeRead: mt,
                    read: vt,
                    afterRead: bt,
                    beforeMain: yt,
                    main: wt,
                    afterMain: Et,
                    beforeWrite: Dt,
                    write: Lt,
                    afterWrite: Tt,
                    modifierPhases: kt,
                    applyStyles: It,
                    arrow: Xt,
                    computeStyles: ei,
                    eventListeners: ii,
                    flip: mi,
                    hide: yi,
                    offset: wi,
                    popperOffsets: Ei,
                    preventOverflow: Di,
                },
                Symbol.toStringTag,
                { value: "Module" }
            )
        ),
        Ni = "dropdown",
        Mi = ".coreui.dropdown",
        $i = "Escape",
        Pi = "Space",
        Hi = "ArrowUp",
        ji = "ArrowDown",
        Bi = new RegExp("ArrowUp|ArrowDown|Escape"),
        Ri = `hide${Mi}`,
        Wi = `hidden${Mi}`,
        zi = `show${Mi}`,
        Vi = `shown${Mi}`,
        Fi = `click${Mi}.data-api`,
        qi = `keydown${Mi}.data-api`,
        Ui = `keyup${Mi}.data-api`,
        Yi = "show",
        Ki = '[data-coreui-toggle="dropdown"]',
        Qi = ".dropdown-menu",
        Xi = _() ? "top-end" : "top-start",
        Gi = _() ? "top-start" : "top-end",
        Zi = _() ? "bottom-end" : "bottom-start",
        Ji = _() ? "bottom-start" : "bottom-end",
        en = _() ? "left-start" : "right-start",
        tn = _() ? "right-start" : "left-start",
        nn = { offset: [0, 2], boundary: "clippingParents", reference: "toggle", display: "dynamic", popperConfig: null, autoClose: !0 },
        sn = { offset: "(array|string|function)", boundary: "(string|element)", reference: "(string|element|object)", display: "string", popperConfig: "(null|object|function)", autoClose: "(boolean|string)" };
    class on extends j {
        constructor(e, t) {
            super(e), (this._popper = null), (this._config = this._getConfig(t)), (this._menu = this._getMenuElement()), (this._inNavbar = this._detectNavbar());
        }
        static get Default() {
            return nn;
        }
        static get DefaultType() {
            return sn;
        }
        static get NAME() {
            return Ni;
        }
        toggle() {
            return this._isShown() ? this.hide() : this.show();
        }
        show() {
            if (c(this._element) || this._isShown(this._menu)) return;
            const e = { relatedTarget: this._element };
            if ($.trigger(this._element, zi, e).defaultPrevented) return;
            const t = on.getParentFromElement(this._element);
            this._inNavbar ? q.setDataAttribute(this._menu, "popper", "none") : this._createPopper(t),
                "ontouchstart" in document.documentElement && !t.closest(".navbar-nav") && [].concat(...document.body.children).forEach((e) => $.on(e, "mouseover", d)),
                this._element.focus(),
                this._element.setAttribute("aria-expanded", !0),
                this._menu.classList.add(Yi),
                this._element.classList.add(Yi),
                $.trigger(this._element, Vi, e);
        }
        hide() {
            if (c(this._element) || !this._isShown(this._menu)) return;
            const e = { relatedTarget: this._element };
            this._completeHide(e);
        }
        dispose() {
            this._popper && this._popper.destroy(), super.dispose();
        }
        update() {
            (this._inNavbar = this._detectNavbar()), this._popper && this._popper.update();
        }
        _completeHide(e) {
            $.trigger(this._element, Ri, e).defaultPrevented ||
                ("ontouchstart" in document.documentElement && [].concat(...document.body.children).forEach((e) => $.off(e, "mouseover", d)),
                this._popper && this._popper.destroy(),
                this._menu.classList.remove(Yi),
                this._element.classList.remove(Yi),
                this._element.setAttribute("aria-expanded", "false"),
                q.removeDataAttribute(this._menu, "popper"),
                $.trigger(this._element, Wi, e));
        }
        _getConfig(e) {
            if (
                ((e = { ...this.constructor.Default, ...q.getDataAttributes(this._element), ...e }),
                r(Ni, e, this.constructor.DefaultType),
                "object" == typeof e.reference && !o(e.reference) && "function" != typeof e.reference.getBoundingClientRect)
            )
                throw new TypeError(`${Ni.toUpperCase()}: Option "reference" provided type "object" without a required "getBoundingClientRect" method.`);
            return e;
        }
        _createPopper(e) {
            if (void 0 === Ii) throw new TypeError("Bootstrap's dropdowns require Popper (https://popper.js.org)");
            let t = this._element;
            "parent" === this._config.reference ? (t = e) : o(this._config.reference) ? (t = a(this._config.reference)) : "object" == typeof this._config.reference && (t = this._config.reference);
            const i = this._getPopperConfig(),
                n = i.modifiers.find((e) => "applyStyles" === e.name && !1 === e.enabled);
            (this._popper = xi(t, this._menu, i)), n && q.setDataAttribute(this._menu, "popper", "static");
        }
        _isShown(e = this._element) {
            return e.classList.contains(Yi);
        }
        _getMenuElement() {
            return fe.next(this._element, Qi)[0];
        }
        _getPlacement() {
            const e = this._element.parentNode;
            if (e.classList.contains("dropend")) return en;
            if (e.classList.contains("dropstart")) return tn;
            const t = "end" === getComputedStyle(this._menu).getPropertyValue("--cui-position").trim();
            return e.classList.contains("dropup") ? (t ? Gi : Xi) : t ? Ji : Zi;
        }
        _detectNavbar() {
            return null !== this._element.closest(".navbar");
        }
        _getOffset() {
            const { offset: e } = this._config;
            return "string" == typeof e ? e.split(",").map((e) => Number.parseInt(e, 10)) : "function" == typeof e ? (t) => e(t, this._element) : e;
        }
        _getPopperConfig() {
            const e = {
                placement: this._getPlacement(),
                modifiers: [
                    { name: "preventOverflow", options: { boundary: this._config.boundary } },
                    { name: "offset", options: { offset: this._getOffset() } },
                ],
            };
            return "static" === this._config.display && (e.modifiers = [{ name: "applyStyles", enabled: !1 }]), { ...e, ...("function" == typeof this._config.popperConfig ? this._config.popperConfig(e) : this._config.popperConfig) };
        }
        _selectMenuItem({ key: e, target: t }) {
            const i = fe.find(".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)", this._menu).filter(l);
            i.length && b(i, t, e === ji, !i.includes(t)).focus();
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = on.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e]();
                }
            });
        }
        static clearMenus(e) {
            if (e && (2 === e.button || ("keyup" === e.type && "Tab" !== e.key))) return;
            const t = fe.find(Ki);
            for (let i = 0, n = t.length; i < n; i++) {
                const n = on.getInstance(t[i]);
                if (!n || !1 === n._config.autoClose) continue;
                if (!n._isShown()) continue;
                const s = { relatedTarget: n._element };
                if (e) {
                    const t = e.composedPath(),
                        i = t.includes(n._menu);
                    if (t.includes(n._element) || ("inside" === n._config.autoClose && !i) || ("outside" === n._config.autoClose && i)) continue;
                    if (n._menu.contains(e.target) && (("keyup" === e.type && "Tab" === e.key) || /input|select|option|textarea|form/i.test(e.target.tagName))) continue;
                    "click" === e.type && (s.clickEvent = e);
                }
                n._completeHide(s);
            }
        }
        static getParentFromElement(e) {
            return n(e) || e.parentNode;
        }
        static dataApiKeydownHandler(e) {
            if (/input|textarea/i.test(e.target.tagName) ? e.key === Pi || (e.key !== $i && ((e.key !== ji && e.key !== Hi) || e.target.closest(Qi))) : !Bi.test(e.key)) return;
            const t = this.classList.contains(Yi);
            if (!t && e.key === $i) return;
            if ((e.preventDefault(), e.stopPropagation(), c(this))) return;
            const i = this.matches(Ki) ? this : fe.prev(this, Ki)[0],
                n = on.getOrCreateInstance(i);
            if (e.key !== $i) return e.key === Hi || e.key === ji ? (t || n.show(), void n._selectMenuItem(e)) : void ((t && e.key !== Pi) || on.clearMenus());
            n.hide();
        }
    }
    $.on(document, qi, Ki, on.dataApiKeydownHandler),
        $.on(document, qi, Qi, on.dataApiKeydownHandler),
        $.on(document, Fi, on.clearMenus),
        $.on(document, Ui, on.clearMenus),
        $.on(document, Fi, Ki, function (e) {
            e.preventDefault(), on.getOrCreateInstance(this).toggle();
        }),
        g(on);
    const an = "picker",
        rn = { cancelButtonText: "Cancel", container: "dropdown", disabled: !1, footer: !1, okButtonText: "OK" },
        ln = { cancelButtonText: "string", container: "string", disabled: "boolean", footer: "boolean", okButtonText: "string" };
    class cn extends j {
        constructor(e, t) {
            super(e), (this._config = this._getConfig(t)), (this._dropdown = null), (this._dropdownEl = null), (this._dropdownMenuEl = null), (this._dropdownToggleEl = null), this._createPicker();
        }
        static get Default() {
            return rn;
        }
        static get DefaultType() {
            return ln;
        }
        static get NAME() {
            return an;
        }
        _createDropdown() {
            const e = document.createElement("div");
            e.classList.add("picker"), (this._dropdownEl = e);
            const t = document.createElement("div");
            (this._dropdownToggleEl = t), this._config.disabled || q.setDataAttribute(t, "toggle", "dropdown");
            const i = document.createElement("div");
            i.classList.add("dropdown-menu"), (this._dropdownMenuEl = i), e.append(t, i), this._element.append(e), (this._dropdown = new on(t, { autoClose: "outside" }));
        }
        _createFooter() {
            const e = document.createElement("div");
            e.classList.add("picker-footer");
            const t = document.createElement("button");
            t.classList.add("btn", "btn-sm", "btn-ghost-primary"),
                (t.type = "button"),
                (t.innerHTML = this._config.cancelButtonText),
                t.addEventListener("click", () => {
                    this._dropdown.hide(), $.trigger(this._element, "onCancelClick.coreui.picker");
                });
            const i = document.createElement("button");
            i.classList.add("btn", "btn-sm", "btn-primary"),
                (i.type = "button"),
                (i.innerHTML = this._config.okButtonText),
                i.addEventListener("click", () => {
                    this._dropdown.hide();
                }),
                e.append(t, i),
                this._dropdownMenuEl.append(e);
        }
        _createPicker() {
            "dropdown" === this._config.container && this._createDropdown(), (this._config.footer || this._config.timepicker) && this._createFooter();
        }
        _getConfig(e) {
            return (e = { ...this.constructor.Default, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(an, e, ln), e;
        }
    }
    const hn = (e) => ["am", "AM", "pm", "PM"].some((t) => new Date().toLocaleString(e).includes(t)),
        dn = "time-picker",
        un = ".coreui.time-picker",
        pn = `change${un}`,
        fn = `load${un}.data-api`,
        _n = { ...cn.Default, cleaner: !0, container: "dropdown", disabled: !1, footer: !0, inputReadOnly: !1, locale: navigator.language, placeholder: "Select time", size: null, value: null, variant: "roll" },
        gn = { ...cn.DefaultType, cleaner: "boolean", container: "string", disabled: "boolean", inputReadOnly: "boolean", locale: "string", placeholder: "string", size: "(string|null)", value: "(date|string|null)", variant: "string" };
    class mn extends cn {
        constructor(e, t) {
            var i, n;
            super(e),
                (this._handleTimeChange = (e, t) => {
                    const i = this._date || new Date("1970-01-01");
                    var n, s;
                    "toggle" === e && ("am" === t && ((this._ampm = "am"), i.setHours(i.getHours() - 12)), "pm" === t && ((this._ampm = "pm"), i.setHours(i.getHours() + 12))),
                        "hours" === e &&
                            (hn(this._config.locale) ? i.setHours(((n = this._ampm), (s = Number.parseInt(t, 10)), "am" === n && 12 === s ? 0 : "am" === n ? s : "pm" === n && 12 === s ? 12 : s + 12)) : i.setHours(Number.parseInt(t, 10))),
                        "minutes" === e && i.setMinutes(Number.parseInt(t, 10)),
                        "seconds" === e && i.setSeconds(Number.parseInt(t, 10)),
                        this._input && (this._input.value = i.toLocaleTimeString(this._config.locale)),
                        (this._date = new Date(i)),
                        $.trigger(this._element, pn, { timeString: i.toTimeString(), localeTimeString: i.toLocaleTimeString(), date: i });
                }),
                (this._config = this._getConfig(t)),
                (this._date = this._convertStringToDate(this._config.value)),
                (this._ampm = this._date ? ((i = new Date(this._date)), (n = this._config.locale), i.toLocaleTimeString(n).includes("AM") ? "am" : i.toLocaleTimeString(n).includes("PM") || i.getHours() >= 12 ? "pm" : "am") : "am"),
                (this._uid = Math.random().toString(36).slice(2)),
                (this._input = null),
                (this._selectAmPm = null),
                (this._selectHours = null),
                (this._selectMinutes = null),
                (this._selectSeconds = null),
                (this._timePickerBody = null),
                this._createTimePicker(),
                this._createTimePickerSelection(),
                this._addEventListeners();
            const s = fe.find(".selected", this._element);
            s && s.forEach((e) => this._scrollTop(e.parentNode, e));
        }
        static get Default() {
            return _n;
        }
        static get DefaultType() {
            return gn;
        }
        static get NAME() {
            return dn;
        }
        clear() {
            (this._date = null), (this._input.value = ""), (this._timePickerBody.innerHTML = ""), this._createTimePickerSelection();
        }
        reset() {
            (this._date = this._convertStringToDate(this._config.value)),
                (this._input.value = this._convertStringToDate(this._config.value).toLocaleTimeString(this._config.locale)),
                (this._timePickerBody.innerHTML = ""),
                this._createTimePickerSelection();
        }
        update(e) {
            (this._config = this._getConfig(e)), (this._element.innerHTML = ""), this._createTimePicker(), this._createTimePickerSelection();
        }
        _addEventListeners() {
            $.on(this._element, "click", ".picker-input-group-cleaner", (e) => {
                e.stopPropagation(), this.clear();
            }),
                $.on(this._element, "onCancelClick.coreui.picker", () => {
                    this.reset();
                });
        }
        _convertStringToDate(e) {
            return e ? (e instanceof Date ? e : new Date(`1970-01-01 ${this._config.value}`)) : null;
        }
        _createInputGroup() {
            const e = document.createElement("div");
            e.classList.add("input-group", "picker-input-group"), this._config.size && e.classList.add(`input-group-${this._config.size}`);
            const t = document.createElement("input");
            t.classList.add("form-control"),
                (t.disabled = this._config.disabled),
                (t.placeholder = this._config.placeholder),
                (t.readOnly = this._config.inputReadOnly),
                (t.type = "text"),
                (t.value = this._date ? this._date.toLocaleTimeString(this._config.locale) : "");
            const i = document.createElement("span");
            return (
                i.classList.add("input-group-text"),
                (i.innerHTML = '\n      <span class="picker-input-group-indicator">\n        <span class="picker-input-group-icon time-picker-input-icon"></span>\n      </span>'),
                this._config.cleaner && (i.innerHTML += '\n        <span class="picker-input-group-cleaner" role="button">\n          <span class="picker-input-group-icon time-picker-cleaner-icon"></span>\n        </span>'),
                e.append(t, i),
                (this._input = t),
                e
            );
        }
        _createTimePicker() {
            this._element.classList.add("time-picker"),
                "dropdown" === this._config.container && (this._dropdownToggleEl.append(this._createInputGroup()), this._dropdownMenuEl.prepend(this._createTimePickerBody())),
                "inline" === this._config.container && this._element.append(this._createTimePickerBody());
        }
        _createTimePickerBody() {
            const e = document.createElement("div");
            return e.classList.add("time-picker-body"), "roll" === this._config.variant && e.classList.add("time-picker-roll"), (this._timePickerBody = e), e;
        }
        _createTimePickerSelection() {
            const e = this._date ? (hn(this._config.locale) ? this._date.getHours() % 12 || 12 : this._date.getHours()) : null,
                t = this._date ? this._date.getMinutes() : null,
                i = this._date ? this._date.getSeconds() : null;
            "roll" === this._config.variant && this._createTimePickerRoll(e, t, i), "select" === this._config.variant && this._createTimePickerSelect(e, t, i);
        }
        _createSelect(e, t, i, n) {
            const s = document.createElement("select");
            return (
                s.classList.add("form-select", "form-select-sm", e),
                (s.disabled = this._config.disabled),
                s.addEventListener("change", (t) => this._handleTimeChange(e, t.target.value)),
                t.forEach((e) => {
                    const t = document.createElement("option");
                    (t.value = e.value), (t.innerHTML = e.label), s.append(t);
                }),
                n && (s.value = n.toString()),
                (this[i] = s),
                s
            );
        }
        _createTimePickerSelect(e, t, i) {
            const n = document.createElement("div");
            n.classList.add("time-separator"),
                (n.innerHTML = ":"),
                (this._timePickerBody.innerHTML = '<span class="time-picker-inline-icon"></span>'),
                this._timePickerBody.append(
                    this._createSelect("hours", this._getHours(), "_selectHours", e),
                    n.cloneNode(!0),
                    this._createSelect("minutes", this._getMinutesOrSeconds(), "_selectMinutes", t),
                    n,
                    this._createSelect("seconds", this._getMinutesOrSeconds(), "_selectSeconds", i)
                ),
                hn(this._config.locale) &&
                    this._timePickerBody.append(
                        this._createSelect(
                            "toggle",
                            [
                                { value: "am", label: "AM" },
                                { value: "pm", label: "PM" },
                            ],
                            "_selectAmPm",
                            this._ampm
                        )
                    );
        }
        _createTimePickerRoll(e, t, i) {
            this._timePickerBody.append(
                this._createTimePickerRollCol(this._getHours(), "hours", e),
                this._createTimePickerRollCol(this._getMinutesOrSeconds(), "minutes", t),
                this._createTimePickerRollCol(this._getMinutesOrSeconds(), "seconds", i)
            ),
                hn(this._config.locale) &&
                    this._timePickerBody.append(
                        this._createTimePickerRollCol(
                            [
                                { value: "am", label: "AM" },
                                { value: "pm", label: "PM" },
                            ],
                            "toggle",
                            this._ampm
                        )
                    );
        }
        _createTimePickerRollCol(e, t, i) {
            const n = document.createElement("div");
            return (
                n.classList.add("time-picker-roll-col"),
                e.forEach((e) => {
                    const s = document.createElement("div");
                    s.classList.add("time-picker-roll-cell"),
                        e.value === i && s.classList.add("selected"),
                        s.setAttribute("role", "button"),
                        (s.innerHTML = e.label),
                        s.addEventListener("click", () => {
                            s.classList.add("selected"), this._scrollTo(n, s);
                            for (const e of n.children) e !== s && e.classList.remove("selected");
                            this._handleTimeChange(t, e.value);
                        }),
                        n.append(s);
                }),
                n
            );
        }
        _getConfig(e) {
            return (e = { ...this.constructor.Default, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(dn, e, gn), e;
        }
        _getHours(e = this._config.locale) {
            return Array.from({ length: hn(e) ? 12 : 24 }, (t, i) => ({ value: hn(e) ? i + 1 : i, label: (hn(e) ? i + 1 : i).toLocaleString(e) }));
        }
        _getMinutesOrSeconds(e = this._config.locale) {
            return Array.from({ length: 60 }, (t, i) => ({ value: i, label: i.toLocaleString(e).padStart(2, (0).toLocaleString(e)) }));
        }
        _scrollTo(e, t) {
            e.scrollTo({ top: t.offsetTop, behavior: "smooth" });
        }
        _scrollTop(e, t) {
            e.scrollTop = t.offsetTop;
        }
        _updateTimePicker() {
            (this._element.innerHTML = ""), this._createTimePicker();
        }
        static timePickerInterface(e, t) {
            const i = mn.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = mn.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    $.on(window, fn, () => {
        const e = fe.find('[data-coreui-toggle="time-picker"]');
        for (let t = 0, i = e.length; t < i; t++) mn.timePickerInterface(e[t]);
    }),
        g(mn);
    const vn = "date-range-picker",
        bn = ".coreui.date-range-picker",
        yn = `endDateChange${bn}`,
        wn = `startDateChange${bn}`,
        En = `load${bn}.data-api`,
        Dn = {
            ...cn.Default,
            calendars: 2,
            cleaner: !0,
            calendarDate: null,
            date: null,
            disabled: !1,
            disabledDates: null,
            endDate: null,
            firstDayOfWeek: 1,
            locale: navigator.language,
            maxDate: null,
            minDate: null,
            placeholder: ["Start date", "End date"],
            range: !0,
            ranges: {},
            size: null,
            startDate: null,
            selectEndDate: !1,
            timepicker: !1,
        },
        Ln = {
            ...cn.DefaultType,
            calendars: "number",
            cleaner: "boolean",
            calendarDate: "(date|string|null)",
            date: "(date|string|null)",
            disabledDates: "(array|null)",
            disabled: "boolean",
            endDate: "(date|string|null)",
            firstDayOfWeek: "number",
            locale: "string",
            maxDate: "(date|string|null)",
            minDate: "(date|string|null)",
            placeholder: "(array|string)",
            range: "boolean",
            ranges: "object",
            size: "(string|null)",
            startDate: "(date|string|null)",
            selectEndDate: "boolean",
            timepicker: "boolean",
        };
    class Tn extends cn {
        constructor(e, t) {
            super(e),
                (this._config = this._getConfig(t)),
                (this._calendarDate = this._convertStringToDate(this._config.calendarDate || this._config.date || this._config.startDate || new Date())),
                (this._startDate = this._convertStringToDate(this._config.date || this._config.startDate)),
                (this._endDate = this._convertStringToDate(this._config.endDate)),
                (this._mobile = window.innerWidth < 768),
                (this._selectEndDate = this._config.selectEndDate),
                (this._calendars = null),
                (this._calendarStart = null),
                (this._calendarEnd = null),
                (this._dateRangePicker = null),
                (this._endInput = null),
                (this._startInput = null),
                (this._timePickerEnd = null),
                (this._timePickerStart = null),
                this._createDateRangePicker(),
                this._createCalendars(),
                this._addEventListeners(),
                this._addCalendarEventListeners();
        }
        static get Default() {
            return Dn;
        }
        static get DefaultType() {
            return Ln;
        }
        static get NAME() {
            return vn;
        }
        clear() {
            (this._date = null), (this._endDate = null), (this._endInput.value = ""), (this._startDate = null), (this._startInput.value = ""), (this._calendars.innerHTML = ""), this._createCalendars(), this._addCalendarEventListeners();
        }
        reset() {
            (this._date = null),
                (this._endDate = this._config.endDate),
                (this._endInput.value = this._setInputValue(this._config.endDate)),
                (this._startDate = this._config.startDate),
                (this._startInput.value = this._setInputValue(this._config.startDate)),
                (this._calendars.innerHTML = ""),
                this._createCalendars(),
                this._addCalendarEventListeners();
        }
        update(e) {
            (this._config = this._getConfig(e)), (this._element.innerHTML = ""), this._createDateRangePicker(), this._createCalendars(), this._addEventListeners(), this._addCalendarEventListeners();
        }
        _addEventListeners() {
            $.on(this._startInput, "click", () => {
                this._selectEndDate = !1;
            }),
                $.on(this._endInput, "click", () => {
                    this._selectEndDate = !0;
                }),
                $.on(this._element, "click", ".picker-input-group-cleaner", (e) => {
                    e.stopPropagation(), this.clear();
                }),
                $.on(this._element, "onCancelClick.coreui.picker", () => {
                    this.reset();
                }),
                $.on(window, "resize", () => {
                    this._mobile = window.innerWidth < 768;
                });
        }
        _addCalendarEventListeners() {
            fe.find(".calendar", this._element).forEach((e) => {
                $.on(e, "calendarDateChange.coreui.calendar", (e) => {
                    (this._calendarDate = e.date), this._updateCalendars();
                }),
                    $.on(e, "startDateChange.coreui.calendar", (e) => {
                        (this._startDate = e.date), (this._selectEndDate = e.selectEndDate), (this._startInput.value = this._setInputValue(e.date)), this._updateCalendars(), $.trigger(this._element, wn, { date: e.date });
                    }),
                    $.on(e, "endDateChange.coreui.calendar", (e) => {
                        (this._endDate = e.date), (this._selectEndDate = e.selectEndDate), (this._endInput.value = this._setInputValue(e.date)), this._updateCalendars(), $.trigger(this._element, yn, { date: e.date });
                    }),
                    $.on(e, "cellHover.coreui.calendar", (e) => {
                        this._selectEndDate
                            ? (this._endInput.value = e.date ? e.date.toLocaleDateString(this._config.locale) : this._endDate ? this._setInputValue(this._endDate) : "")
                            : (this._startInput.value = e.date ? e.date.toLocaleDateString(this._config.locale) : this._startDate ? this._setInputValue(this._startDate) : "");
                    });
            });
        }
        _convertStringToDate(e) {
            return e ? (e instanceof Date ? e : new Date(e)) : null;
        }
        _createInput(e, t) {
            const i = document.createElement("input");
            return i.classList.add("form-control"), (i.disabled = this._config.disabled), (i.placeholder = e), (i.readOnly = this._config.inputReadOnly), (i.type = "text"), (i.value = t), i;
        }
        _createInputGroup() {
            const e = document.createElement("div");
            e.classList.add("input-group", "picker-input-group"), this._config.size && e.classList.add(`input-group-${this._config.size}`);
            const t = this._createInput(this._getPlaceholder()[0], this._setInputValue(this._startDate)),
                i = this._createInput(this._getPlaceholder()[1], this._setInputValue(this._endDate)),
                n = document.createElement("span");
            n.classList.add("input-group-text"), (n.innerHTML = '<span class="picker-input-group-icon date-picker-arrow-icon"></span>');
            const s = document.createElement("span");
            return (
                s.classList.add("input-group-text"),
                (s.innerHTML = '\n      <span class="picker-input-group-indicator">\n        <span class="picker-input-group-icon time-picker-input-icon"></span>\n      </span>'),
                this._config.cleaner && (s.innerHTML += '\n        <span class="picker-input-group-cleaner" role="button">\n          <span class="picker-input-group-icon time-picker-cleaner-icon"></span>\n        </span>'),
                (this._startInput = t),
                (this._endInput = i),
                this._config.range ? e.append(t, n, i, s) : e.append(t, s),
                e
            );
        }
        _createCalendars() {
            document.createElement("div").classList.add("date-picker-calendar"),
                Array.from({ length: this._mobile ? 1 : this._config.calendars }).forEach((e, t) => {
                    const i = document.createElement("div");
                    if (
                        (i.classList.add("date-picker-calendar"),
                        this._calendars.append(i),
                        new pe(i, {
                            calendarDate: new Date(this._calendarDate.getFullYear(), this._calendarDate.getMonth() + t, 1),
                            disabledDates: this._config.disabledDates,
                            endDate: this._endDate,
                            firstDayOfWeek: this._config.firstDayOfWeek,
                            locale: this._config.locale,
                            maxDate: this._config.maxDate,
                            minDate: this._config.minDate,
                            range: this._config.range,
                            selectEndDate: this._selectEndDate,
                            startDate: this._startDate,
                        }),
                        this._config.timepicker)
                    ) {
                        if ((1 === this._config.calendars || this._mobile) && this._config.range) {
                            const e = document.createElement("div");
                            e.classList.add("time-picker"),
                                new mn(e, { container: "inline", disabled: !this._startDate, locale: this._config.locale, value: this._startDate, variant: "select" }),
                                i.append(e),
                                $.one(e, "change.coreui.timepicker", (e) => {
                                    (this._startDate = e.date), (this._startInput.value = this._setInputValue(this._startDate)), this._updateCalendars();
                                });
                            const t = document.createElement("div");
                            t.classList.add("time-picker"),
                                new mn(t, { container: "inline", disabled: !this._endDate, locale: this._config.locale, value: this._endDate, variant: "select" }),
                                i.append(t),
                                $.one(t, "change.coreui.time-picker", (e) => {
                                    (this._endDate = e.date), (this._endInput.value = this._setInputValue(this._endDate)), this._updateCalendars();
                                });
                        }
                        if (!this._mobile && (0 === t || this._config.calendars - t == 1)) {
                            const e = document.createElement("div");
                            e.classList.add("time-picker"),
                                new mn(e, { container: "inline", disabled: 0 === t ? !this._startDate : !this._endDate, locale: this._config.locale, value: 0 === t ? this._startDate : this._endDate, variant: "select" }),
                                i.append(e),
                                $.one(e, "change.coreui.time-picker", (e) => {
                                    0 === t ? ((this._startDate = e.date), (this._startInput.value = this._setInputValue(this._startDate))) : ((this._endDate = e.date), (this._endInput.value = this._setInputValue(this._endDate))),
                                        this._updateCalendars();
                                });
                        }
                    }
                    this._calendars.append(i);
                });
        }
        _createDateRangePicker() {
            this._element.classList.add("date-picker"), this._dropdownToggleEl.append(this._createInputGroup()), this._dropdownMenuEl.prepend(this._createDateRangePickerBody());
        }
        _createDateRangePickerBody() {
            const e = document.createElement("div");
            if ((e.classList.add("date-picker-body"), Object.keys(this._config.ranges).length)) {
                const t = document.createElement("div");
                t.classList.add("date-picker-ranges"),
                    Object.keys(this._config.ranges).forEach((e) => {
                        const i = document.createElement("button");
                        i.classList.add("btn", "btn-ghost-secondary"),
                            (i.role = "button"),
                            i.addEventListener("click", () => {
                                (this._startDate = this._config.ranges[e][0]),
                                    (this._endDate = this._config.ranges[e][1]),
                                    (this._startInput.value = this._setInputValue(this._startDate)),
                                    (this._endInput.value = this._setInputValue(this._endDate)),
                                    this._updateCalendars();
                            }),
                            (i.innerHTML = e),
                            t.append(i);
                    }),
                    e.append(t);
            }
            const t = document.createElement("div");
            return t.classList.add("date-picker-calendars"), (this._calendars = t), e.append(t), e;
        }
        _setInputValue(e) {
            return e ? (this._config.timepicker ? e.toLocaleString(this._config.locale) : e.toLocaleDateString(this._config.locale)) : "";
        }
        _updateCalendars() {
            (this._calendars.innerHTML = ""), this._createCalendars(), this._addCalendarEventListeners();
        }
        _getConfig(e) {
            return (e = { ...this.constructor.Default, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(vn, e, Ln), e;
        }
        _getPlaceholder() {
            const { placeholder: e } = this._config;
            return "string" == typeof e ? e.split(",") : e;
        }
        static dateRangePickerInterface(e, t) {
            const i = Tn.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = Tn.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    $.on(window, En, () => {
        const e = fe.find('[data-coreui-toggle="date-range-picker"]');
        for (let t = 0, i = e.length; t < i; t++) Tn.dateRangePickerInterface(e[t]);
    }),
        g(Tn);
    const kn = ".coreui.date-picker",
        Cn = `dateChange${kn}`,
        An = `load${kn}.data-api`,
        On = { ...Tn.Default, calendars: 1, placeholder: ["Select date"], range: !1 },
        Sn = { ...Tn.DefaultType, date: "(date|string|null)" };
    class xn extends Tn {
        static get Default() {
            return On;
        }
        static get DefaultType() {
            return Sn;
        }
        static get NAME() {
            return "date-picker";
        }
        _addCalendarEventListeners() {
            super._addCalendarEventListeners(),
                fe.find(".calendar", this._element).forEach((e) => {
                    $.on(e, "startDateChange.coreui.calendar", (e) => {
                        (this._startDate = e.date), (this._selectEndDate = e.selectEndDate), (this._startInput.value = this._setInputValue(e.date)), this._updateCalendars(), $.trigger(this._element, Cn, { date: e.date });
                    });
                });
        }
        static datePickerInterface(e, t) {
            const i = xn.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = xn.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    $.on(window, An, () => {
        const e = fe.find('[data-coreui-toggle="date-picker"]');
        for (let t = 0, i = e.length; t < i; t++) xn.datePickerInterface(e[t]);
    }),
        g(xn);
    const In = "loading-button",
        Nn = "coreui.loading-button",
        Mn = `.${Nn}`,
        $n = `start${Mn}`,
        Pn = `stop${Mn}`,
        Hn = "is-loading",
        jn = { disabledOnLoading: !1, spinner: !0, spinnerType: "border", timeout: !1 },
        Bn = { disabledOnLoading: "boolean", spinner: "boolean", spinnerType: "string", timeout: "(boolean|number)" };
    class Rn extends j {
        constructor(e, t) {
            super(e), (this._config = this._getConfig(t)), (this._timeout = this._config.timeout), (this._spinner = null), (this._state = "idle"), this._element && H.set(e, Nn, this);
        }
        static get Default() {
            return jn;
        }
        static get DefaultType() {
            return Bn;
        }
        static get DATA_KEY() {
            return Nn;
        }
        static get NAME() {
            return In;
        }
        start() {
            "loading" !== this._state &&
                (this._createSpinner(),
                (this._state = "loading"),
                setTimeout(() => {
                    this._element.classList.add(Hn), $.trigger(this._element, $n), this._config.disabledOnLoading && this._element.setAttribute("disabled", !0);
                }, 1),
                this._config.timeout &&
                    setTimeout(() => {
                        this.stop();
                    }, this._config.timeout));
        }
        stop() {
            this._element.classList.remove(Hn);
            const e = () => {
                this._removeSpinner(), (this._state = "idle"), this._config.disabledOnLoading && this._element.removeAttribute("disabled"), $.trigger(this._element, Pn);
            };
            this._spinner ? this._queueCallback(e, this._spinner, !0) : e();
        }
        dispose() {
            H.removeData(this._element, Nn), (this._element = null);
        }
        _getConfig(e) {
            return (e = { ...jn, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(In, e, Bn), e;
        }
        _createSpinner() {
            if (this._config.spinner) {
                const e = document.createElement("span"),
                    t = this._config.spinnerType;
                e.classList.add("btn-loading-spinner", `spinner-${t}`, `spinner-${t}-sm`),
                    e.setAttribute("role", "status"),
                    e.setAttribute("aria-hidden", "true"),
                    this._element.insertBefore(e, this._element.firstChild),
                    (this._spinner = e);
            }
        }
        _removeSpinner() {
            this._config.spinner && (this._spinner.remove(), (this._spinner = null));
        }
        static loadingButtonInterface(e, t) {
            const i = Rn.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                Rn.loadingButtonInterface(this, e);
            });
        }
    }
    g(Rn);
    const Wn = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
        zn = ".sticky-top";
    class Vn {
        constructor() {
            this._element = document.body;
        }
        getWidth() {
            const e = document.documentElement.clientWidth;
            return Math.abs(window.innerWidth - e);
        }
        hide() {
            const e = this.getWidth();
            this._disableOverFlow(), this._setElementAttributes(this._element, "paddingRight", (t) => t + e), this._setElementAttributes(Wn, "paddingRight", (t) => t + e), this._setElementAttributes(zn, "marginRight", (t) => t - e);
        }
        _disableOverFlow() {
            this._saveInitialAttribute(this._element, "overflow"), (this._element.style.overflow = "hidden");
        }
        _setElementAttributes(e, t, i) {
            const n = this.getWidth();
            this._applyManipulationCallback(e, (e) => {
                if (e !== this._element && window.innerWidth > e.clientWidth + n) return;
                this._saveInitialAttribute(e, t);
                const s = window.getComputedStyle(e)[t];
                e.style[t] = `${i(Number.parseFloat(s))}px`;
            });
        }
        reset() {
            this._resetElementAttributes(this._element, "overflow"), this._resetElementAttributes(this._element, "paddingRight"), this._resetElementAttributes(Wn, "paddingRight"), this._resetElementAttributes(zn, "marginRight");
        }
        _saveInitialAttribute(e, t) {
            const i = e.style[t];
            i && q.setDataAttribute(e, t, i);
        }
        _resetElementAttributes(e, t) {
            this._applyManipulationCallback(e, (e) => {
                const i = q.getDataAttribute(e, t);
                void 0 === i ? e.style.removeProperty(t) : (q.removeDataAttribute(e, t), (e.style[t] = i));
            });
        }
        _applyManipulationCallback(e, t) {
            o(e) ? t(e) : fe.find(e, this._element).forEach(t);
        }
        isOverflowing() {
            return this.getWidth() > 0;
        }
    }
    const Fn = { className: "modal-backdrop", isVisible: !0, isAnimated: !1, rootElement: "body", clickCallback: null },
        qn = { className: "string", isVisible: "boolean", isAnimated: "boolean", rootElement: "(element|string)", clickCallback: "(function|null)" },
        Un = "show",
        Yn = "mousedown.coreui.backdrop";
    class Kn {
        constructor(e) {
            (this._config = this._getConfig(e)), (this._isAppended = !1), (this._element = null);
        }
        show(e) {
            this._config.isVisible
                ? (this._append(),
                  this._config.isAnimated && u(this._getElement()),
                  this._getElement().classList.add(Un),
                  this._emulateAnimation(() => {
                      m(e);
                  }))
                : m(e);
        }
        hide(e) {
            this._config.isVisible
                ? (this._getElement().classList.remove(Un),
                  this._emulateAnimation(() => {
                      this.dispose(), m(e);
                  }))
                : m(e);
        }
        _getElement() {
            if (!this._element) {
                const e = document.createElement("div");
                (e.className = this._config.className), this._config.isAnimated && e.classList.add("fade"), (this._element = e);
            }
            return this._element;
        }
        _getConfig(e) {
            return ((e = { ...Fn, ...("object" == typeof e ? e : {}) }).rootElement = a(e.rootElement)), r("backdrop", e, qn), e;
        }
        _append() {
            this._isAppended ||
                (this._config.rootElement.append(this._getElement()),
                $.on(this._getElement(), Yn, () => {
                    m(this._config.clickCallback);
                }),
                (this._isAppended = !0));
        }
        dispose() {
            this._isAppended && ($.off(this._element, Yn), this._element.remove(), (this._isAppended = !1));
        }
        _emulateAnimation(e) {
            v(e, this._getElement(), this._config.isAnimated);
        }
    }
    const Qn = { trapElement: null, autofocus: !0 },
        Xn = { trapElement: "element", autofocus: "boolean" },
        Gn = ".coreui.focustrap",
        Zn = `focusin${Gn}`,
        Jn = `keydown.tab${Gn}`,
        es = "backward";
    class ts {
        constructor(e) {
            (this._config = this._getConfig(e)), (this._isActive = !1), (this._lastTabNavDirection = null);
        }
        activate() {
            const { trapElement: e, autofocus: t } = this._config;
            this._isActive || (t && e.focus(), $.off(document, Gn), $.on(document, Zn, (e) => this._handleFocusin(e)), $.on(document, Jn, (e) => this._handleKeydown(e)), (this._isActive = !0));
        }
        deactivate() {
            this._isActive && ((this._isActive = !1), $.off(document, Gn));
        }
        _handleFocusin(e) {
            const { target: t } = e,
                { trapElement: i } = this._config;
            if (t === document || t === i || i.contains(t)) return;
            const n = fe.focusableChildren(i);
            0 === n.length ? i.focus() : this._lastTabNavDirection === es ? n[n.length - 1].focus() : n[0].focus();
        }
        _handleKeydown(e) {
            "Tab" === e.key && (this._lastTabNavDirection = e.shiftKey ? es : "forward");
        }
        _getConfig(e) {
            return (e = { ...Qn, ...("object" == typeof e ? e : {}) }), r("focustrap", e, Xn), e;
        }
    }
    const is = "modal",
        ns = "Escape",
        ss = { backdrop: !0, keyboard: !0, focus: !0 },
        os = { backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean" },
        as = "hidden.coreui.modal",
        rs = "show.coreui.modal",
        ls = "resize.coreui.modal",
        cs = "click.dismiss.coreui.modal",
        hs = "keydown.dismiss.coreui.modal",
        ds = "mousedown.dismiss.coreui.modal",
        us = "modal-open",
        ps = "show",
        fs = "modal-static";
    class _s extends j {
        constructor(e, t) {
            super(e),
                (this._config = this._getConfig(t)),
                (this._dialog = fe.findOne(".modal-dialog", this._element)),
                (this._backdrop = this._initializeBackDrop()),
                (this._focustrap = this._initializeFocusTrap()),
                (this._isShown = !1),
                (this._ignoreBackdropClick = !1),
                (this._isTransitioning = !1),
                (this._scrollBar = new Vn());
        }
        static get Default() {
            return ss;
        }
        static get NAME() {
            return is;
        }
        toggle(e) {
            return this._isShown ? this.hide() : this.show(e);
        }
        show(e) {
            this._isShown ||
                this._isTransitioning ||
                $.trigger(this._element, rs, { relatedTarget: e }).defaultPrevented ||
                ((this._isShown = !0),
                this._isAnimated() && (this._isTransitioning = !0),
                this._scrollBar.hide(),
                document.body.classList.add(us),
                this._adjustDialog(),
                this._setEscapeEvent(),
                this._setResizeEvent(),
                $.on(this._dialog, ds, () => {
                    $.one(this._element, "mouseup.dismiss.coreui.modal", (e) => {
                        e.target === this._element && (this._ignoreBackdropClick = !0);
                    });
                }),
                this._showBackdrop(() => this._showElement(e)));
        }
        hide() {
            if (!this._isShown || this._isTransitioning) return;
            if ($.trigger(this._element, "hide.coreui.modal").defaultPrevented) return;
            this._isShown = !1;
            const e = this._isAnimated();
            e && (this._isTransitioning = !0),
                this._setEscapeEvent(),
                this._setResizeEvent(),
                this._focustrap.deactivate(),
                this._element.classList.remove(ps),
                $.off(this._element, cs),
                $.off(this._dialog, ds),
                this._queueCallback(() => this._hideModal(), this._element, e);
        }
        dispose() {
            [window, this._dialog].forEach((e) => $.off(e, ".coreui.modal")), this._backdrop.dispose(), this._focustrap.deactivate(), super.dispose();
        }
        handleUpdate() {
            this._adjustDialog();
        }
        _initializeBackDrop() {
            return new Kn({ isVisible: Boolean(this._config.backdrop), isAnimated: this._isAnimated() });
        }
        _initializeFocusTrap() {
            return new ts({ trapElement: this._element });
        }
        _getConfig(e) {
            return (e = { ...ss, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(is, e, os), e;
        }
        _showElement(e) {
            const t = this._isAnimated(),
                i = fe.findOne(".modal-body", this._dialog);
            (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE) || document.body.append(this._element),
                (this._element.style.display = "block"),
                this._element.removeAttribute("aria-hidden"),
                this._element.setAttribute("aria-modal", !0),
                this._element.setAttribute("role", "dialog"),
                (this._element.scrollTop = 0),
                i && (i.scrollTop = 0),
                t && u(this._element),
                this._element.classList.add(ps),
                this._queueCallback(
                    () => {
                        this._config.focus && this._focustrap.activate(), (this._isTransitioning = !1), $.trigger(this._element, "shown.coreui.modal", { relatedTarget: e });
                    },
                    this._dialog,
                    t
                );
        }
        _setEscapeEvent() {
            this._isShown
                ? $.on(this._element, hs, (e) => {
                      this._config.keyboard && e.key === ns ? (e.preventDefault(), this.hide()) : this._config.keyboard || e.key !== ns || this._triggerBackdropTransition();
                  })
                : $.off(this._element, hs);
        }
        _setResizeEvent() {
            this._isShown ? $.on(window, ls, () => this._adjustDialog()) : $.off(window, ls);
        }
        _hideModal() {
            (this._element.style.display = "none"),
                this._element.setAttribute("aria-hidden", !0),
                this._element.removeAttribute("aria-modal"),
                this._element.removeAttribute("role"),
                (this._isTransitioning = !1),
                this._backdrop.hide(() => {
                    document.body.classList.remove(us), this._resetAdjustments(), this._scrollBar.reset(), $.trigger(this._element, as);
                });
        }
        _showBackdrop(e) {
            $.on(this._element, cs, (e) => {
                this._ignoreBackdropClick ? (this._ignoreBackdropClick = !1) : e.target === e.currentTarget && (!0 === this._config.backdrop ? this.hide() : "static" === this._config.backdrop && this._triggerBackdropTransition());
            }),
                this._backdrop.show(e);
        }
        _isAnimated() {
            return this._element.classList.contains("fade");
        }
        _triggerBackdropTransition() {
            if ($.trigger(this._element, "hidePrevented.coreui.modal").defaultPrevented) return;
            const { classList: e, scrollHeight: t, style: i } = this._element,
                n = t > document.documentElement.clientHeight;
            (!n && "hidden" === i.overflowY) ||
                e.contains(fs) ||
                (n || (i.overflowY = "hidden"),
                e.add(fs),
                this._queueCallback(() => {
                    e.remove(fs),
                        n ||
                            this._queueCallback(() => {
                                i.overflowY = "";
                            }, this._dialog);
                }, this._dialog),
                this._element.focus());
        }
        _adjustDialog() {
            const e = this._element.scrollHeight > document.documentElement.clientHeight,
                t = this._scrollBar.getWidth(),
                i = t > 0;
            ((!i && e && !_()) || (i && !e && _())) && (this._element.style.paddingLeft = `${t}px`), ((i && !e && !_()) || (!i && e && _())) && (this._element.style.paddingRight = `${t}px`);
        }
        _resetAdjustments() {
            (this._element.style.paddingLeft = ""), (this._element.style.paddingRight = "");
        }
        static jQueryInterface(e, t) {
            return this.each(function () {
                const i = _s.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === i[e]) throw new TypeError(`No method named "${e}"`);
                    i[e](t);
                }
            });
        }
    }
    $.on(document, "click.coreui.modal.data-api", '[data-coreui-toggle="modal"]', function (e) {
        const t = n(this);
        ["A", "AREA"].includes(this.tagName) && e.preventDefault(),
            $.one(t, rs, (e) => {
                e.defaultPrevented ||
                    $.one(t, as, () => {
                        l(this) && this.focus();
                    });
            });
        const i = fe.findOne(".modal.show");
        i && _s.getInstance(i).hide(), _s.getOrCreateInstance(t).toggle(this);
    }),
    B(_s),
    g(_s);

    const zs = "navigation",
        Vs = "coreui.navigation",
        Fs = `.${Vs}`,
        qs = { activeLinksExact: !0, groupsAutoCollapse: !0 },
        Us = { activeLinksExact: "boolean", groupsAutoCollapse: "(string|boolean)" },
        Ys = "active",
        Ks = "show",
        Qs = "nav-group-toggle",
        Xs = `click${Fs}.data-api`,
        Gs = `load${Fs}.data-api`,
        Zs = ".nav-group",
        Js = ".nav-group-items",
        eo = ".nav-group-toggle";
    class to extends j {
        constructor(e, t) {
            super(e), (this._config = this._getConfig(t)), this._setActiveLink(), this._addEventListeners(), H.set(e, Vs, this);
        }
        static get Default() {
            return qs;
        }
        static get DATA_KEY() {
            return Vs;
        }
        static get DefaultType() {
            return Us;
        }
        static get NAME() {
            return zs;
        }
        _getConfig(e) {
            return (e = { ...qs, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(zs, e, Us), e;
        }
        _setActiveLink() {
            Array.from(this._element.querySelectorAll(".nav-link")).forEach((e) => {
                if (e.classList.contains(Qs)) return;
                let t = String(window.location);
                (/\?.*=/.test(t) || /\?./.test(t)) && (t = t.split("?")[0]),
                    /#./.test(t) && (t = t.split("#")[0]),
                    this._config.activeLinksExact &&
                        e.href === t &&
                        (e.classList.add(Ys),
                        Array.from(this._getParents(e, Zs)).forEach((e) => {
                            e.classList.add(Ks), e.setAttribute("aria-expanded", !0);
                        })),
                    !this._config.activeLinksExact &&
                        e.href.startsWith(t) &&
                        (e.classList.add(Ys),
                        Array.from(this._getParents(e, Zs)).forEach((e) => {
                            e.classList.add(Ks), e.setAttribute("aria-expanded", !0);
                        }));
            });
        }
        _getParents(e, t) {
            const i = [];
            for (; e && e !== document; e = e.parentNode) t ? e.matches(t) && i.push(e) : i.push(e);
            return i;
        }
        _getAllSiblings(e, t) {
            const i = [];
            e = e.parentNode.firstChild;
            do {
                3 !== e.nodeType && 8 !== e.nodeType && ((t && !t(e)) || i.push(e));
            } while ((e = e.nextSibling));
            return i;
        }
        _getChildren(e, t) {
            const i = [];
            for (; e; e = e.nextSibling) 1 === e.nodeType && e !== t && i.push(e);
            return i;
        }
        _getSiblings(e, t) {
            return this._getChildren(e.parentNode.firstChild, e).filter(t);
        }
        _slideDown(e) {
            e.style.height = "auto";
            const t = e.clientHeight;
            (e.style.height = "0px"),
                setTimeout(() => {
                    e.style.height = `${t}px`;
                }, 0),
                this._queueCallback(
                    () => {
                        e.style.height = "auto";
                    },
                    e,
                    !0
                );
        }
        _slideUp(e, t) {
            const i = e.clientHeight;
            (e.style.height = `${i}px`),
                setTimeout(() => {
                    e.style.height = "0px";
                }, 0),
                this._queueCallback(
                    () => {
                        "function" == typeof t && t();
                    },
                    e,
                    !0
                );
        }
        _toggleGroupItems(e) {
            let t = e.target;
            t.classList.contains(Qs) || (t = t.closest(eo));
            !0 === this._config.groupsAutoCollapse &&
                this._getSiblings(t.parentNode, (e) => Boolean(e.classList.contains("nav-group") && e.classList.contains(Ks))).forEach((e) => {
                    this._slideUp(fe.findOne(Js, e), () => {
                        e.classList.remove(Ks), e.setAttribute("aria-expanded", !1);
                    });
                }),
                t.parentNode.classList.contains(Ks)
                    ? this._slideUp(fe.findOne(Js, t.parentNode), () => {
                          t.parentNode.classList.remove(Ks), t.parentNode.setAttribute("aria-expanded", !1);
                      })
                    : (t.parentNode.classList.add(Ks), t.parentNode.setAttribute("aria-expanded", !0), this._slideDown(fe.findOne(Js, t.parentNode)));
        }
        _addEventListeners() {
            $.on(this._element, Xs, eo, (e) => {
                e.preventDefault(), this._toggleGroupItems(e, this);
            });
        }
        static navigationInterface(e, t) {
            const i = to.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                to.navigationInterface(this, e);
            });
        }
    }
    $.on(window, Gs, () => {
        Array.from(document.querySelectorAll('[data-coreui="navigation"]')).forEach((e) => {
            to.navigationInterface(e);
        });
    }),
        g(to);
    const io = "offcanvas",
        no = ".coreui.offcanvas",
        so = `load${no}.data-api`,
        oo = { backdrop: !0, keyboard: !0, scroll: !1 },
        ao = { backdrop: "boolean", keyboard: "boolean", scroll: "boolean" },
        ro = "show",
        lo = ".offcanvas.show",
        co = `show${no}`,
        ho = `shown${no}`,
        uo = `hide${no}`,
        po = `hidden${no}`,
        fo = `click${no}.data-api`,
        _o = `keydown.dismiss${no}`;
    class go extends j {
        constructor(e, t) {
            super(e), (this._config = this._getConfig(t)), (this._isShown = !1), (this._backdrop = this._initializeBackDrop()), (this._focustrap = this._initializeFocusTrap()), this._addEventListeners();
        }
        static get NAME() {
            return io;
        }
        static get Default() {
            return oo;
        }
        toggle(e) {
            return this._isShown ? this.hide() : this.show(e);
        }
        show(e) {
            this._isShown ||
                $.trigger(this._element, co, { relatedTarget: e }).defaultPrevented ||
                ((this._isShown = !0),
                (this._element.style.visibility = "visible"),
                this._backdrop.show(),
                this._config.scroll || new Vn().hide(),
                this._element.removeAttribute("aria-hidden"),
                this._element.setAttribute("aria-modal", !0),
                this._element.setAttribute("role", "dialog"),
                this._element.classList.add(ro),
                this._queueCallback(
                    () => {
                        this._config.scroll || this._focustrap.activate(), $.trigger(this._element, ho, { relatedTarget: e });
                    },
                    this._element,
                    !0
                ));
        }
        hide() {
            this._isShown &&
                ($.trigger(this._element, uo).defaultPrevented ||
                    (this._focustrap.deactivate(),
                    this._element.blur(),
                    (this._isShown = !1),
                    this._element.classList.remove(ro),
                    this._backdrop.hide(),
                    this._queueCallback(
                        () => {
                            this._element.setAttribute("aria-hidden", !0),
                                this._element.removeAttribute("aria-modal"),
                                this._element.removeAttribute("role"),
                                (this._element.style.visibility = "hidden"),
                                this._config.scroll || new Vn().reset(),
                                $.trigger(this._element, po);
                        },
                        this._element,
                        !0
                    )));
        }
        dispose() {
            this._backdrop.dispose(), this._focustrap.deactivate(), super.dispose();
        }
        _getConfig(e) {
            return (e = { ...oo, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(io, e, ao), e;
        }
        _initializeBackDrop() {
            return new Kn({ className: "offcanvas-backdrop", isVisible: this._config.backdrop, isAnimated: !0, rootElement: this._element.parentNode, clickCallback: () => this.hide() });
        }
        _initializeFocusTrap() {
            return new ts({ trapElement: this._element });
        }
        _addEventListeners() {
            $.on(this._element, _o, (e) => {
                this._config.keyboard && "Escape" === e.key && this.hide();
            });
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = go.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e] || e.startsWith("_") || "constructor" === e) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    $.on(document, fo, '[data-coreui-toggle="offcanvas"]', function (e) {
        const t = n(this);
        if ((["A", "AREA"].includes(this.tagName) && e.preventDefault(), c(this))) return;
        $.one(t, po, () => {
            l(this) && this.focus();
        });
        const i = fe.findOne(lo);
        i && i !== t && go.getInstance(i).hide(), go.getOrCreateInstance(t).toggle(this);
    }),
        $.on(window, so, () => fe.find(lo).forEach((e) => go.getOrCreateInstance(e).show())),
        B(go),
        g(go);
    const mo = new Set(["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"]),
        vo = /^(?:(?:https?|mailto|ftp|tel|file|sms):|[^#&/:?]*(?:[#/?]|$))/i,
        bo = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[\d+/a-z]+=*$/i,
        yo = (e, t) => {
            const i = e.nodeName.toLowerCase();
            if (t.includes(i)) return !mo.has(i) || Boolean(vo.test(e.nodeValue) || bo.test(e.nodeValue));
            const n = t.filter((e) => e instanceof RegExp);
            for (let e = 0, t = n.length; e < t; e++) if (n[e].test(i)) return !0;
            return !1;
        };
    function wo(e, t, i) {
        if (!e.length) return e;
        if (i && "function" == typeof i) return i(e);
        const n = new window.DOMParser().parseFromString(e, "text/html"),
            s = [].concat(...n.body.querySelectorAll("*"));
        for (let e = 0, i = s.length; e < i; e++) {
            const i = s[e],
                n = i.nodeName.toLowerCase();
            if (!Object.keys(t).includes(n)) {
                i.remove();
                continue;
            }
            const o = [].concat(...i.attributes),
                a = [].concat(t["*"] || [], t[n] || []);
            o.forEach((e) => {
                yo(e, a) || i.removeAttribute(e.nodeName);
            });
        }
        return n.body.innerHTML;
    }
    const Eo = "tooltip",
        Do = ".coreui.tooltip",
        Lo = new Set(["sanitize", "allowList", "sanitizeFn"]),
        To = {
            animation: "boolean",
            template: "string",
            title: "(string|element|function)",
            trigger: "string",
            delay: "(number|object)",
            html: "boolean",
            selector: "(string|boolean)",
            placement: "(string|function)",
            offset: "(array|string|function)",
            container: "(string|element|boolean)",
            fallbackPlacements: "array",
            boundary: "(string|element)",
            customClass: "(string|function)",
            sanitize: "boolean",
            sanitizeFn: "(null|function)",
            allowList: "object",
            popperConfig: "(null|object|function)",
        },
        ko = { AUTO: "auto", TOP: "top", RIGHT: _() ? "left" : "right", BOTTOM: "bottom", LEFT: _() ? "right" : "left" },
        Co = {
            animation: !0,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
            trigger: "hover focus",
            title: "",
            delay: 0,
            html: !1,
            selector: !1,
            placement: "top",
            offset: [0, 0],
            container: !1,
            fallbackPlacements: ["top", "right", "bottom", "left"],
            boundary: "clippingParents",
            customClass: "",
            sanitize: !0,
            sanitizeFn: null,
            allowList: {
                "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i],
                a: ["target", "href", "title", "rel"],
                area: [],
                b: [],
                br: [],
                col: [],
                code: [],
                div: [],
                em: [],
                hr: [],
                h1: [],
                h2: [],
                h3: [],
                h4: [],
                h5: [],
                h6: [],
                i: [],
                img: ["src", "srcset", "alt", "title", "width", "height"],
                li: [],
                ol: [],
                p: [],
                pre: [],
                s: [],
                small: [],
                span: [],
                sub: [],
                sup: [],
                strong: [],
                u: [],
                ul: [],
            },
            popperConfig: null,
        },
        Ao = {
            HIDE: `hide${Do}`,
            HIDDEN: `hidden${Do}`,
            SHOW: `show${Do}`,
            SHOWN: `shown${Do}`,
            INSERTED: `inserted${Do}`,
            CLICK: `click${Do}`,
            FOCUSIN: `focusin${Do}`,
            FOCUSOUT: `focusout${Do}`,
            MOUSEENTER: `mouseenter${Do}`,
            MOUSELEAVE: `mouseleave${Do}`,
        },
        Oo = "fade",
        So = "show",
        xo = "show",
        Io = "out",
        No = ".tooltip-inner",
        Mo = ".modal",
        $o = "hide.coreui.modal",
        Po = "hover",
        Ho = "focus";
    class jo extends j {
        constructor(e, t) {
            if (void 0 === Ii) throw new TypeError("Bootstrap's tooltips require Popper (https://popper.js.org)");
            super(e), (this._isEnabled = !0), (this._timeout = 0), (this._hoverState = ""), (this._activeTrigger = {}), (this._popper = null), (this._config = this._getConfig(t)), (this.tip = null), this._setListeners();
        }
        static get Default() {
            return Co;
        }
        static get NAME() {
            return Eo;
        }
        static get Event() {
            return Ao;
        }
        static get DefaultType() {
            return To;
        }
        enable() {
            this._isEnabled = !0;
        }
        disable() {
            this._isEnabled = !1;
        }
        toggleEnabled() {
            this._isEnabled = !this._isEnabled;
        }
        toggle(e) {
            if (this._isEnabled)
                if (e) {
                    const t = this._initializeOnDelegatedTarget(e);
                    (t._activeTrigger.click = !t._activeTrigger.click), t._isWithActiveTrigger() ? t._enter(null, t) : t._leave(null, t);
                } else {
                    if (this.getTipElement().classList.contains(So)) return void this._leave(null, this);
                    this._enter(null, this);
                }
        }
        dispose() {
            clearTimeout(this._timeout), $.off(this._element.closest(Mo), $o, this._hideModalHandler), this.tip && this.tip.remove(), this._disposePopper(), super.dispose();
        }
        show() {
            if ("none" === this._element.style.display) throw new Error("Please use show on visible elements");
            if (!this.isWithContent() || !this._isEnabled) return;
            const e = $.trigger(this._element, this.constructor.Event.SHOW),
                t = h(this._element),
                i = null === t ? this._element.ownerDocument.documentElement.contains(this._element) : t.contains(this._element);
            if (e.defaultPrevented || !i) return;
            "tooltip" === this.constructor.NAME && this.tip && this.getTitle() !== this.tip.querySelector(No).innerHTML && (this._disposePopper(), this.tip.remove(), (this.tip = null));
            const n = this.getTipElement(),
                s = ((e) => {
                    do {
                        e += Math.floor(1e6 * Math.random());
                    } while (document.getElementById(e));
                    return e;
                })(this.constructor.NAME);
            n.setAttribute("id", s), this._element.setAttribute("aria-describedby", s), this._config.animation && n.classList.add(Oo);
            const o = "function" == typeof this._config.placement ? this._config.placement.call(this, n, this._element) : this._config.placement,
                a = this._getAttachment(o);
            this._addAttachmentClass(a);
            const { container: r } = this._config;
            H.set(n, this.constructor.DATA_KEY, this),
                this._element.ownerDocument.documentElement.contains(this.tip) || (r.append(n), $.trigger(this._element, this.constructor.Event.INSERTED)),
                this._popper ? this._popper.update() : (this._popper = xi(this._element, n, this._getPopperConfig(a))),
                n.classList.add(So);
            const l = this._resolvePossibleFunction(this._config.customClass);
            l && n.classList.add(...l.split(" ")),
                "ontouchstart" in document.documentElement &&
                    [].concat(...document.body.children).forEach((e) => {
                        $.on(e, "mouseover", d);
                    });
            const c = this.tip.classList.contains(Oo);
            this._queueCallback(
                () => {
                    const e = this._hoverState;
                    (this._hoverState = null), $.trigger(this._element, this.constructor.Event.SHOWN), e === Io && this._leave(null, this);
                },
                this.tip,
                c
            );
        }
        hide() {
            if (!this._popper) return;
            const e = this.getTipElement();
            if ($.trigger(this._element, this.constructor.Event.HIDE).defaultPrevented) return;
            e.classList.remove(So),
                "ontouchstart" in document.documentElement && [].concat(...document.body.children).forEach((e) => $.off(e, "mouseover", d)),
                (this._activeTrigger.click = !1),
                (this._activeTrigger.focus = !1),
                (this._activeTrigger.hover = !1);
            const t = this.tip.classList.contains(Oo);
            this._queueCallback(
                () => {
                    this._isWithActiveTrigger() ||
                        (this._hoverState !== xo && e.remove(), this._cleanTipClass(), this._element.removeAttribute("aria-describedby"), $.trigger(this._element, this.constructor.Event.HIDDEN), this._disposePopper());
                },
                this.tip,
                t
            ),
                (this._hoverState = "");
        }
        update() {
            null !== this._popper && this._popper.update();
        }
        isWithContent() {
            return Boolean(this.getTitle());
        }
        getTipElement() {
            if (this.tip) return this.tip;
            const e = document.createElement("div");
            e.innerHTML = this._config.template;
            const t = e.children[0];
            return this.setContent(t), t.classList.remove(Oo, So), (this.tip = t), this.tip;
        }
        setContent(e) {
            this._sanitizeAndSetContent(e, this.getTitle(), No);
        }
        _sanitizeAndSetContent(e, t, i) {
            const n = fe.findOne(i, e);
            t || !n ? this.setElementContent(n, t) : n.remove();
        }
        setElementContent(e, t) {
            if (null !== e)
                return o(t)
                    ? ((t = a(t)), void (this._config.html ? t.parentNode !== e && ((e.innerHTML = ""), e.append(t)) : (e.textContent = t.textContent)))
                    : void (this._config.html ? (this._config.sanitize && (t = wo(t, this._config.allowList, this._config.sanitizeFn)), (e.innerHTML = t)) : (e.textContent = t));
        }
        getTitle() {
            const e = this._element.getAttribute("data-coreui-original-title") || this._config.title;
            return this._resolvePossibleFunction(e);
        }
        updateAttachment(e) {
            return "right" === e ? "end" : "left" === e ? "start" : e;
        }
        _initializeOnDelegatedTarget(e, t) {
            return t || this.constructor.getOrCreateInstance(e.delegateTarget, this._getDelegateConfig());
        }
        _getOffset() {
            const { offset: e } = this._config;
            return "string" == typeof e ? e.split(",").map((e) => Number.parseInt(e, 10)) : "function" == typeof e ? (t) => e(t, this._element) : e;
        }
        _resolvePossibleFunction(e) {
            return "function" == typeof e ? e.call(this._element) : e;
        }
        _getPopperConfig(e) {
            const t = {
                placement: e,
                modifiers: [
                    { name: "flip", options: { fallbackPlacements: this._config.fallbackPlacements } },
                    { name: "offset", options: { offset: this._getOffset() } },
                    { name: "preventOverflow", options: { boundary: this._config.boundary } },
                    { name: "arrow", options: { element: `.${this.constructor.NAME}-arrow` } },
                    { name: "onChange", enabled: !0, phase: "afterWrite", fn: (e) => this._handlePopperPlacementChange(e) },
                ],
                onFirstUpdate: (e) => {
                    e.options.placement !== e.placement && this._handlePopperPlacementChange(e);
                },
            };
            return { ...t, ...("function" == typeof this._config.popperConfig ? this._config.popperConfig(t) : this._config.popperConfig) };
        }
        _addAttachmentClass(e) {
            this.getTipElement().classList.add(`${this._getBasicClassPrefix()}-${this.updateAttachment(e)}`);
        }
        _getAttachment(e) {
            return ko[e.toUpperCase()];
        }
        _setListeners() {
            this._config.trigger.split(" ").forEach((e) => {
                if ("click" === e) $.on(this._element, this.constructor.Event.CLICK, this._config.selector, (e) => this.toggle(e));
                else if ("manual" !== e) {
                    const t = e === Po ? this.constructor.Event.MOUSEENTER : this.constructor.Event.FOCUSIN,
                        i = e === Po ? this.constructor.Event.MOUSELEAVE : this.constructor.Event.FOCUSOUT;
                    $.on(this._element, t, this._config.selector, (e) => this._enter(e)), $.on(this._element, i, this._config.selector, (e) => this._leave(e));
                }
            }),
                (this._hideModalHandler = () => {
                    this._element && this.hide();
                }),
                $.on(this._element.closest(Mo), $o, this._hideModalHandler),
                this._config.selector ? (this._config = { ...this._config, trigger: "manual", selector: "" }) : this._fixTitle();
        }
        _fixTitle() {
            const e = this._element.getAttribute("title"),
                t = typeof this._element.getAttribute("data-coreui-original-title");
            (e || "string" !== t) &&
                (this._element.setAttribute("data-coreui-original-title", e || ""),
                !e || this._element.getAttribute("aria-label") || this._element.textContent || this._element.setAttribute("aria-label", e),
                this._element.setAttribute("title", ""));
        }
        _enter(e, t) {
            (t = this._initializeOnDelegatedTarget(e, t)),
                e && (t._activeTrigger["focusin" === e.type ? Ho : Po] = !0),
                t.getTipElement().classList.contains(So) || t._hoverState === xo
                    ? (t._hoverState = xo)
                    : (clearTimeout(t._timeout),
                      (t._hoverState = xo),
                      t._config.delay && t._config.delay.show
                          ? (t._timeout = setTimeout(() => {
                                t._hoverState === xo && t.show();
                            }, t._config.delay.show))
                          : t.show());
        }
        _leave(e, t) {
            (t = this._initializeOnDelegatedTarget(e, t)),
                e && (t._activeTrigger["focusout" === e.type ? Ho : Po] = t._element.contains(e.relatedTarget)),
                t._isWithActiveTrigger() ||
                    (clearTimeout(t._timeout),
                    (t._hoverState = Io),
                    t._config.delay && t._config.delay.hide
                        ? (t._timeout = setTimeout(() => {
                              t._hoverState === Io && t.hide();
                          }, t._config.delay.hide))
                        : t.hide());
        }
        _isWithActiveTrigger() {
            for (const e in this._activeTrigger) if (this._activeTrigger[e]) return !0;
            return !1;
        }
        _getConfig(e) {
            const t = q.getDataAttributes(this._element);
            return (
                Object.keys(t).forEach((e) => {
                    Lo.has(e) && delete t[e];
                }),
                ((e = { ...this.constructor.Default, ...t, ...("object" == typeof e && e ? e : {}) }).container = !1 === e.container ? document.body : a(e.container)),
                "number" == typeof e.delay && (e.delay = { show: e.delay, hide: e.delay }),
                "number" == typeof e.title && (e.title = e.title.toString()),
                "number" == typeof e.content && (e.content = e.content.toString()),
                r(Eo, e, this.constructor.DefaultType),
                e.sanitize && (e.template = wo(e.template, e.allowList, e.sanitizeFn)),
                e
            );
        }
        _getDelegateConfig() {
            const e = {};
            for (const t in this._config) this.constructor.Default[t] !== this._config[t] && (e[t] = this._config[t]);
            return e;
        }
        _cleanTipClass() {
            const e = this.getTipElement(),
                t = new RegExp(`(^|\\s)${this._getBasicClassPrefix()}\\S+`, "g"),
                i = e.getAttribute("class").match(t);
            null !== i && i.length > 0 && i.map((e) => e.trim()).forEach((t) => e.classList.remove(t));
        }
        _getBasicClassPrefix() {
            return "bs-tooltip";
        }
        _handlePopperPlacementChange(e) {
            const { state: t } = e;
            t && ((this.tip = t.elements.popper), this._cleanTipClass(), this._addAttachmentClass(this._getAttachment(t.placement)));
        }
        _disposePopper() {
            this._popper && (this._popper.destroy(), (this._popper = null));
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = jo.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e]();
                }
            });
        }
    }
    g(jo);
    const Bo = ".coreui.popover",
        Ro = {
            ...jo.Default,
            placement: "right",
            offset: [0, 8],
            trigger: "click",
            content: "",
            template: '<div class="popover" role="tooltip"><div class="popover-arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
        },
        Wo = { ...jo.DefaultType, content: "(string|element|function)" },
        zo = {
            HIDE: `hide${Bo}`,
            HIDDEN: `hidden${Bo}`,
            SHOW: `show${Bo}`,
            SHOWN: `shown${Bo}`,
            INSERTED: `inserted${Bo}`,
            CLICK: `click${Bo}`,
            FOCUSIN: `focusin${Bo}`,
            FOCUSOUT: `focusout${Bo}`,
            MOUSEENTER: `mouseenter${Bo}`,
            MOUSELEAVE: `mouseleave${Bo}`,
        };
    class Vo extends jo {
        static get Default() {
            return Ro;
        }
        static get NAME() {
            return "popover";
        }
        static get Event() {
            return zo;
        }
        static get DefaultType() {
            return Wo;
        }
        isWithContent() {
            return this.getTitle() || this._getContent();
        }
        setContent(e) {
            this._sanitizeAndSetContent(e, this.getTitle(), ".popover-header"), this._sanitizeAndSetContent(e, this._getContent(), ".popover-body");
        }
        _getContent() {
            return this._resolvePossibleFunction(this._config.content);
        }
        _getBasicClassPrefix() {
            return "bs-popover";
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = Vo.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e]();
                }
            });
        }
    }
    g(Vo);
    const Fo = "scrollspy",
        qo = ".coreui.scrollspy",
        Uo = { offset: 10, method: "auto", target: "" },
        Yo = { offset: "number", method: "string", target: "(string|element)" },
        Ko = `activate${qo}`,
        Qo = `scroll${qo}`,
        Xo = `load${qo}.data-api`,
        Go = "active",
        Zo = ".nav-link, .list-group-item, .dropdown-item",
        Jo = "position";
    class ea extends j {
        constructor(e, t) {
            super(e),
                (this._scrollElement = "BODY" === this._element.tagName ? window : this._element),
                (this._config = this._getConfig(t)),
                (this._offsets = []),
                (this._targets = []),
                (this._activeTarget = null),
                (this._scrollHeight = 0),
                $.on(this._scrollElement, Qo, () => this._process()),
                this.refresh(),
                this._process();
        }
        static get Default() {
            return Uo;
        }
        static get NAME() {
            return Fo;
        }
        refresh() {
            const e = this._scrollElement === this._scrollElement.window ? "offset" : Jo,
                t = "auto" === this._config.method ? e : this._config.method,
                n = t === Jo ? this._getScrollTop() : 0;
            (this._offsets = []),
                (this._targets = []),
                (this._scrollHeight = this._getScrollHeight()),
                fe
                    .find(Zo, this._config.target)
                    .map((e) => {
                        const s = i(e),
                            o = s ? fe.findOne(s) : null;
                        if (o) {
                            const e = o.getBoundingClientRect();
                            if (e.width || e.height) return [q[t](o).top + n, s];
                        }
                        return null;
                    })
                    .filter((e) => e)
                    .sort((e, t) => e[0] - t[0])
                    .forEach((e) => {
                        this._offsets.push(e[0]), this._targets.push(e[1]);
                    });
        }
        dispose() {
            $.off(this._scrollElement, qo), super.dispose();
        }
        _getConfig(e) {
            return ((e = { ...Uo, ...q.getDataAttributes(this._element), ...("object" == typeof e && e ? e : {}) }).target = a(e.target) || document.documentElement), r(Fo, e, Yo), e;
        }
        _getScrollTop() {
            return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
        }
        _getScrollHeight() {
            return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
        }
        _getOffsetHeight() {
            return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
        }
        _process() {
            const e = this._getScrollTop() + this._config.offset,
                t = this._getScrollHeight(),
                i = this._config.offset + t - this._getOffsetHeight();
            if ((this._scrollHeight !== t && this.refresh(), e >= i)) {
                const e = this._targets[this._targets.length - 1];
                this._activeTarget !== e && this._activate(e);
            } else {
                if (this._activeTarget && e < this._offsets[0] && this._offsets[0] > 0) return (this._activeTarget = null), void this._clear();
                for (let t = this._offsets.length; t--; ) this._activeTarget !== this._targets[t] && e >= this._offsets[t] && (void 0 === this._offsets[t + 1] || e < this._offsets[t + 1]) && this._activate(this._targets[t]);
            }
        }
        _activate(e) {
            (this._activeTarget = e), this._clear();
            const t = Zo.split(",").map((t) => `${t}[data-coreui-target="${e}"],${t}[href="${e}"]`),
                i = fe.findOne(t.join(","), this._config.target);
            i.classList.add(Go),
                i.classList.contains("dropdown-item")
                    ? fe.findOne(".dropdown-toggle", i.closest(".dropdown")).classList.add(Go)
                    : fe.parents(i, ".nav, .list-group").forEach((e) => {
                          fe.prev(e, ".nav-link, .list-group-item").forEach((e) => e.classList.add(Go)),
                              fe.prev(e, ".nav-item").forEach((e) => {
                                  fe.children(e, ".nav-link").forEach((e) => e.classList.add(Go));
                              });
                      }),
                $.trigger(this._scrollElement, Ko, { relatedTarget: e });
        }
        _clear() {
            fe.find(Zo, this._config.target)
                .filter((e) => e.classList.contains(Go))
                .forEach((e) => e.classList.remove(Go));
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = ea.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e]();
                }
            });
        }
    }
    $.on(window, Xo, () => {
        fe.find('[data-coreui-spy="scroll"]').forEach((e) => new ea(e));
    }),
        g(ea);
    const ta = "sidebar",
        ia = ".coreui.sidebar",
        na = {},
        sa = {},
        oa = "hide",
        aa = "show",
        ra = "sidebar-narrow",
        la = "sidebar-narrow-unfoldable",
        ca = `hide${ia}`,
        ha = `hidden${ia}`,
        da = `show${ia}`,
        ua = `shown${ia}`,
        pa = `click${ia}.data-api`,
        fa = `load${ia}.data-api`,
        _a = ".sidebar";
    class ga extends j {
        constructor(e, t) {
            super(e),
                (this._config = this._getConfig(t)),
                (this._show = this._isVisible()),
                (this._mobile = this._isMobile()),
                (this._overlaid = this._isOverlaid()),
                (this._narrow = this._isNarrow()),
                (this._unfoldable = this._isUnfoldable()),
                (this._backdrop = this._initializeBackDrop()),
                this._addEventListeners();
        }
        static get Default() {
            return na;
        }
        static get DefaultType() {
            return sa;
        }
        static get NAME() {
            return ta;
        }
        show() {
            $.trigger(this._element, da),
                this._element.classList.contains(oa) && this._element.classList.remove(oa),
                this._isMobile() && (this._element.classList.add(aa), this._backdrop.show(), new Vn().hide()),
                this._queueCallback(
                    () => {
                        !0 === this._isVisible() && ((this._show = !0), (this._isMobile() || this._isOverlaid()) && this._addClickOutListener(), $.trigger(this._element, ua));
                    },
                    this._element,
                    !0
                );
        }
        hide() {
            $.trigger(this._element, ca),
                this._element.classList.contains(aa) && this._element.classList.remove(aa),
                this._isMobile() ? (this._backdrop.hide(), new Vn().reset()) : this._element.classList.add(oa),
                this._queueCallback(
                    () => {
                        !1 === this._isVisible() && ((this._show = !1), (this._isMobile() || this._isOverlaid()) && this._removeClickOutListener(), $.trigger(this._element, ha));
                    },
                    this._element,
                    !0
                );
        }
        toggle() {
            this._isVisible() ? this.hide() : this.show();
        }
        narrow() {
            this._isMobile() || (this._addClassName(ra), (this._narrow = !0));
        }
        unfoldable() {
            this._isMobile() || (this._addClassName(la), (this._unfoldable = !0));
        }
        reset() {
            this._isMobile() || (this._narrow && (this._element.classList.remove(ra), (this._narrow = !1)), this._unfoldable && (this._element.classList.remove(la), (this._unfoldable = !1)));
        }
        toggleNarrow() {
            this._narrow ? this.reset() : this.narrow();
        }
        toggleUnfoldable() {
            this._unfoldable ? this.reset() : this.unfoldable();
        }
        _getConfig(e) {
            return (e = { ...na, ...q.getDataAttributes(this._element), ...("object" == typeof e ? e : {}) }), r(ta, e, sa), e;
        }
        _initializeBackDrop() {
            return new Kn({ className: "sidebar-backdrop", isVisible: this._isMobile(), isAnimated: !0, rootElement: this._element.parentNode, clickCallback: () => this.hide() });
        }
        _isMobile() {
            return Boolean(window.getComputedStyle(this._element, null).getPropertyValue("--cui-is-mobile"));
        }
        _isNarrow() {
            return this._element.classList.contains(ra);
        }
        _isOverlaid() {
            return this._element.classList.contains("sidebar-overlaid");
        }
        _isUnfoldable() {
            return this._element.classList.contains(la);
        }
        _isVisible() {
            const e = this._element.getBoundingClientRect();
            return e.top >= 0 && e.left >= 0 && Math.floor(e.bottom) <= (window.innerHeight || document.documentElement.clientHeight) && Math.floor(e.right) <= (window.innerWidth || document.documentElement.clientWidth);
        }
        _addClassName(e) {
            this._element.classList.add(e);
        }
        _clickOutListener(e, t) {
            null === e.target.closest(_a) && (e.preventDefault(), e.stopPropagation(), t.hide());
        }
        _addClickOutListener() {
            $.on(document, pa, (e) => {
                this._clickOutListener(e, this);
            });
        }
        _removeClickOutListener() {
            $.off(document, pa);
        }
        _addEventListeners() {
            this._mobile && this._show && this._addClickOutListener(),
                this._overlaid && this._show && this._addClickOutListener(),
                $.on(this._element, pa, "[data-coreui-toggle]", (e) => {
                    e.preventDefault();
                    const t = q.getDataAttribute(e.target, "toggle");
                    "narrow" === t && this.toggleNarrow(), "unfoldable" === t && this.toggleUnfoldable();
                }),
                $.on(this._element, pa, '[data-coreui-close="sidebar"]', (e) => {
                    e.preventDefault(), this.hide();
                }),
                $.on(window, "resize", () => {
                    this._isMobile() && this._isVisible() && (this.hide(), (this._backdrop = this._initializeBackDrop()));
                });
        }
        static sidebarInterface(e, t) {
            const i = ga.getOrCreateInstance(e, t);
            if ("string" == typeof t) {
                if (void 0 === i[t]) throw new TypeError(`No method named "${t}"`);
                i[t]();
            }
        }
        static jQueryInterface(e) {
            return this.each(function () {
                ga.sidebarInterface(this, e);
            });
        }
    }
    $.on(window, fa, () => {
        Array.from(document.querySelectorAll(_a)).forEach((e) => {
            ga.sidebarInterface(e);
        });
    }),
        g(ga);
    const ma = "active",
        va = "fade",
        ba = "show",
        ya = ".active",
        wa = ":scope > li > .active";
    class Ea extends j {
        static get NAME() {
            return "tab";
        }
        show() {
            if (this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && this._element.classList.contains(ma)) return;
            let e;
            const t = n(this._element),
                i = this._element.closest(".nav, .list-group");
            if (i) {
                const t = "UL" === i.nodeName || "OL" === i.nodeName ? wa : ya;
                (e = fe.find(t, i)), (e = e[e.length - 1]);
            }
            const s = e ? $.trigger(e, "hide.coreui.tab", { relatedTarget: this._element }) : null;
            if ($.trigger(this._element, "show.coreui.tab", { relatedTarget: e }).defaultPrevented || (null !== s && s.defaultPrevented)) return;
            this._activate(this._element, i);
            const o = () => {
                $.trigger(e, "hidden.coreui.tab", { relatedTarget: this._element }), $.trigger(this._element, "shown.coreui.tab", { relatedTarget: e });
            };
            t ? this._activate(t, t.parentNode, o) : o();
        }
        _activate(e, t, i) {
            const n = (!t || ("UL" !== t.nodeName && "OL" !== t.nodeName) ? fe.children(t, ya) : fe.find(wa, t))[0],
                s = i && n && n.classList.contains(va),
                o = () => this._transitionComplete(e, n, i);
            n && s ? (n.classList.remove(ba), this._queueCallback(o, e, !0)) : o();
        }
        _transitionComplete(e, t, i) {
            if (t) {
                t.classList.remove(ma);
                const e = fe.findOne(":scope > .dropdown-menu .active", t.parentNode);
                e && e.classList.remove(ma), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !1);
            }
            e.classList.add(ma), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !0), u(e), e.classList.contains(va) && e.classList.add(ba);
            let n = e.parentNode;
            if ((n && "LI" === n.nodeName && (n = n.parentNode), n && n.classList.contains("dropdown-menu"))) {
                const t = e.closest(".dropdown");
                t && fe.find(".dropdown-toggle", t).forEach((e) => e.classList.add(ma)), e.setAttribute("aria-expanded", !0);
            }
            i && i();
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = Ea.getOrCreateInstance(this);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e]();
                }
            });
        }
    }
    $.on(document, "click.coreui.tab.data-api", '[data-coreui-toggle="tab"], [data-coreui-toggle="pill"], [data-coreui-toggle="list"]', function (e) {
        ["A", "AREA"].includes(this.tagName) && e.preventDefault(), c(this) || Ea.getOrCreateInstance(this).show();
    }),
        g(Ea);
    const Da = "toast",
        La = ".coreui.toast",
        Ta = `mouseover${La}`,
        ka = `mouseout${La}`,
        Ca = `focusin${La}`,
        Aa = `focusout${La}`,
        Oa = `hide${La}`,
        Sa = `hidden${La}`,
        xa = `show${La}`,
        Ia = `shown${La}`,
        Na = "hide",
        Ma = "show",
        $a = "showing",
        Pa = { animation: "boolean", autohide: "boolean", delay: "number" },
        Ha = { animation: !0, autohide: !0, delay: 5e3 };
    class ja extends j {
        constructor(e, t) {
            super(e), (this._config = this._getConfig(t)), (this._timeout = null), (this._hasMouseInteraction = !1), (this._hasKeyboardInteraction = !1), this._setListeners();
        }
        static get DefaultType() {
            return Pa;
        }
        static get Default() {
            return Ha;
        }
        static get NAME() {
            return Da;
        }
        show() {
            $.trigger(this._element, xa).defaultPrevented ||
                (this._clearTimeout(),
                this._config.animation && this._element.classList.add("fade"),
                this._element.classList.remove(Na),
                u(this._element),
                this._element.classList.add(Ma),
                this._element.classList.add($a),
                this._queueCallback(
                    () => {
                        this._element.classList.remove($a), $.trigger(this._element, Ia), this._maybeScheduleHide();
                    },
                    this._element,
                    this._config.animation
                ));
        }
        hide() {
            this._element.classList.contains(Ma) &&
                ($.trigger(this._element, Oa).defaultPrevented ||
                    (this._element.classList.add($a),
                    this._queueCallback(
                        () => {
                            this._element.classList.add(Na), this._element.classList.remove($a), this._element.classList.remove(Ma), $.trigger(this._element, Sa);
                        },
                        this._element,
                        this._config.animation
                    )));
        }
        dispose() {
            this._clearTimeout(), this._element.classList.contains(Ma) && this._element.classList.remove(Ma), super.dispose();
        }
        _getConfig(e) {
            return (e = { ...Ha, ...q.getDataAttributes(this._element), ...("object" == typeof e && e ? e : {}) }), r(Da, e, this.constructor.DefaultType), e;
        }
        _maybeScheduleHide() {
            this._config.autohide &&
                (this._hasMouseInteraction ||
                    this._hasKeyboardInteraction ||
                    (this._timeout = setTimeout(() => {
                        this.hide();
                    }, this._config.delay)));
        }
        _onInteraction(e, t) {
            switch (e.type) {
                case "mouseover":
                case "mouseout":
                    this._hasMouseInteraction = t;
                    break;
                case "focusin":
                case "focusout":
                    this._hasKeyboardInteraction = t;
            }
            if (t) return void this._clearTimeout();
            const i = e.relatedTarget;
            this._element === i || this._element.contains(i) || this._maybeScheduleHide();
        }
        _setListeners() {
            $.on(this._element, Ta, (e) => this._onInteraction(e, !0)),
                $.on(this._element, ka, (e) => this._onInteraction(e, !1)),
                $.on(this._element, Ca, (e) => this._onInteraction(e, !0)),
                $.on(this._element, Aa, (e) => this._onInteraction(e, !1));
        }
        _clearTimeout() {
            clearTimeout(this._timeout), (this._timeout = null);
        }
        static jQueryInterface(e) {
            return this.each(function () {
                const t = ja.getOrCreateInstance(this, e);
                if ("string" == typeof e) {
                    if (void 0 === t[e]) throw new TypeError(`No method named "${e}"`);
                    t[e](this);
                }
            });
        }
    }
    return (
        B(ja),
        g(ja),
        {
            Alert: R,
            Button: z,
            Calendar: pe,
            Carousel: Be,
            Collapse: it,
            DatePicker: xn,
            DateRangePicker: Tn,
            Dropdown: on,
            LoadingButton: Rn,
            Modal: _s,
            Navigation: to,
            OffCanvas: go,
            Popover: Vo,
            ScrollSpy: ea,
            Sidebar: ga,
            Tab: Ea,
            Timepicker: mn,
            Toast: ja,
            Tooltip: jo,
        }
    );
});

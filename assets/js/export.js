/*******************************************************************

    Module        : /modules/js/export.js
    Desc.         : Fungsi-fungsi export data & laporan
    Created By    : Siau Pheng (siaupheng@3fonia.com).
    Created Date  : April 1st, 2011.
    Last Modified : January 7th, 2016.

    (c) 2011 - 2016, 3FONIA Software; WWW.3FONIA.COM.

*******************************************************************/

var cEXPORT = function(url, options){
    var vars = {
        'url': '',
        'param': []
    };
    this.construct = function(url, options){
        $.extend(vars, options);
        vars.url = url;
    };
    this.set_url = function (url){
        vars.url = url;
    };
    this.add_param = function (par_id, par_val){
        var _getvalue = (par_val) ? par_id + "=" + escape(par_val) : par_id;
        vars.param.push(_getvalue);
    },
    this.download = function(){
        if (!vars.url) {
            console.warn(vars.param + ", cek url php!");
            return false;
        }
        var _pro = (window.location.protocol=='https:') ? 'https://' : 'http://';
        var _src = _pro+FoniaJS.WEB_HOST + vars.url + '?' + (FoniaJS.RequestParam(vars.param)).join('&');
        var $exportData = $('#IFrame_Export');
        if (!$exportData.length) {
            var $exportData = $('<iframe>', {id:'IFrame_Export','class':'d-none'}).appendTo('body');
        }
        $exportData.prop('src', _src);
        return this;
    };
    this.construct(url, options);
};
